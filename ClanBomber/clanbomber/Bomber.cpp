/***************************************************************************
                          Bomber.cpp  -  description
                             -------------------
    begin                : ?
    copyright            : (C) 1999 by Andreas Hundt, Denis Oliver Kropp
    email                : clanbomber@fischlustig.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
// $Id: Bomber.cpp,v 1.3 2004/01/08 18:10:39 der_fisch Exp $

#include <ClanLib/Core/System/system.h>
#include <ClanLib/Sound/soundbuffer.h>
#include <ClanLib/Display/Display/display.h>

#include "ClanBomber.h"
#include "Bomber.h"

#include "Bomber_Corpse.h"
#include "Bomb.h"
#include "Config.h"
#include "Timer.h"
#include "Controller.h"
#include "Disease.h"
#include "Map.h"
#include "MapTile.h"

#include "Extra_Bomb.h"
#include "Extra_Skateboard.h"
#include "Extra_Power.h"
#include "Extra_Kick.h"
#include "Extra_Glove.h"

#include <math.h>

Bomber::Bomber( int _x, int _y, COLOR _color, Controller* _controller, CL_String _name, int _team, int _number, ClanBomberApplication *_app) : GameObject( _x, _y, _app )
{
	name = _name;
	team = _team;
	number = _number;

	surface = Resources::Bombers(_color);

	color = _color;
	points = 0;
	kills = 0;
	deaths = 0;
	disease = NULL;
	
	switch (color)
	{
		case 0 :
		case 1 :
		case 3 :
			offset_x = 60;
			offset_y = 11;
			break;
		case 2 :
		case 4 :
		case 5 :
		case 6 :
		case 7 :
			offset_x = 60;
			offset_y = 40;	
	}

	controller = _controller;
	controller->attach(this);
	controller->reset();

	reset();
	app->bomber_objects.add(this);
}

void Bomber::reset()
{
	if (orig_x > 300)
	{
		cur_dir = DIR_LEFT;
	} else
	{
		cur_dir = DIR_RIGHT;
	}
	sprite_nr = 0;

	can_pass_bomber = true;
	can_throw = true;

	if (disease)
	{
		disease->stop();
		delete disease;
		disease = NULL;
	}

	dead = false;
	falling = false;
	fallen_down = false;
	flying = false;

	extra_gloves = 0;
	extra_skateboards =0;
	extra_power = 0;
	extra_kick = 0;
	extra_bombs = 0;

	skateboards = Config::get_start_skateboards();
	speed = 90 + (skateboards*40);

	can_kick = Config::get_start_kick();
	cur_bombs = Config::get_start_bombs();
	bombs = Config::get_start_bombs();
	power = Config::get_start_power();
	gloves = Config::get_start_glove();
	
	x = orig_x;
	y = orig_y;
	anim_count = 0;
}

int Bomber::get_team() const
{
	return team;
}

int Bomber::get_number() const
{
	return number;
}

void Bomber::set_team(int _team)
{
	team = _team;
}

CL_String Bomber::get_name() const
{
	return name;
}

bool Bomber::is_diseased()
{
	return (disease != NULL);
}

Bomber::~Bomber()
{
	if (controller)
	{
		delete controller;
	}
}

void Bomber::show()
{
	if (disease && !dead && !falling)
	{
		GameObject::show(int(x)+60,int(y)+40,sin( CL_System::get_time() / 50.0f )  *  0.15f+1);
	} else
	if (!dead && cur_dir!=DIR_NONE)
	{
		GameObject::show();
	}
}

void Bomber::act()
{
	if (dead)
	{
		return;
	}
	
	controller->update();
	
	if (disease)
	{
		infect_others();
		
		disease->act();
		if (disease->get_countdown() == -1)
		{
			delete disease;
			disease = NULL;
		}
	}
		
	GameObject::act();
	bool moved = false;

	if (fallen_down)
	{
		die();
		return;
	}

	if (controller->is_left())
	{
		moved = true;
		cur_dir = DIR_LEFT;
	} else
	if (controller->is_up())
	{
		moved = true;
		cur_dir = DIR_UP;
	} else
	if (controller->is_right())
	{
		moved = true;
		cur_dir = DIR_RIGHT;
	} else
	if (controller->is_down())
	{
		moved = true;
		cur_dir = DIR_DOWN;
	} else
	{
		anim_count = 0;
	}
	if (moved)
	{
		anim_count += Timer::time_elapsed()*20*(speed/90);
		move();
	}


	if (anim_count >= 9) anim_count = 1;

	sprite_nr = cur_dir*10 + (int)anim_count;
		
	if ( controller->is_bomb() )
	{
		if (get_maptile()->bomb)
		{
			if (gloves && !moved)
			{
				get_maptile()->bomb->throww(cur_dir);
				anim_count = 2;
			}
		}
		else
		{
			put_bomb();
		}
	}
	
	if (!falling)
	{
		z = Z_BOMBER + get_y();
	}
}

void Bomber::infect_others()
{
	CL_Iterator<GameObject> object_counter(get_maptile()->objects);
	
	while (object_counter.next() != NULL)
	{
		if (object_counter()->get_type()==GameObject::BOMBER && !((Bomber*)(object_counter()))->is_diseased())
		{
			((Bomber*)(object_counter()))->infect( disease->spawn((Bomber*)(object_counter())) );
		}
	}
}

void Bomber::put_bomb()
{
	if (cur_bombs && !falling)
	{
		if (app->map->add_bomb( int((x+20)/40), int((y+20)/40), power, this ))
		{
			PLAY_PAN(Resources::Game_putbomb());
		}
	}
}

void Bomber::gain_extra_skateboard()
{
	if (Config::get_max_skateboards() > skateboards)
	{
		skateboards++;
		extra_skateboards++;
		speed+=40;
	}
}


void Bomber::gain_extra_bomb()
{
	if (Config::get_max_bombs() > bombs)
	{
		bombs++;
		extra_bombs++;
		cur_bombs++;
	}
}

void Bomber::gain_extra_glove()
{
	gloves++;
	extra_gloves++;
}

Bomber::COLOR Bomber::get_color() const
{
	return color;
}

void Bomber::gain_extra_power()
{
	if (Config::get_max_power() > power)
	{
		power++;
		extra_power++;
	}
}

void Bomber::loose_all_extras()
{
	int i;
	Extra* extra;
		
	for (i=0;i<extra_bombs;i++)
	{
		extra = new Extra_Bomb(int(x),int(y),app);		
	     extra->fly_to(app->map->get_passable());
	}
	extra_bombs = 0;
	
	for (i=0;i<extra_power;i++)
	{
		extra = new Extra_Power(int(x),int(y),app);		
	     extra->fly_to(app->map->get_passable());
	}
	extra_power = 0;

	for (i=0;i<extra_skateboards;i++)
	{		
		extra = new Extra_Skateboard(int(x),int(y),app);
	     extra->fly_to(app->map->get_passable());
	}
	extra_skateboards = 0;

	for (i=0;i<extra_kick;i++)
	{		
		extra = new Extra_Kick(int(x),int(y),app);		
	     extra->fly_to(app->map->get_passable());
	}
	extra_kick = 0;

	for (i=0;i<extra_gloves;i++)
	{				
		extra = new Extra_Glove(int(x),int(y),app);		
	     extra->fly_to(app->map->get_passable());				
	}
	extra_gloves = 0;
}


void Bomber::loose_disease()
{
	Extra* extra;

	if (disease)
	{

		disease->stop();
		extra = disease->spawn_extra(int(x),int(y));		
		extra->fly_to(app->map->get_passable());

		delete disease;
		disease = NULL;		
	}
}

void Bomber::infect (Disease* _disease)
{
	if (disease)
	{
		disease->stop();
		delete disease;
	}
	disease = _disease;
}


void Bomber::gain_extra_kick()
{
		can_kick = true;
		extra_kick++;
}

void Bomber::inc_cur_bombs()
{
	cur_bombs++;
}

void Bomber::dec_cur_bombs()
{
	if (cur_bombs)
	{
		cur_bombs--;
	}
}

GameObject::ObjectType Bomber::get_type() const
{
	return BOMBER;
}

bool Bomber::die()
{
	if (dead || is_falling())
	{
		return false;
	}
	dead = true;
	deaths++;
	PLAY_PAN(Resources::Game_die());

	if (!fallen_down)
	{
		loose_all_extras();
		if (!Config::get_kids_mode())
		{
			new Bomber_Corpse(int(x), int(y), color, cur_dir*10+9, app);
		}
	}
	x = 0;
	y = 0;
	sprite_nr = cur_dir*10+9;
	return true;
}

void Bomber::inc_kills()
{
	kills++;
}

int Bomber::get_cur_bombs() const
{
	return cur_bombs;
}

int Bomber::get_power() const
{
	return power;
}

int Bomber::get_bombs() const
{
	return bombs;
}

int Bomber::get_kills() const
{
	return kills;
}

int Bomber::get_points() const
{
	return points;
}

void Bomber::set_points( int _points )
{
	points = _points;
}

void Bomber::inc_points()
{
	points++;
}

void Bomber::dec_points()
{
	points--;
}

bool Bomber::is_dead() const
{
	return dead;
}

int Bomber::get_deaths() const
{
	return deaths;
}


