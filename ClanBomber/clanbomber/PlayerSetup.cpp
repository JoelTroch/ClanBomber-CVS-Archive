/***************************************************************************
                          PlayerSetup.cpp  -  description                              
                             -------------------                                         
    begin                : Fri May 14 1999                                           
    copyright            : (C) 1999 by Andreas Hundt, Denis Oliver Kropp                         
    email                : clanbomber@fischlustig.de                                     
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   * 
 *                                                                         *
 ***************************************************************************/

#include <ClanLib/Core/System/system.h>
#include <ClanLib/Display/Input/input.h>
#include <ClanLib/Display/Input/inputbuffer.h>
#include <ClanLib/Display/Input/keyboard.h>
#include <ClanLib/Sound/soundbuffer.h>
#include <ClanLib/Display/Display/display.h>
#include <ClanLib/Display/Display/surface.h>
#include <ClanLib/Display/Font/font.h>

#include "ClanBomber.h"
#include "PlayerSetup.h"

#include "Config.h"
#include "Timer.h"
#include "Controller.h"

PlayerSetup::PlayerSetup(ClanBomberApplication* _app)
{
	app = _app;
	cur_row = 0;
	cur_col = 0;
	key_buffer = new CL_InputBuffer( CL_Input::keyboards[0] );
}

PlayerSetup::~PlayerSetup()
{
	delete key_buffer;
}

void PlayerSetup::exec()
{
	draw();
	CL_Display::flip_display();	
	while (CL_Keyboard::get_keycode(CL_KEY_ENTER))
	{
		CL_System::sleep( 50 );
		CL_System::keep_alive();
	}
	key_buffer->clear();
	
	while (1)
	{
		while (key_buffer->keys_left() == 0)
		{
			CL_System::sleep( 10 );
			CL_System::keep_alive();
		}
		if (key_buffer->peek_key().state != CL_Key::Pressed)
		{
			key_buffer->get_key();
			continue;
		}
		switch (key_buffer->get_key().id)
		{
			case CL_KEY_ESCAPE:
				return;
			break;
			case CL_KEY_ENTER:
				Resources::Menu_clear()->play();
				handle_enter();
				Config::save();
			break;
			case CL_KEY_SPACE:
				if (Config::bomber[cur_row].is_enabled())
				{
					Config::bomber[cur_row].disable();
				} else
				{
					Config::bomber[cur_row].enable();
				}
				Config::save();
				Resources::Menu_clear()->play();
			break;
			case CL_KEY_H:
				Config::bomber[cur_row].set_highlight_maptile(!Config::bomber[cur_row].get_highlight_maptile());
				Config::save();
				Resources::Menu_clear()->play();
			break;
			case CL_KEY_DOWN:
				cur_row += (cur_row<7) ? 1 : 0;
				Resources::Menu_break()->play();
			break;
			case CL_KEY_UP:
				cur_row -= (cur_row>0) ? 1 : 0;
				Resources::Menu_break()->play();
			break;
			case CL_KEY_LEFT:
				cur_col -= (cur_col>0) ? 1 : 0;
				Resources::Menu_break()->play();
			break;
			case CL_KEY_RIGHT:
				cur_col += (cur_col<3) ? 1 : 0;
				Resources::Menu_break()->play();
			break;
		}
		draw();
		CL_Display::flip_display();
	}
}

void PlayerSetup::draw(bool fick)
{
	int cg[5] = { 17, 95, 340, 500, 720 };

	Resources::Playersetup_background()->put_screen(0, 0, 0 );
	
	Resources::Font_small()->print_center( 55, 40, "SKIN" );
	Resources::Font_small()->print_left( 130, 40, "NAME" );
	Resources::Font_small()->print_center( 418, 40, "TEAM" );
	Resources::Font_small()->print_center( 612, 40, "CONTROLLER" );
	
	Resources::Font_small()->print_left( 12, 580, "SPACE ENABLES OR DISABLES A PLAYER, H TOGGLES MAPTILE HIGHLIGHTING" );
	
	CL_Display::fill_rect( 0, 70+cur_row*63, 800, 70+cur_row*63 + 63,  0.2f, 0.4f, 0.8f, 0.3f );
	if (!fick) CL_Display::fill_rect( cg[cur_col], 70+cur_row*63, cg[cur_col+1], 70+cur_row*63 + 63,  0.5f, 0.5f, 1.0f, 0.4f );

	for (int i=0; i<8; i++)
	{
		if (Config::bomber[i].get_highlight_maptile())
		{
			CL_Display::fill_rect( 34, 90+i*63, 74, 130+i*63,  0.0f, 0.0f, 1.0f, 0.2f);
		}
		Resources::Bombers(Config::bomber[i].get_skin())->put_screen( 35, 130+i*63 - Resources::Bombers(Config::bomber[i].get_skin())->get_height(), 0 );

		if (!fick || cur_row!=i) Resources::Font_big()->print_left( 130, 90 + i*63, Config::bomber[i].get_name() );
		Resources::Playersetup_teams()->put_screen( 360, 72 + i*63, Config::bomber[i].get_team() );
		switch (Config::bomber[i].get_controller() )
		{
			case Controller::AI:
				Resources::Playersetup_controls()->put_screen( 550, 72 + i*63,0);
				Resources::Font_small()->print_right( 650,102 + i*63,"DOK"); 				
			break;

			case Controller::AI_MIC:
				Resources::Playersetup_controls()->put_screen( 550, 72 + i*63,0);
				Resources::Font_small()->print_right( 650,102 + i*63,"MIC"); 					
			break;

			case Controller::KEYMAP_1:
			case Controller::KEYMAP_2:
			case Controller::KEYMAP_3:
				Resources::Playersetup_controls()->put_screen( 550, 72 + i*63,Config::bomber[i].get_controller()-Controller::KEYMAP_1+1);
			break;

			case Controller::JOYSTICK_1:
			case Controller::JOYSTICK_2:
			case Controller::JOYSTICK_3:
			case Controller::JOYSTICK_4:
			case Controller::JOYSTICK_5:
			case Controller::JOYSTICK_6:
			case Controller::JOYSTICK_7:
				Resources::Playersetup_controls()->put_screen( 550, 72 + i*63,4);
				Resources::Font_small()->print_right( 650,102 + i*63,CL_String(Config::bomber[i].get_controller()-Controller::JOYSTICK_1+1));
			break;
		}

		if (!Config::bomber[i].is_enabled())
		{
			CL_Display::fill_rect( 0, 70+i*63, 800, 70+i*63 + 63,  0.1f, 0.1f, 0.1f, 0.6f );
		}
	}
}

void PlayerSetup::handle_enter()
{
	switch (cur_col)
	{
		case 0:	Config::bomber[cur_row].set_skin( Config::bomber[cur_row].get_skin()+1 ); break;
		case 1:   enter_name(); Resources::Menu_clear()->play(); break;
		case 2:	Config::bomber[cur_row].set_team( Config::bomber[cur_row].get_team()+1 ); break;
		case 3:	Config::bomber[cur_row].set_controller( Config::bomber[cur_row].get_controller()+1 ); break;
	}
}

void PlayerSetup::enter_name()
{
	float alpha = 0;
	
	while (CL_Keyboard::get_keycode(CL_KEY_ENTER))
	{
		CL_System::keep_alive();
	}
	
	key_buffer->clear();
	CL_String new_string = Config::bomber[cur_row].get_name();
	
	Timer::reset();
	while (1)
	{
		alpha += Timer::time_elapsed(true);
		while (alpha > 0.5f) alpha -= 0.5f;
		draw(true);
		CL_Display::fill_rect( 95,70+cur_row*63, 340,70+(cur_row+1)*63, 0.2f, 0.8f, 0.5f, (alpha>0.25f) ? (0.5f-alpha)*3.0f : alpha*3.0f );

		Resources::Font_big()->print_left( 130, 90 + cur_row*63, new_string );

		CL_Display::flip_display();
		
		CL_System::keep_alive();
		
		if (enter_string( key_buffer, new_string ))
		{
			Config::bomber[cur_row].set_name( new_string );
			return;
		}
	}
}




