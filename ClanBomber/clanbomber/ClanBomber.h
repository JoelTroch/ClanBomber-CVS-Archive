/***************************************************************************
                          ClanBomber.h  -  description
                             -------------------
    begin                : ?
    copyright            : (C) 1999 by Andreas Hundt, Denis Oliver Kropp
    email                : clanbomber@fischlustig.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
// $Id: ClanBomber.h,v 1.4 2001/11/12 02:44:10 xmassx Exp $

#ifndef ClanBomber_h
#define ClanBomber_h


#include "Resources.h"

class Menu;
class Debug;
class Observer;
class Map;
class Bomb;
class Bomber;
class GameObject;

#include <ClanLib/Core/System/setupcore.h>
#include <ClanLib/Display/setupdisplay.h>
#include <ClanLib/Sound/setupsound.h>
#include <ClanLib/Application/clanapp.h>
#include <ClanLib/Core/System/clanstring.h>
#include "link.h"

#ifndef WIN32
	#include <config.h>
#endif

#ifdef MUSIC
#include <ClanLib/mikmod.h>
#endif

enum
{
	MENU_LOCALGAME,
	MENU_OPTIONS,
	MENU_CREDITS,
	MENU_PLAYER_SETUP,
	MENU_EXTRA_VALUES,
	MENU_EXTRA_ONOFF,
	MENU_EXIT,
	MENU_CONTROLLER,
	MENU_HELP,
	MENU_DISEASE_ONOFF,
	MENU_MAP_EDITOR,
	MENU_TIMING,

	CONFIG_START_BOMBS,
	CONFIG_START_POWER,
	CONFIG_START_SKATES,
	CONFIG_START_KICK,
	CONFIG_START_GLOVE,

	CONFIG_BOMBS,
	CONFIG_POWER,
	CONFIG_SKATES,
	CONFIG_KICK,
	CONFIG_GLOVE,

	CONFIG_MAX_BOMBS,
	CONFIG_MAX_POWER,
	CONFIG_MAX_SKATES,

	CONFIG_POINTS,
	CONFIG_MAP_SEL,
	CONFIG_ROUND_TIME,
	CONFIG_MUSIC,

	CONFIG_JOINT,
	CONFIG_VIAGRA,
	CONFIG_KOKS,

	CONFIG_KIDS_MODE,
	CONFIG_SHAKE,
	CONFIG_CORPSE_PARTS,
	CONFIG_RANDOM_POSITIONS,
	CONFIG_RANDOM_MAP_ORDER,

	CONFIG_BOMB_COUNTDOWN,
	CONFIG_BOMB_DELAY,
	CONFIG_BOMB_SPEED,

	LOCALGAME_START
};

typedef enum
{
	DIR_NONE = -1,
	DIR_DOWN  = 0,
	DIR_LEFT  = 1,
	DIR_UP    = 2,
	DIR_RIGHT = 3
} Direction;


class ClanBomberApplication : public CL_ClanApplication
{
friend class Debug;
protected:

	void run_intro();
	void init_game(); // single player init
	void deinit_game();
	void show_tutorial();

	void run_game();
	void show_all();
	void act_all();
	void delete_some();

	
	Menu* menu;
	
	Observer* observer;
	CL_SoundBuffer* wav;
	CL_SoundBuffer_Session* ses;
	
	static CL_String map_path;
	static CL_String local_map_path;

	bool key_F1;
	bool show_fps;
	int fps;
	int frame_count;
	float frame_time;

public:
	Map* map;
	CL_List<GameObject> objects;
	CL_List<Bomber> bomber_objects;
	virtual char *get_title();
	virtual int main(int argc, char** argv);
#ifndef WIN32
	virtual void quit_app(int value) __attribute__ ((noreturn));
#else
	virtual void quit_app(int value);
#endif
	static CL_String get_map_path();
	static CL_String get_local_map_path();


	virtual void init_modules()
    {
		        CL_SetupCore::init();
	        CL_SetupDisplay::init();
	        CL_SetupSound::init();
#ifdef MUSIC	
			CL_SetupMikMod::init();
#endif
	}

	virtual void deinit_modules()
	{
#ifdef MUSIC	
	  CL_SetupMikMod::deinit();
#endif
	        CL_SetupSound::deinit();
		CL_SetupDisplay::deinit();
	        CL_SetupCore::deinit();
	}
};

#endif


// we like profanity in open source software, please read the following words carefully:
// fuck, pussy, dick, sperm, motherfucker
//
// hope our source code will now be censored by all governments that suck.


/* Global macros and functions (to be implemented in ClanLib?) */

// this is for reading strings from an ascii file

class CL_InputSource;
class CL_InputBuffer;
CL_String read_line( CL_InputSource* in );
int enter_string( CL_InputBuffer *key_buffer, CL_String &string );

#define PLAY_PAN(PP_sample) \
			{ \
				CL_SoundBuffer_Session ss((PP_sample)->prepare()); \
				ss.set_pan( (x-340) / 340.0f ); \
				ss.play(); \
			}

#ifdef WITH_DEBUG
 #define dout(dstring) cout << endl << "__ " << __FILE__ << ":" << __LINE__ << " __" << endl << dstring << endl
#else
 #ifdef WITH_DEBUG_SIMPLE
  #define dout(dstring) cout << dstring << endl
 #else
  #define dout(dstring)
 #endif
#endif

#ifndef WIN32
	#define min(a,b) ((a<b) ? (a) : (b))
	#define max(a,b) ((a>b) ? (a) : (b))
#endif

#define SHIFT (CL_Keyboard::get_keycode(CL_KEY_LSHIFT) || CL_Keyboard::get_keycode(CL_KEY_RSHIFT))



