/***************************************************************************
                          GameObject.cpp  -  description
                             -------------------
    begin                : ?
    copyright            : (C) 1999 by Andreas Hundt, Denis Oliver Kropp
    email                : clanbomber@fischlustig.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
// $Id: GameObject.cpp,v 1.3 2004/01/08 18:10:39 der_fisch Exp $

#include <ClanLib/Sound/soundbuffer.h>
#include <ClanLib/Display/Display/surface.h>
#include <ClanLib/Core/System/cl_assert.h>

#include "ClanBomber.h"
#include "GameObject.h"

#include "Map.h"
#include "MapTile.h"
#include "Timer.h"
#include "Bomber.h"
#include "Bomb.h"

#include <math.h>

GameObject::GameObject( int _x, int _y, ClanBomberApplication *_app )
{
	app = _app;
	
	offset_x = 60;
	offset_y = 40;
	delete_me = false;
	remainder =0;
	speed = 240;
	
	cur_dir = DIR_NONE;
	can_kick = false;
	can_pass_bomber = false;
	can_fly_over_walls = true;
	flying = false;
	falling = false;
	fallen_down = false;
	stopped = false;
	fly_progress = 0;	// must be set to 0!

	x = orig_x = _x;
	y = orig_y = _y;
	z = 0;
}

GameObject::~GameObject()
{
}

bool GameObject::move_right()
{
	MapTile* right_maptile = app->map->get_maptile_xy( int(x)+40,int(y)+20);

	if (right_maptile->is_blocking())
	{
		return false;
	}
		
	if ((right_maptile->bomb==NULL) || ((get_type()==BOMB) && (right_maptile->bomb==this)) || (get_x()%40>19))
	{
		x++;

		if (get_y()%40 > 19)
		{
			if (app->map->get_maptile( (get_x()+40)/40, (get_y()-20)/40 )->is_blocking())
			{
				y++;
			}
		} else
		if (get_y()%40 > 0)
		{
			if (app->map->get_maptile( (get_x()+40)/40, (get_y()+60)/40 )->is_blocking())
			{
				y--;
			}
		}

		if (!can_pass_bomber)
		{
                        if ( right_maptile->has_bomber() )
                        {
                                if (right_maptile != get_maptile())
                                {
                                        x--;
                                        return false;
                                }
                        }
		}
	}
    	else
	{
		if (can_kick  && (right_maptile->bomb->get_cur_dir() == DIR_NONE))
		{
			right_maptile->bomb->kick(DIR_RIGHT);
		}
		return false;
	}
	
	return true;
}

bool GameObject::move_left()
{
	MapTile* left_maptile = app->map->get_maptile_xy( int(x)-1,int(y)+20);
	if (left_maptile->is_blocking())
	{
		return false;
	}
		
	if ((left_maptile->bomb==NULL) || ((get_type()==BOMB) && (left_maptile->bomb==this)) || (get_x()%40<20))
	{
		x--;
		
		if (get_y()%40 > 19)
		{
			if (app->map->get_maptile( (get_x()-1)/40, (get_y()-20)/40 )->is_blocking())
			{
				y++;
			}
		} else
		if (get_y()%40 > 0)
		{
			if (app->map->get_maptile( (get_x()-1)/40, (get_y()+60)/40 )->is_blocking())
			{
				y--;
			}
		}
			
		if (!can_pass_bomber)
		{
			if ( left_maptile->has_bomber() )
			{			
				if (left_maptile != get_maptile())
				{
					x++;
					return false;
				}
			}
		}
	}
    	else
	{
		if (can_kick  && (left_maptile->bomb->get_cur_dir() == DIR_NONE))
		{
			left_maptile->bomb->kick(DIR_LEFT);
		}
		return false;
	}	
	return true;
}

bool GameObject::move_up()
{
	MapTile* up_maptile = app->map->get_maptile_xy(int(x)+20,int(y)-1);
	if (up_maptile->is_blocking())
	{
		return false;
	}
		
	if ((up_maptile->bomb==NULL) || ((get_type()==BOMB) && (up_maptile->bomb==this)) || (get_y()%40<20))
	{
		y--;
			
		if (get_x()%40 > 19)
		{
			if (app->map->get_maptile((get_x()-20)/40,(get_y()-1)/40)->is_blocking())
			{
				x++;
			}
		} else
		if (get_x()%40 > 0)
		{
			if (app->map->get_maptile((get_x()+60)/40,(get_y()-1)/40)->is_blocking())
			{
				x--;
			}
		}
			
		if (!can_pass_bomber)
		{
                        if ( up_maptile->has_bomber() )
                        {
                                if (up_maptile != get_maptile())
                                {
                                        y++;
                                        return false;
                                }
                        }
		}
	}
    	else
	{
		if (can_kick  && (up_maptile->bomb->get_cur_dir() == DIR_NONE))
		{
			up_maptile->bomb->kick(DIR_UP);
		}
		return false;
	}
	
	return true;
}

bool GameObject::move_down()
{
	MapTile* down_maptile = app->map->get_maptile_xy(int(x)+20,int(y)+40);
	
	if (down_maptile->is_blocking())
	{
		return false;
	}
	
	if ((down_maptile->bomb==NULL) || ((get_type()==BOMB) && (down_maptile->bomb==this)) || (get_y()%40>19))
	{
		y++;
			
		if (get_x()%40 > 19)
		{
			if (app->map->get_maptile((get_x()-20)/40,(get_y()+40)/40)->is_blocking())
			{
				x++;
			}
		} else
		if (get_x()%40 > 0)
		{
			if (app->map->get_maptile((get_x()+60)/40,(get_y()+40)/40)->is_blocking())
			{
				x--;
			}
		}
			
		if (!can_pass_bomber)
		{
                        if ( down_maptile->has_bomber() )
                        {
                                if (down_maptile != get_maptile())
                                {
                                        y--;
                                        return false;
                                }
                        }
		}
	}
    	else
	{
		if (can_kick  && (down_maptile->bomb->get_cur_dir() == DIR_NONE))
		{
			down_maptile->bomb->kick(DIR_DOWN);
		}
		return false;
	}
	
	return true;
}

bool GameObject::move()
{
	if (!flying)
	{
		int span = (int)(Timer::time_elapsed()*speed);
	
		remainder += Timer::time_elapsed()*speed - (int)(Timer::time_elapsed()*speed);
		span += (int)(remainder);
		remainder -= (int)(remainder);
	
		for (int i=0; i<span; i++)
		{
			switch (cur_dir)
			{
				case DIR_LEFT:
					if (!move_left())
					{
						stop();
						return false;
					}
				break;
				case DIR_RIGHT:
					if (!move_right())
					{
						stop();
						return false;
					}
				break;
				case DIR_UP:
					if (!move_up())
					{
						stop();
						return false;
					}
				break;
				case DIR_DOWN:
					if (!move_down())
					{
						stop();
						return false;
					}
				break;
				default:
				break;
			}
		}
	}
	return true;
}

void GameObject::act()
{
	stopped = false;
	if (flying)
	{
		continue_flying();
	}
	if (falling)
	{
		continue_falling();
	}
}

void GameObject::fly_to (int _x, int _y, int _speed)
{
	if (!flying)
	{	
		flying = true;
		
		fly_dest_x = _x;
		fly_dest_y = _y;
		
		fly_progress = 0;	// goes from 0 to 1
		fly_dist_x = _x - x;
		fly_dist_y = _y - y;
		
		fly_speed = _speed ? _speed : speed;
		fly_speed /= sqrt( fly_dist_x*fly_dist_x + fly_dist_y*fly_dist_y );
		
		z += Z_FLYING;
	}
}

void GameObject::fly_to (MapTile *maptile, int _speed)
{
	cl_assert( maptile );
	fly_to( maptile->get_x(), maptile->get_y(), _speed );
}

void GameObject::continue_flying()
{
	if (Timer::time_elapsed() == 0)
	{
		return;
	}

	float time_span = Timer::time_elapsed();
	float time_step = time_span;
	int steps = 1;
	while( abs((int)(time_step*fly_speed*fly_dist_x)) > 5  ||  abs((int)(time_step*fly_speed*fly_dist_y)) > 5 )
	{
		time_step /= 2.0f;
		steps *= 2;
	}

	while (steps--)
	{
		x += time_step * fly_speed * fly_dist_x;
		y += time_step * fly_speed * fly_dist_y;
		
		if (get_type() == CORPSE_PART  &&  app->map != NULL)
		{
			if (!can_fly_over_walls && get_maptile()->get_type() == MapTile::WALL)
			{
				x -= time_step * fly_speed * fly_dist_x;
				y -= time_step * fly_speed * fly_dist_y;
				fly_dest_x = x;
				fly_dest_y = y;
//				break;
			}
		} else
		{
			if (!can_fly_over_walls  &&  app->map != NULL  &&  (
				app->map->get_maptile_xy( int(x), int(y) )->get_type() == MapTile::WALL ||
				app->map->get_maptile_xy( int(x)+39, int(y) )->get_type() == MapTile::WALL ||
				app->map->get_maptile_xy( int(x), int(y)+39 )->get_type() == MapTile::WALL ||
				app->map->get_maptile_xy( int(x)+39, int(y)+39)->get_type() == MapTile::WALL ))
			{
				x -= time_step * fly_speed * fly_dist_x;
				y -= time_step * fly_speed * fly_dist_y;
				fly_dest_x = x;
				fly_dest_y = y;
//				break;
			}
		}
	
		fly_progress += time_step * fly_speed;
	
		if (fly_progress >= 1)
		{
			flying = false;
			fly_progress = 1;	// don�t set to 0! 1 indicates a finished flight!
			if (z > Z_FLYING)
			{
				z -= Z_FLYING;
			}
			x = fly_dest_x;
			y = fly_dest_y;
		}
	}
}

bool GameObject::move( int _speed, Direction _dir )
{
	if (_dir==DIR_NONE && cur_dir!=DIR_NONE)
	{
		return move( _speed, cur_dir );
	}

	int span = (int)(Timer::time_elapsed() * _speed);
	
	remainder += Timer::time_elapsed() * _speed - (int)(Timer::time_elapsed() * _speed);
	span += (int)(remainder);
	remainder -= (int)(remainder);
	
	for (int i=0; i<span; i++)
	{
		switch (_dir)
		{
			case DIR_LEFT:
				if (!move_left())
				{
					stop();
					return false;
				}
			break;
			case DIR_RIGHT:
				if (!move_right())
				{
					stop();
					return false;
				}
			break;
			case DIR_UP:
				if (!move_up())
				{
					stop();
					return false;
				}
			break;
			case DIR_DOWN:
				if (!move_down())
				{
					stop();
					return false;
				}
			break;
			default:
			break;
		}
	}
	return true;
}

void GameObject::fall()
{
	if (!falling && !flying)
	{	
		falling = true;
		z = Z_FALLING;
		fall_countdown = 1.0f;
		if (get_type() != CORPSE_PART  &&  get_type() != EXTRA)
		{
			PLAY_PAN(Resources::Game_deepfall());
		}
	}
}

void GameObject::continue_falling()
{
	speed=(int)(fall_countdown*60);
	fall_countdown -= Timer::time_elapsed();
	if (fall_countdown < 0)
	{
		fallen_down = true;
	}
}

void GameObject::stop()
{
	stopped = true;
}

void GameObject::snap()
{
	x = ((get_x()+20)/40) *40;
	y = ((get_y()+20)/40) *40;
}

void GameObject::set_dir ( Direction _dir)
{
	cur_dir = _dir;
}

void GameObject::set_pos( int _x, int _y )
{
	x = _x;
	y = _y;
}

void GameObject::set_orig( int _x, int _y )
{
	orig_x = _x;
	orig_y = _y;
}

void GameObject::move_pos( int _x, int _y )
{
	x += _x;
	y += _y;
}

int GameObject::get_x() const
{
	return (int)x;
}

int GameObject::get_y() const
{
	return (int)y;
}

int GameObject::get_z() const
{
	return z;
}

int GameObject::get_speed() const
{
	return speed;
}

int GameObject::get_map_x() const
{
	return (get_x()+20)/40;
}

int GameObject::get_map_y() const
{
	return (get_y()+20)/40;
}

void GameObject::inc_speed( int _c )
{
	speed += _c;
}

void GameObject::dec_speed( int _c )
{
	speed -= _c;
}

void GameObject::set_speed( int _speed )
{
	speed = _speed;
}

bool GameObject::is_flying() const
{
	return flying;
}

bool GameObject::is_stopped() const
{
	return stopped;
}

Direction GameObject::get_cur_dir() const
{
	return cur_dir;
}

int GameObject::whats_left()
{
	return app->map->get_maptile_xy( int(x)-1,int(y)+20 )->get_type();
}

int GameObject::whats_right()
{
	return app->map->get_maptile_xy( int(x)+40, int(y)+20 )->get_type();
}

int GameObject::whats_up()
{
	return app->map->get_maptile_xy( int(x)+20, int(y)-1 )->get_type();
}

int GameObject::whats_down()
{
	return app->map->get_maptile_xy( int(x)+20, int(y)+40 )->get_type();
}

MapTile* GameObject::get_maptile() const
{
	return app->map->get_maptile_xy( int(x)+20, int(y)+20 );
}

void GameObject::show()
{	
	if (falling)
	{
		show(int(x)+60,int(y)+40,fall_countdown);
	}
	else if (flying)
	{
		float scalefactor = sinf(fly_progress*3.14f) * (1.3f/fly_speed) + 1;
		surface->put_screen( int(x+offset_x+20-scalefactor*20), int(y+offset_y+40-scalefactor*40), scalefactor, scalefactor, sprite_nr );
	}
	else
	{
		surface->put_screen( int(x+offset_x), int(y+offset_y), sprite_nr );
	}
}

void GameObject::show(int _x, int _y)
{
	surface->put_screen( _x, _y-40+offset_y, sprite_nr );
}

void GameObject::show(int _x, int _y, float _scale)
{
	surface->put_screen( int(20+_x-20*_scale), int(_y+20+((-40+offset_y)*_scale)-20*_scale), _scale, _scale, sprite_nr );
}

bool GameObject::is_falling()
{
	return (falling && !fallen_down);
}
