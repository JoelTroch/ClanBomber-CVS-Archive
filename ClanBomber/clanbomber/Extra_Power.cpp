/***************************************************************************
                          Extra_Power.cpp  -  description
                             -------------------
    begin                : ?
    copyright            : (C) 1999 by Andreas Hundt, Denis Oliver Kropp
    email                : clanbomber@fischlustig.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
// $Id: Extra_Power.cpp,v 1.1 2001/08/08 21:46:14 der_fisch Exp $

#include <ClanLib/Sound/soundbuffer.h>


#include "ClanBomber.h"
#include "Extra_Power.h"

#include "Bomber.h"

Extra_Power::Extra_Power( int _x, int _y, ClanBomberApplication *_app ) : Extra( _x, _y, _app )
{
	sprite_nr = 1;
}


Extra_Power::~Extra_Power()
{
}

void Extra_Power::effect( Bomber* bomber )
{
	bomber->loose_disease();
	bomber->gain_extra_power();
	PLAY_PAN(Resources::Extras_wow());
}























