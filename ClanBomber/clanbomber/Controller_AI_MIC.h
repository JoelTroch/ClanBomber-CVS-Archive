#ifndef Controller_AI_MiC_h
#define Controller_AI_MiC_h

#define random(num)  (rand() % (num))

#include "Controller.h"

class Explosion;
class ClanBomberApplication;
class Bomber;

class Controller_AI_MiC: public Controller
{
public:
  Controller_AI_MiC();
  virtual ~Controller_AI_MiC();
  void init_explosions_timer();
  void put_explosions_timer_on_top(int mapx, int mapy, int index_of_bomb);
  int get_explosions_stack_index(int mapx, int mapy, int id_of_bomb);
  void update_bomb_timer(int mapx, int mapy, int id_of_bomb, float update_timer);
  void update_explosions_timer();
  void compute_ground_rating();
  void rate_around(int **pground);
  bool feasible_dir(int bx, int by, int dir);
  bool is_opposite_dir(int dir);
  bool bomber_pos_will_change();
  bool could_go_on_with_same_direction();
  bool bomber_pos_has_changed();
  bool is_foreign_bomber_at(int posx, int posy);
  bool is_friendly_bomber_at(int posx, int posy);
  bool would_destroy_extras_at(int mapx, int mapy);
  int get_direction();
  int get_extra_rating(int extra_x, int extra_y, int bomber_x, int bomber_y);
  int get_extra_rating(int extra_x, int extra_y, int edge1_x, int edge1_y, int bomber_x, int bomber_y);
  int get_box_number(int mapx, int mapy);
  int get_detonation_rating(int i, int j);
  bool there_are_my_bombs_on_the_ground();
  void reset();
  void update();
  void init_non_ground();
  void init_exploding_boxes();
  bool extraAt(int i, int j);
  bool diseaseAt(int i, int j);
  bool is_very_dangerous(int i, int j);
  Explosion* get_explosion(int i, int j);
  void add_bomb_to_ground(int i, int j);
  bool try_bombing(int i, int j);
	bool there_are_wise_moves(int i, int j);
  void init_paths();
  bool same_positions(int x1, int y1, int x2, int y2);
	int get_ground_rating(int dir, int mapx, int mapy);
  bool can_move_in_time(float countdown);
  bool can_move_over_distance(int distance, float countdown);
  bool is_left();
  bool is_right();
  bool is_up();
  bool is_down();
  bool is_bomb();
  int get_direction_rating(int dir, int mapx, int mapy, int **pground);
private:
  int direction;
  int last_dir_rating;
	int last_bomber_posx;
  int last_bomber_posy;
  bool bombing;
  bool flight_from_bomb;
  bool simulation_mode_on;
  bool sure_directions_after_bombing[4];
  bool adding_bomb_to_ground;
  bool checking_wise_moves;
  int **ground;
  bool **exploding_boxes;
  struct explosions_stack
	{
    float timer;
    bool is_bomb;
    int bomb_id;
	};
	struct explosions
	{
    int max;
    explosions_stack stack[7];
    bool has_bomb;
	};
	explosions **explosions_timer;
  struct PosXY
  {
    int posx;
    int posy;
  };
  PosXY paths[4];
  bool getting_paths;
};//Controller_AI_MiC

#endif









