/***************************************************************************
                          Map.cpp  -  description
                             -------------------
    begin                : ?
    copyright            : (C) 1999 by Andreas Hundt, Denis Oliver Kropp
    email                : clanbomber@fischlustig.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
// $Id: Map.cpp,v 1.4 2004/01/08 18:10:39 der_fisch Exp $

#include <ClanLib/Core/System/error.h>
#include <ClanLib/Core/System/cl_assert.h>
#include <ClanLib/Display/Display/surface.h>
#include <ClanLib/Display/Font/font.h>
#include <fstream>
#include <cstdio>
#include <cstring>

#include "ClanBomber.h"
#include "Map.h"

#include "MapTile.h"
#include "MapTile_Arrow.h"
#include "MapTile_Trap.h"
#include "MapTile_Wall.h"
#include "MapEntry.h"
#include "Config.h"
#include "Bomb.h"
#include "Bomber.h"

#ifdef WIN32
	#include <io.h>
	#define strcasecmp strcmpi
#else
	#include <dirent.h>
#endif

CL_List<MapEntry> Map::map_list;

Map::Map( ClanBomberApplication* _app )
{
	app = _app;
	dummy_maptile = new MapTile_Wall(1000,1000,app);

	for (int y=0; y<MAP_HEIGHT; y++)
	{
		for (int x=0; x<MAP_WIDTH; x++)
		{
			maptiles[x][y] = NULL;
		}
	}
	current_map = NULL;
	
	enum_maps();
	sort_maps();
	load_selection();

	maps = new CL_Iterator<MapEntry>(map_list);
}

Map::~Map()
{
	delete maps;
}

void Map::sort_maps()
{
	MapEntry **mapss = new MapEntry*[map_list.get_num_items()];
	int num = map_list.get_num_items();

	int i;	// MSVC really should know the new ANSI standard!

	for (i=0; i<num; i++)
	{
		mapss[i] = map_list[i];
	}

	bool found = true;
	while (found)
	{
		found = false;
		for (i=0; i<num-1; i++)
		{
			if (strcasecmp( mapss[i]->get_name(), mapss[i+1]->get_name() )  > 0)
			{
				MapEntry *temp = mapss[i];
				mapss[i] = mapss[i+1];
				mapss[i+1] = temp;
				found = true;
			}
		}
	}

	map_list.clear();
	for (i=0; i<num; i++)
	{
		map_list.add( mapss[i] );
	}

	delete[] mapss;
}

#ifdef WIN32

void Map::enum_maps()
{
	if (map_list.get_num_items())
	{
		return;
	}

	long find_handle;
	_finddata_t map_file;

	find_handle = _findfirst( "maps/*.map", &map_file );

	if (find_handle != -1L)
	{
		map_list.add( new MapEntry( "maps", map_file.name ) );

		while ( _findnext( find_handle, &map_file ) == 0 )
		{
			map_list.add( new MapEntry( "maps", map_file.name ) );
		}
	} else
	{
		throw CL_Error("Could not find any map files!");
	}
}

#else

void Map::enum_maps()
{
	if (map_list.get_num_items())
	{
		return;
	}

	DIR		*dir;
	dirent	*entry;
	
	CL_String path = app->get_map_path();
	dir = opendir( path );
	if (!dir)
	{
		throw CL_Error("Could not find any map files!");
	}
	while ( (entry = readdir(dir)) != NULL )
	{
		CL_String s(entry->d_name);
		
		if ( s.right(4) == ".map" )
		{
			map_list.add( new MapEntry( path, s ) );
		}
	}
	closedir( dir );
	
	path = app->get_local_map_path();
	dir = opendir( path );
	if (dir)
	{
		while ( (entry = readdir(dir)) != NULL )
		{
			CL_String s(entry->d_name);
			
			if ( s.right(4) == ".map" )
			{
				map_list.add( new MapEntry( path, s ) );
			}
		}
	}
}

#endif

bool Map::any_valid_map()
{
	for (int i=0; i<get_map_count(); i++)
	{
		if (Config::get_number_of_players() <= map_list[i]->get_max_players()  && map_list[i]->is_enabled())
		{
			return true;
		}
	}
	return false;
}

int Map::get_map_count()
{
	return map_list.get_num_items();
}

void Map::spin_up()
{
	current_map = maps->next();
	if (!current_map)
	{
		current_map = maps->next();
	}
}

void Map::spin_down()
{
	current_map = maps->prev();
	if (!current_map)
	{
		current_map = maps->prev();
	}
}

void Map::spin_to(int nr)
{
	cl_assert( nr >= 0 );
	cl_assert( nr < get_map_count() );
	
	current_map = maps->first();
	
	for (int i=0; i<nr; i++)
	{
		current_map = maps->next();
	}
}


void Map::load_random_valid()
{
	do
	{
		spin_to(rand()%get_map_count());
	} while (Config::get_number_of_players() > get_max_players()  || !current_map->is_enabled());
	reload();
}


void Map::load_next_valid(int map_nr)
{
	if (map_nr != -1)
	{
		spin_to(map_nr);
	} else
	{
		spin_up();
	}
	
	while (Config::get_number_of_players() > get_max_players()  || !current_map->is_enabled())
	{
		spin_up();
	}
	
	reload();
}


void Map::load_previous_valid(int map_nr)
{
	if (map_nr != -1)
	{
		spin_to(map_nr);
	} else
	{
		spin_down();
	}
	
	while (Config::get_number_of_players() > get_max_players()  || !current_map->is_enabled())
	{
		spin_down();
	}
	
	reload();
}

void Map::load( int map_nr )
{
	spin_to(map_nr);
	reload();
}
	
void Map::reload()
{
	current_map->read_bomber_positions(); // needed by MapEditor
	clear();
	
	for (int y=0; y<MAP_HEIGHT; y++)
	{
		for (int x=0; x<MAP_WIDTH; x++)
		{
			switch (current_map->get_data(x,y))
			{
				case 48:
				case 49:
				case 50:
				case 51:
				case 52:
				case 53:
				case 54:
				case 55:
				case ' ':
					maptiles[x][y] = MapTile::create(MapTile::GROUND, 40*x, 40*y, app);
					break;
				case '*':
					maptiles[x][y] = MapTile::create(MapTile::WALL, 40*x, 40*y, app);
					break;
				case '+':
					maptiles[x][y] = MapTile::create(MapTile::BOX, 40*x, 40*y, app);
					break;
				case 'R':
					if ( rand() %3)
					{
						maptiles[x][y] = MapTile::create(MapTile::BOX, 40*x, 40*y, app);
					}
					else
					{
						maptiles[x][y] = MapTile::create(MapTile::GROUND, 40*x, 40*y, app);
					}					
					break;
				case 'S':
					maptiles[x][y] = MapTile::create( MapTile::ICE, 40*x, 40*y, app);
					break;
				case '<':
					maptiles[x][y] = new MapTile_Arrow(40*x, 40*y, DIR_LEFT,app);
					break;
				case '>':
					maptiles[x][y] = new MapTile_Arrow(40*x, 40*y, DIR_RIGHT,app);
					break;
				case '^':
					maptiles[x][y] = new MapTile_Arrow( 40*x, 40*y, DIR_UP,app);
					break;
				case 'v':
					maptiles[x][y] = new MapTile_Arrow( 40*x, 40*y, DIR_DOWN,app);
					break;
				case 'o':
					maptiles[x][y] = new MapTile_Trap( 40*x, 40*y, app);
					break;
				default:
					maptiles[x][y] = MapTile::create(MapTile::NONE, 40*x, 40*y, app);
					break;
			}
		}
	}
}

void Map::show()
{
	for (int y=0; y<MAP_HEIGHT; y++)
	{
		for (int x=0; x<MAP_WIDTH; x++)
		{
			if (maptiles[x][y]->get_type()!=MapTile::NONE)
			{
				if (is_shaken > 0)
				{
					if (maptiles[x][y])
					{
						maptiles[x][y]->draw_shaken();
					}
				}
				else
				{
					if (maptiles[x][y])
					{
						maptiles[x][y]->draw();
					}
				}
			}
		}
	}
	if (is_shaken > 0)	
	{
		is_shaken--;
	}
}

void Map::show_random_boxes()
{
	for (int y=0; y<MAP_HEIGHT; y++)
	{
		for (int x=0; x<MAP_WIDTH; x++)
		{
			if (current_map->get_data(x,y) == 'R')
			{
				Resources::Game_maptile_addons()->put_screen( 60+x*40, 40+y*40, 5 );
			}
		}
	}
}

void Map::show_start_positions()
{
	for (int i=0; i<current_map->get_max_players(); i++)
	{
		CL_Vector pos = current_map->get_bomber_pos(i);
		Resources::Game_maptile_addons()->put_screen( 60 + int(pos.x*40), 40+int(pos.y*40), 6 );
		
		CL_String number = i+1;
		Resources::Font_small()->print_center( 84 + int(pos.x*40), 52 + int(pos.y*40), number );
	}
}

void Map::show_preview( int x, int y, float factor )
{
	int tile_size = (int)(40*factor);
	for (int yy=0; yy<MAP_HEIGHT; yy++)
	{
		for (int xx=0; xx<MAP_WIDTH; xx++)
		{
			if (maptiles[xx][yy])
			{
				maptiles[xx][yy]->draw_tiny( x+xx*tile_size, y+yy*tile_size, factor );
			}
		}
	}
}

void Map::refresh_holes()
{
	for (int y=0; y<MAP_HEIGHT; y++)
	{
		for (int x=0; x<MAP_WIDTH; x++)
		{
			if (maptiles[x][y]->get_type()==MapTile::NONE)
			{
				if (is_shaken > 0)
				{
					if (maptiles[x][y])
					{
						maptiles[x][y]->draw_shaken();
					}
				}
				else
				{
					if (maptiles[x][y])
					{
						maptiles[x][y]->draw();
					}
				}
			}
		}
	}
}

void Map::act()
{
	int y;


	for (y=0; y<MAP_HEIGHT; y++)
	{
		for (int x=0; x<MAP_WIDTH; x++)
		{
			maptiles[x][y]->objects.clear();
			maptiles[x][y]->bomb = NULL;
		}
	}


	CL_Iterator<Bomber> bomber_object_counter(app->bomber_objects);
	while (bomber_object_counter.next() != NULL)
	{
		if (!bomber_object_counter()->is_flying() && !bomber_object_counter()->is_dead())
		{
			bomber_object_counter()->get_maptile()->objects.add(bomber_object_counter());
		}
	}


	CL_Iterator<GameObject> object_counter(app->objects);
	while (object_counter.next() != NULL)
	{
		if (!object_counter()->is_flying())
		{
			object_counter()->get_maptile()->objects.add(object_counter());
			if (object_counter()->get_type() == GameObject::BOMB && !(object_counter()->is_falling()))
			{
				object_counter()->get_maptile()->bomb = (Bomb*)(object_counter());
			}
		}
	}


	for (y=0; y<MAP_HEIGHT; y++)
	{
		for (int x=0; x<MAP_WIDTH; x++)
		{
			maptiles[x][y]->act();
			if (maptiles[x][y]->delete_me)
			{
				MapTile* new_tile;
				new_tile = MapTile::spawn( maptiles[x][y] );
				delete maptiles[x][y];
				maptiles[x][y] = new_tile;
			}
		}
	}
}


void Map::shake( int _shake )
{
	is_shaken = Config::get_shaky_explosions()?_shake:0;
}


void Map::clear()
{
	for (int y=0; y<MAP_HEIGHT; y++)
	{
		for (int x=0; x<MAP_WIDTH; x++)
		{
			if (maptiles[x][y])
			{
				delete maptiles[x][y];
				maptiles[x][y] = NULL;
			}
		}
	}
	is_shaken = 0;
}

bool Map::add_bomb(int bx, int by, int bpower, Bomber *bomber)
{
	if ((bx<0) || (by<0) || (bx>=MAP_WIDTH) || (by>=MAP_HEIGHT))
	{
		return false;
	}
	
	if (maptiles[bx][by] == NULL)
	{
		return false;
	}

	if (maptiles[bx][by]->bomb == NULL)
	{
		maptiles[bx][by]->bomb = new Bomb( bx*40, by*40,bpower, bomber, app );
		return true;
	}

	return false;
}

CL_String Map::get_name()
{
	cl_assert( current_map != NULL );
	
	return current_map->get_name();
}

CL_String Map::get_author()
{
	cl_assert( current_map != NULL );
	
	return current_map->get_author();
}

CL_Vector Map::get_bomber_pos(int nr)
{
	cl_assert( current_map != NULL );
	
	return current_map->get_bomber_pos(nr);
}

void Map::randomize_bomber_positions()
{
	cl_assert( current_map != NULL );
	
	current_map->randomize_bomber_positions();
}

int Map::get_max_players()
{
	cl_assert( current_map != NULL );
	
	return current_map->get_max_players();
}


MapTile* Map::get_passable()
{
	int i =0;
	int xmapfield;
	int ymapfield;
	do
	{
		xmapfield = rand() % 17;
		ymapfield = rand() % 13;
		if (maptiles[xmapfield][ymapfield]->is_passable() && !maptiles[xmapfield][ymapfield]->is_vanishing())
		{
				return maptiles[xmapfield][ymapfield];				
		}
		i++;
	} while (i<1000);

	return NULL;
}

MapTile* Map::get_random()
{
	int i =0;
	int xmapfield;
	int ymapfield;
	do
	{
		xmapfield = rand() % 17;
		ymapfield = rand() % 13;
		if (maptiles[xmapfield][ymapfield]->get_type()!=MapTile::NONE && !maptiles[xmapfield][ymapfield]->is_vanishing())
		{
				return maptiles[xmapfield][ymapfield];				
		}
		i++;
	} while (i<1000);

	return NULL;
}


MapTile* Map::get_maptile(int x, int y)
{
	if (x < 0 || x > MAP_WIDTH-1 || y < 0 || y > MAP_HEIGHT-1)
	{
		return dummy_maptile;
	}
	else
	{
		return maptiles[x][y];
	}
}

MapTile* Map::get_maptile_xy(int x, int y)
{
	if (x < 0 || x >= (MAP_WIDTH)*40  || y < 0 || y >= (MAP_HEIGHT)*40 )
	{
		return dummy_maptile;
	}
	else
	{
		return maptiles[x/40][y/40];
	}
}


int Map::new_entry( CL_String _name )
{
	try
	{
		map_list.add( new MapEntry(_name) );
		return map_list.get_num_items() -1;
	}
	catch( CL_Error err )
	{
		delete map_list.get_last();
		map_list.del(map_list.get_last());
		throw err;
	}
}

int Map::delete_entry( int nr )
{
	cl_assert( nr >= 0 );
	cl_assert( nr < get_map_count() );
	
	map_list[nr]->delete_file();
	delete map_list[nr];
	map_list.del(map_list[nr]);
	
	if (nr > get_map_count()-1)
	{
		return nr-1;
	}
	return nr;
}

ostream& operator << (ostream& os, Map& m)
{
	for (int y=0; y<MAP_HEIGHT; y++)
	{
		for (int x=0; x<MAP_WIDTH; x++)
		{
			if (m.maptiles[x][y]->bomb)
			{
				os << "*";
			} else
			if (m.maptiles[x][y]->has_bomber())
			{
				os << "B";
			} else
			if (m.maptiles[x][y]->has_extra())
			{
				os << "e";
			} else
			{
				switch (m.maptiles[x][y]->get_type())
				{
					case MapTile::NONE:
						os << "-";
						break;
					case MapTile::GROUND:
						os << " ";
						break;
					case MapTile::WALL:
						os << "#";
						break;
					case MapTile::BOX:
						os << "+";
						break;
					case MapTile::TRAP:
						os << " ";
						break;
					case MapTile::ARROW:
						os << " ";
						break;
					case MapTile::ICE:
						os << " ";
						break;
				}
			}
			os << " ";
		}
		os << endl;
	}
	return os;
}


void Map::save_selection()
{
	CL_String filename;
#ifdef WIN32
	filename = "maps.disabled";
#else
	filename = getenv("HOME");
	filename += "/.clanbomber/maps.disabled";
#endif
	ofstream map_selection_file(filename);
	CL_Iterator<MapEntry> map_entry_iterator(map_list);
	while (map_entry_iterator.next()!=NULL)
	{
		if (!(map_entry_iterator()->is_enabled()))
		{
			map_selection_file << map_entry_iterator()->get_name() << endl;
		}
	}	
}

void Map::load_selection()
{
	CL_String filename;
#ifdef WIN32
	filename = "maps.disabled";
#else
	filename = getenv("HOME");
	filename += "/.clanbomber/maps.disabled";
#endif

	FILE* map_selection_file;
	char map_name[100];

	if ((map_selection_file = fopen( filename, "r" )) != NULL)
	{
		while (!(feof(map_selection_file)))
		{
			fscanf( map_selection_file, "%s\n", map_name );
			CL_Iterator<MapEntry> map_entry_iterator(map_list);
			while (map_entry_iterator.next()!=NULL)
			{
				if (map_entry_iterator()->get_name() == map_name)
				{
					map_entry_iterator()->disable();
				}	
			}
		}
		fclose(map_selection_file);
	}
}




