/***************************************************************************
                   Controller_Keyboard.cpp  -  description
                             -------------------
    begin                : ?
    copyright            : (C) 1999 by Andreas Hundt, Denis Oliver Kropp
    email                : clanbomber@fischlustig.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
// $Id: Controller_Keyboard.cpp,v 1.2 2001/09/22 16:16:04 der_fisch Exp $

#include <ClanLib/Display/Input/keyboard.h>
#include <ClanLib/Display/Input/key.h>

#include "ClanBomber.h"
#include "Controller_Keyboard.h"

static int mandrake_keys = -1;

Controller_Keyboard::Controller_Keyboard(int keymap_nr) : Controller()
{
	if (mandrake_keys == -1)
	{
		mandrake_keys = getenv("MANDRAKE_KEYS") ? 1 : 0;
	}

	if (mandrake_keys == 0)
	{		
		switch (keymap_nr)
		{
		case 0:
			left_key = CL_KEY_LEFT;
			right_key = CL_KEY_RIGHT;
			up_key = CL_KEY_UP;
			down_key = CL_KEY_DOWN;
			bomb_key = CL_KEY_ENTER;
			break;
		case 1:
			left_key = CL_KEY_A;
			right_key = CL_KEY_D;
			up_key = CL_KEY_W;
			down_key = CL_KEY_S;
			bomb_key = CL_KEY_TAB;
			break;
		case 2:
			left_key = CL_KEY_J;
			right_key = CL_KEY_L;
			up_key = CL_KEY_I;
			down_key = CL_KEY_K;
			bomb_key = CL_KEY_SPACE;
			break;
		case 3:
			left_key = CL_KEY_4;
			right_key = CL_KEY_6;
			up_key = CL_KEY_8;
			down_key = CL_KEY_5;
			bomb_key = CL_KEY_0;
			break;
		}
	}
	else
	{		
		switch (keymap_nr)
		{
		case 0:
			left_key = CL_KEY_NUMLOCK;
			right_key = CL_KEY_KP_DIV;
			up_key = CL_KEY_PAGEUP;
			down_key = CL_KEY_PAGEDOWN;
			bomb_key = CL_KEY_RIGHT;
			break;
		case 1:
			left_key = CL_KEY_S;
			right_key = CL_KEY_D;
			up_key = CL_KEY_Q;
			down_key = CL_KEY_A;
			bomb_key = CL_KEY_ALT;
			break;
		case 2:
			left_key = CL_KEY_J;
			right_key = CL_KEY_L;
			up_key = CL_KEY_I;
			down_key = CL_KEY_K;
			bomb_key = CL_KEY_SPACE;
			break;
		case 3:
			left_key = CL_KEY_4;
			right_key = CL_KEY_6;
			up_key = CL_KEY_8;
			down_key = CL_KEY_5;
			bomb_key = CL_KEY_0;
			break;
		}
	}
	reset();
}

void Controller_Keyboard::update()
{
	if (CL_Keyboard::get_keycode(bomb_key) && !bomb_key_down)
	{
		put_bomb = true;
	} else
	{
		put_bomb = false;
	}
	bomb_key_down = CL_Keyboard::get_keycode(bomb_key);
}

void Controller_Keyboard::reset()
{
	put_bomb = false;
	bomb_key_down = true;
	reverse = false;
}

bool Controller_Keyboard::is_left()
{
	if (reverse)
		return CL_Keyboard::get_keycode( right_key ) && active;
		
	return CL_Keyboard::get_keycode( left_key ) && active;
}

bool Controller_Keyboard::is_right()
{
	if (reverse)
		return CL_Keyboard::get_keycode( left_key ) && active;
		
	return CL_Keyboard::get_keycode( right_key ) && active;
}

bool Controller_Keyboard::is_up()
{
	if (reverse)
		return CL_Keyboard::get_keycode( down_key ) && active;
		
	return CL_Keyboard::get_keycode( up_key ) && active;
}

bool Controller_Keyboard::is_down()
{
	if (reverse)
		return CL_Keyboard::get_keycode( up_key ) && active;
		
	return CL_Keyboard::get_keycode( down_key ) && active;
}

bool Controller_Keyboard::is_bomb()
{
	switch (bomb_mode)
	{
		case NEVER:
			return false;
		case ALWAYS:
			return true;
		default:
		break;
	}
	return put_bomb && active;
}



