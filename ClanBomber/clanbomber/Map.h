/***************************************************************************
                          Map.h  -  description
                             -------------------
    begin                : ?
    copyright            : (C) 1999 by Andreas Hundt, Denis Oliver Kropp
    email                : clanbomber@fischlustig.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
// $Id: Map.h,v 1.2 2001/11/12 02:44:10 xmassx Exp $

#ifndef Map_h
#define Map_h

#include "MapEntry.h"

class ClanBomberApplication;
class MapTile;

class Map
{
public:
	Map( ClanBomberApplication* _app );
	~Map();

	bool add_bomb( int bx, int by, int bpower, Bomber *bomber );
	void clear();
	void load(int map_nr );
	void load_random_valid();
	void load_next_valid(int map_nr = -1);
	void load_previous_valid(int map_nr = -1);
	void show();
	void show_preview( int x, int y, float factor );
	void show_random_boxes();
	void show_start_positions();
	void refresh_holes();
	void act();
	void shake( int _shake );
	void save_selection();
	void load_selection();
	MapTile* get_passable();
	MapTile* get_random();
	MapTile* get_maptile(int x, int y);
	MapTile* get_maptile_xy (int x, int y);

	CL_String get_name();
	CL_String get_author();
	int	get_max_players();
	
	CL_Vector get_bomber_pos(int nr);
	void randomize_bomber_positions();
	
	void reload();
	int get_map_count();
	bool any_valid_map();
	
	int new_entry( CL_String _name );
	int delete_entry( int nr );

	friend std::ostream& operator << (std::ostream&, Map& m);

	// static stuff
	static CL_List<MapEntry> map_list;
	MapTile* maptiles[MAP_WIDTH][MAP_HEIGHT];

protected:
	ClanBomberApplication* app;
	int is_shaken;
	
	MapTile* dummy_maptile; // this is a dummy maptile::wall for "clipping" in get_maptile(int x, int y)

	MapEntry *current_map;
	CL_Iterator<MapEntry> *maps;
	
	void enum_maps();
	void sort_maps();
	void spin_up();
	void spin_down();
	void spin_to(int nr);
};

#endif








