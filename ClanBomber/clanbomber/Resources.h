/***************************************************************************
                          FILENAME  -  description                              
                             -------------------                                         
    begin                : in 1999                                           
    copyright            : (C) 1999 by Andreas Hundt, Denis Oliver Kropp                         
    email                : clanbomber@fischlustig.de                                     
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   * 
 *                                                                         *
 ***************************************************************************/


#ifndef RESOURCES_H
#define RESOURCES_H

class CL_Font;
class CL_Surface;
class CL_SoundBuffer;
class CL_SoundBuffer_Session;
class CL_ResourceManager;

/**
  *@author Andreas Hundt, Denis Oliver Kropp
  */

class Resources
{
public: 
	Resources();
	~Resources();

	static void init();
	static void load_all();

	static CL_Font*		Font_big();
	static CL_Font*		Font_small();

	static CL_Surface*		Titlescreen();

	static CL_Surface*		Intro_fl_logo();
	static CL_SoundBuffer*	Intro_typewriter();
	static CL_SoundBuffer*	Intro_winlevel();

	static CL_Surface*		Playersetup_teams();
	static CL_Surface*		Playersetup_controls();
	static CL_Surface*		Playersetup_teamlamps();
	static CL_Surface*		Playersetup_background();

	static CL_Surface*		Mapselector_background();
	static CL_Surface*		Mapselector_not_available();
	
	static CL_Surface*		Gamestatus_tools();
	static CL_SoundBuffer*	Gamestatus_klatsch();
	static CL_Surface*		Gamestatus_background();

	static CL_SoundBuffer*	Credits_forward();
	static CL_SoundBuffer*	Credits_rewind();
	static CL_SoundBuffer*	Credits_stop();


	static CL_Surface*		Credits_horst_evil();
	
	static CL_Surface*		Bombers(int nr);

	static CL_Surface*		Extras_extras();
	static CL_SoundBuffer*	Extras_wow();
	static CL_SoundBuffer*	Extras_joint();
	static CL_SoundBuffer*	Extras_horny();
	static CL_SoundBuffer*	Extras_schnief();

	static CL_SoundBuffer*	Menu_whoosh();
	static CL_SoundBuffer*	Menu_break();
	static CL_SoundBuffer*	Menu_clear();
	static CL_SoundBuffer*	Menu_back();
	
	static CL_Surface*		Observer_observer();
	static CL_SoundBuffer*	Observer_hurry_up();
	static CL_SoundBuffer*	Observer_time_over();
	static CL_SoundBuffer*	Observer_crunch();

	static CL_SoundBuffer*	Game_die();
	static CL_SoundBuffer*	Game_explode();
	static CL_SoundBuffer*	Game_putbomb();
	static CL_SoundBuffer*	Game_deepfall();
	static CL_Surface*		Game_maptiles();
	static CL_Surface*		Game_maptile_addons();
	static CL_Surface*		Game_bombs();
	static CL_Surface*		Game_explosion();
	static CL_Surface*		Game_cb_logo_small();
	
	static CL_Surface*		MapEditor_background();

	static CL_SoundBuffer*	Corpse_explode();
	static CL_Surface*		Corpse_part();
	
	static CL_SoundBuffer*	Splash(int nr);

	static CL_ResourceManager *res;
	
protected:	
	static CL_Font *fnt_big;
	static CL_Font *fnt_small;

	static CL_Surface *sur_titlescreen;
	
	static CL_Surface *sur_fl_logo;
	static CL_SoundBuffer *sfx_typewriter;
	static CL_SoundBuffer *sfx_winlevel;
	
	static CL_Surface *sur_ps_teams;
	static CL_Surface *sur_ps_controls;
	static CL_Surface *sur_ps_teamlamps;
	static CL_Surface *sur_ps_background;
	
	static CL_Surface *sur_ls_background;
	static CL_Surface *sur_ls_not_available;
	
	static CL_Surface *sur_gs_tools;
	static CL_SoundBuffer *sfx_klatsch;
	static CL_Surface *sur_gs_background;
	
	static CL_SoundBuffer *sfx_forward;
	static CL_SoundBuffer *sfx_rewind;
	static CL_SoundBuffer *sfx_stop;

	static CL_Surface *sur_horst;
	static CL_Surface *sur_horst_evil;
	
	static CL_Surface *sur_bombers[8];
	
	static CL_Surface *sur_extras;
	static CL_SoundBuffer *sfx_wow;
	static CL_SoundBuffer *sfx_joint;
	static CL_SoundBuffer *sfx_horny;
	static CL_SoundBuffer *sfx_schnief;
	
	static CL_SoundBuffer *sfx_whoosh;
	static CL_SoundBuffer *sfx_break;
	static CL_SoundBuffer *sfx_clear;
	static CL_SoundBuffer *sfx_menu_back;
	
	static CL_Surface *sur_observer;
	static CL_SoundBuffer *sfx_hurry_up;
	static CL_SoundBuffer *sfx_time_over;
	static CL_SoundBuffer *sfx_crunch;
	
	static CL_SoundBuffer *sfx_die;
	static CL_SoundBuffer *sfx_explode;
	static CL_SoundBuffer *sfx_putbomb;
	static CL_SoundBuffer *sfx_deepfall;
	static CL_Surface *sur_maptiles;
	static CL_Surface *sur_maptile_addons;
	static CL_Surface *sur_bombs;
	static CL_Surface *sur_explosion;
	static CL_Surface *sur_cb_logo_small;

	static CL_Surface *sur_me_background;

	static CL_SoundBuffer *sfx_corpse_explode;
	static CL_Surface *sur_corpse_part;
	
	static CL_SoundBuffer *sfx_splash[2];
};

#endif










