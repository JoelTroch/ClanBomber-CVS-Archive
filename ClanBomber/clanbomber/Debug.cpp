/***************************************************************************
                          Debug.cpp  -  description
                             -------------------
    begin                : ?
    copyright            : (C) 1999 by Andreas Hundt, Denis Oliver Kropp
    email                : clanbomber@fischlustig.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
// $Id: Debug.cpp,v 1.1 2001/08/08 21:46:14 der_fisch Exp $

#include <ClanLib/Display/Input/keyboard.h>
#include <ClanLib/Display/Input/key.h>
#include <ClanLib/Display/Font/font.h>

#include "ClanBomber.h"
#include "Debug.h"

#include "Timer.h"
#include "Config.h"
#include "Controller.h"
#include "Map.h"
#include "Bomber.h"


Debug::Debug( int _x, int _y, ClanBomberApplication *_app ) : GameObject( _x, _y, _app )
{
	app->objects.add( this );
	current_map=1;
	key_F2 = false;
	key_F3 = false;
	key_divide = false;
	key_multiply = false;
}

Debug::~Debug()
{
}

void Debug::show()
{
}

void Debug::act()
{
	if (CL_Keyboard::get_keycode(CL_KEY_KP_MULT) && !key_multiply)
	{
		app->map->load_next_valid();
	}
	key_multiply = CL_Keyboard::get_keycode(CL_KEY_KP_MULT);
	
	if (CL_Keyboard::get_keycode(CL_KEY_KP_DIV) && !key_divide)
	{
		app->map->load_previous_valid();
	}
	key_divide = CL_Keyboard::get_keycode(CL_KEY_KP_DIV);
	
	if (CL_Keyboard::get_keycode(CL_KEY_F2) && !key_F2)
	{

		Bomber* bomber =new Bomber(((rand()%(MAP_WIDTH-2))+1)*40,((rand()%(MAP_HEIGHT-2))+1)*40,Bomber::GREEN, Controller::create(Controller::AI), "Debug Robi", 0,0, app);
//		bomber->fly_to(app->map->get_passable());
		bomber->controller->activate();
	}
	key_F2 = CL_Keyboard::get_keycode(CL_KEY_F2);
	
	if (CL_Keyboard::get_keycode(CL_KEY_F3) && !key_F3)
	{
		Bomber* bomber =new Bomber(((rand()%(MAP_WIDTH-2))+1)*40,((rand()%(MAP_HEIGHT-2))+1)*40,Bomber::RED, Controller::create(Controller::AI_MIC), "Debug Robi MIC", 0,0, app);
//		bomber->fly_to(app->map->get_passable());
		bomber->controller->activate();
	}
	key_F3 = CL_Keyboard::get_keycode(CL_KEY_F3);
}


























