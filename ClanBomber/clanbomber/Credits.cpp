/***************************************************************************
                          FILENAME  -  description                              
                             -------------------                                         
    begin                : in 1999                                           
    copyright            : (C) 1999 by Andreas Hundt, Denis Oliver Kropp                         
    email                : clanbomber@fischlustig.de                                     
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   * 
 *                                                                         *
 ***************************************************************************/

#include "ClanBomber.h"
#include "Config.h"

#include <ClanLib/Sound/soundbuffer_session.h>
#include <ClanLib/Display/Font/font.h>
#include <ClanLib/Display/Display/surface.h>
#include <ClanLib/Display/Display/display.h>
#include <ClanLib/Core/System/system.h>
#include <ClanLib/Sound/soundbuffer.h>
#include <ClanLib/Display/Input/keyboard.h>
#include <ClanLib/Display/Input/key.h>

#include "Credits.h"

#include "Corpse_Part.h"
#include "Timer.h"

#define SCROLL_SPEED 400

Credits::Credits( ClanBomberApplication *_app )
{
	app = _app;
	
	text.add( new CL_String( "ClanBomber Credits" ) );
	text.add( new CL_String( "" ) );
	text.add( new CL_String( "Game Design" ) );
	text.add( new CL_String( "Andreas Hundt" ) );
	text.add( new CL_String( "Denis Oliver Kropp" ) );	
	text.add( new CL_String( "" ) );
	text.add( new CL_String( "Coding" ) );
	text.add( new CL_String( "Denis Oliver Kropp" ) );
	text.add( new CL_String( "Andreas Hundt" ) );
	text.add( new CL_String( "mass (oza stuf)" ) );
	text.add( new CL_String( "resix (AI MiC)" ) );
	text.add( new CL_String( "" ) );
	text.add( new CL_String( "Graphics" ) );
	text.add( new CL_String( "Denis Oliver Kropp" ) );
	text.add( new CL_String( "Andreas Hundt" ) );
	text.add( new CL_String( "" ) );
	text.add( new CL_String( "Thanks go out to:" ) );
	text.add( new CL_String( "Magnus Norddahl" ) );
	text.add( new CL_String( "(24h ClanLib support)" ) );
	text.add( new CL_String( "Martin Pitt" ) );
	text.add( new CL_String( "(Debian package maintainer)" ) );
	text.add( new CL_String( "Fredrik Hallenberg" ) );
	text.add( new CL_String( "(former Debian package maintainer)" ) );
	text.add( new CL_String( "non" ) );
	text.add( new CL_String( "(for creating horst (the guy to the background))" ) );
	text.add( new CL_String( "clanner" ) );
	text.add( new CL_String( "(for creating maps and playing with us)" ) );
	text.add( new CL_String( "Ivar" ) );
	text.add( new CL_String( "(for creating maps)" ) );
	text.add( new CL_String( "Magnus Reftel" ) );
	text.add( new CL_String( "(disable shaky explosions patch)" ) );
	text.add( new CL_String( "the xtux creators" ) );
	text.add( new CL_String( "(for the original tux and bsd-devil graphics)" ) );
	text.add( new CL_String( "SuSE Linux AG" ) );
	text.add( new CL_String( "(for donating free SuSE Linux Professionial packages)" ) );
	text.add( new CL_String( "" ) );
 	text.add( new CL_String( "" ) ); 	
	text.add( new CL_String( "Everyone else supporting this game..." ) );
	text.add( new CL_String( "... and playing it" ) );
	
	yoffset = 50;

	ss_forward = Resources::Credits_forward()->prepare();
	ss_forward.stop(); // workaround
	ss_forward.set_looping(true);

	ss_rewind = Resources::Credits_rewind()->prepare();
	ss_rewind.stop(); // workaround
	ss_rewind.set_looping(true);
}

Credits::~Credits()
{
	for (int i=0; i<text.get_num_items(); i++)
	{
		delete text[i];
	}
}

void Credits::exec()
{
	float t = 0;

	Timer::reset();
	
	draw();
	while (CL_Keyboard::get_keycode(CL_KEY_ENTER))
	{
		CL_System::keep_alive();
	}
	
	while (1)
	{
		CL_System::keep_alive();
		
		CL_Iterator<GameObject> counter(app->objects);
		if (CL_Keyboard::get_keycode(CL_KEY_ESCAPE))
		{
			while (counter.next() != NULL)
			{
				delete counter();
				counter.remove();
			}
			return;
		}
		
		if (CL_Keyboard::get_keycode(CL_KEY_UP))
		{
			if (!ss_rewind.is_playing() && !stopped)  // workaround for looping
			{
//				ss_rewind.set_position(0);
				ss_rewind.play();
			}
			speed = -SCROLL_SPEED;
		} else
		if (CL_Keyboard::get_keycode(CL_KEY_DOWN))
		{
			if (!ss_forward.is_playing() && !stopped)  // workaround for looping
			{
//				ss_forward.set_position(0);
				ss_forward.play();
			}
			speed = SCROLL_SPEED;
		} else
		{
			ss_forward.stop();
			ss_rewind.stop();
			speed = 40;
		}
		
		yoffset -= speed * Timer::time_elapsed(true);
		if (yoffset > 50)
		{
			if (speed != 20 && !Config::get_kids_mode())
			{
				t += Timer::time_elapsed();
				for (; t>0.04f; t-=0.04f)
				{
					Corpse_Part* cp = new Corpse_Part( rand()%800-60, -40, app );
					cp->fly_to( rand()%800-60, 540 );
					Resources::Splash(rand()%2)->play();
				}
			}
			yoffset = 50;
			speed = 20;

			if (!stopped)
			{
				Resources::Credits_stop()->play();
				ss_rewind.stop();
			}
			stopped = true;
		} else
		if (yoffset < -text.get_num_items()*40 + 100)
		{
			if (speed != 40 && !Config::get_kids_mode())
			{
				t += Timer::time_elapsed();
				for (; t>0.04f; t-=0.04f)
				{
					Corpse_Part* cp = new Corpse_Part( rand()%800-60, -40, app );
					cp->fly_to( rand()%800-60, 540 );
					Resources::Splash(rand()%2)->play();
				}
			}
			yoffset = -text.get_num_items()*40 + 100;

			if (!stopped)
			{
				Resources::Credits_stop()->play();
				ss_forward.stop();
			}
			stopped = true;
		} else
		{
			stopped = false;
		}
				
		// Let them do their stuff
		CL_Iterator<GameObject> object_counter(app->objects);
		while (object_counter.next() != NULL)
		{
			object_counter()->act();
		}
		// Check if we should delete some
		while (object_counter.next() != NULL)
		{
			if (object_counter()->delete_me)
			{
				delete object_counter();
				object_counter.remove();
			}
		}
		
		draw();
	}
}

void Credits::draw()
{
	//CL_Display::clear_display();


	Resources::Credits_horst_evil()->put_screen(0,0);
	
	Resources::Game_cb_logo_small()->put_screen( 10, (int)yoffset + 10 );
	Resources::Game_cb_logo_small()->put_screen( 640, (int)yoffset + 10 );
	
	Resources::Intro_fl_logo()->put_screen( 100, (int)yoffset + text.get_num_items()*40 + 130 );
	
	for (int i=0; i<text.get_num_items(); i++)
	{
		Resources::Font_big()->print_center( 400, (int)yoffset + i*40 + 60, *text[i] );
	}
	
	if (yoffset < -10)
	{
		Resources::Font_big()->print_left( 10, 9, "+" );
		Resources::Font_big()->print_left( 783, 9, "+" );
	}
	if (yoffset > -text.get_num_items()*40 + 350)
	{
		Resources::Font_big()->print_left( 10, 560, "-" );
		Resources::Font_big()->print_left( 783, 560, "-" );
	}
	
	draw_objects();
	
	CL_Display::flip_display();
}

void Credits::draw_objects()
{
	GameObject** draw_list;
	draw_list = new GameObject*[app->objects.get_num_items()];

	int n = 0;
	int i;

	CL_Iterator<GameObject> object_counter(app->objects);
	while (object_counter.next() != NULL)
	{
		draw_list[n] = object_counter();
		n++;
	}
	
	bool sort = true;
	GameObject* obj;
	while(sort)
	{
		sort = false;
		for( i=0; i<n-1; i++ )
		{
			if (draw_list[i]->get_z() > draw_list[i+1]->get_z())
			{
				obj = draw_list[i];
				draw_list[i] = draw_list[i+1];
				draw_list[i+1] = obj;
				sort = true;
			}
		}
	}

	for( i=0; i<n; i++ )
	{
		draw_list[i]->show();
	}
}
