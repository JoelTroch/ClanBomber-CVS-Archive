/***************************************************************************
                          Observer.cpp  -  description
                             -------------------                                         
    begin                : Sat Mar 6 1999                                           
    copyright            : (C) 1999 by Andreas Hundt, Denis Oliver Kropp                         
    email                : clanbomber@fischlustig.de                                     
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   * 
 *                                                                         *
 ***************************************************************************/
// $Id: Observer.cpp,v 1.2 2004/01/08 18:10:39 der_fisch Exp $

#include <ClanLib/Sound/soundbuffer.h>
#include <ClanLib/Display/Display/display.h>
#include <ClanLib/Display/Display/surface.h>
#include <ClanLib/Display/Font/font.h>

#include "ClanBomber.h"
#include "Observer.h"

#include "Config.h"
#include "Timer.h"
#include "GameStatus_Team.h"
#include "Map.h"
#include "MapTile.h"
#include "Controller.h"

#include <math.h>

Observer::Observer( int _x, int _y, ClanBomberApplication *_app ) : GameObject( _x, _y, _app )
{
	x=0;
	y=0;
	z = Z_OBSERVER;
	end_of_game = false;

	surface = Resources::Observer_observer();
	sprite_nr= 0;
	offset_x=10;
	offset_y=2;
	speed = 300;
	CL_Iterator<Bomber> bomber_object_counter(app->bomber_objects);
	mode = NORMAL;
	while (bomber_object_counter.next() != NULL)
	{		
		if (bomber_object_counter()->get_team() != 0)
		{
			mode = TEAM;
		}
	}	
	if (mode == TEAM)
	{
		game_status = new GameStatus_Team (_app);
	}
	else
	{
		game_status = new GameStatus (_app);
	}
	
	reset_round_time();
	first_destruction = true;
}

Observer::~Observer()
{
	delete game_status;
}

bool Observer::end_of_game_requested()
{
	return end_of_game;
}

int Observer::active_players()	
{
	int c = 0;
	int t = -1;

	CL_Iterator<Bomber> bomber_object_counter(app->bomber_objects);
	while (bomber_object_counter.next() != NULL)
	{
		if (!bomber_object_counter()->is_dead())
		{
			if ((bomber_object_counter()->get_team() != t) || (bomber_object_counter()->get_team() == 0))
			{
				t = bomber_object_counter()->get_team();
				c++;
			}
		}
	}
	return c;
}

float Observer::get_round_time() const
{
	return round_time;
}

void Observer::reset_round_time()
{
	round_time=Config::get_round_time();
	play_hurryup = true;
	first_destruction = true;
	x = 0;
	y = 0;
	flying = false;
}

void Observer::act()
{
	GameObject::act();
	
	if (round_time == Config::get_round_time())
	{
		CL_Iterator<Bomber> bomber_object_counter(app->bomber_objects);
		while (bomber_object_counter.next() != NULL)
		{
			if (bomber_object_counter()->is_flying())
			{
				return;
			}
		}													
		while (bomber_object_counter.next() != NULL)
		{
			bomber_object_counter()->controller->activate();
		}													
	}
	
	round_time -= Timer::time_elapsed();
	if (round_time < 0)
	{
		offset_x = 60;
		offset_y = 40;
		if (!flying)
		{
			if (first_destruction)
			{
				Resources::Observer_time_over()->play();
			} else
			{
				get_maptile()->vanish();
				PLAY_PAN(Resources::Observer_crunch());
			}
			first_destruction = false;											

			MapTile *maptile = app->map->get_random();
			if (maptile)
			{
				fly_to(maptile);
			}
		}
	}
	if (active_players() < 2)
	{
		CL_Iterator<GameObject> object_counter(app->objects);
		while (object_counter.next() != NULL)
		{
			if (object_counter()->get_type() == GameObject::EXPLOSION)
			{
				return;
			}
			if (object_counter()->get_type() == GameObject::EXTRA && object_counter()->is_flying())
			{
				return;
			}
		}
		CL_Iterator<Bomber> bomber_object_counter(app->bomber_objects);
		while (bomber_object_counter.next() != NULL)
		{
			if (!bomber_object_counter()->is_dead()  &&  bomber_object_counter()->is_falling())
			{
				return;
			}
		}													

		offset_x = 10;
		offset_y = 2;
		
		while (bomber_object_counter.next() != NULL)
		{
			if (!bomber_object_counter()->is_dead())
			{
				bomber_object_counter()->inc_points();
			}
		}													

		if (Config::get_random_map_order())
		{
			app->map->load_random_valid();
		}else
		{
			app->map->load_next_valid();
		}		
		reset_round_time();
		game_status->analyze_game();
		game_status->show();
		Timer::reset();
		end_of_game = game_status->get_end_of_game(); //FIXME
		
		
		CL_Iterator<GameObject> object_counter2(app->objects);
		while (object_counter2.next() != NULL)
		{
			if (object_counter2()->get_type() != GameObject::DEBUG)
			{
				delete object_counter2();
				object_counter2.remove();
			}
		}

		CL_Vector pos;
		if (Config::get_random_positions())
		{
			app->map->randomize_bomber_positions();
		}
		int c = 0;		
		CL_Iterator<Bomber> bomber_object_counter2(app->bomber_objects);
		while (bomber_object_counter2.next() != NULL)
		{
			pos = app->map->get_bomber_pos(c++);
			bomber_object_counter2()->set_orig( int(pos.x*40), int(pos.y*40) );
			bomber_object_counter2()->reset();
			bomber_object_counter2()->set_pos( 350, 270 );
			bomber_object_counter2()->fly_to( int(pos.x*40), int(pos.y*40), 200 );
			bomber_object_counter2()->controller->deactivate();
		}
	}
}

void Observer::kill_all_bombers()
{
	CL_Iterator<Bomber> bomber_object_counter(app->bomber_objects);
	while (bomber_object_counter.next() != NULL)
	{		
		bomber_object_counter()->die();
	}	
}

void Observer::show()
{
	Resources::Game_cb_logo_small()->put_screen(325,0,0);
	GameObject::show();
	if (round_time >0)
	{
		Resources::Observer_observer()->put_screen (600,1,1);
		Resources::Font_big()->print_left( 650, 7, CL_String((int)round_time) );
	}
	else
	{
		Resources::Observer_observer()->put_screen (600,1,2);
	}		

		
	Resources::Font_big()->print_left( 80, 5, app->map->get_name() );
	
	if ((round_time < 21) && (round_time > 18))
	{
		if (fmod(round_time,0.3f) > 0.15f)
		{
			CL_Display::fill_rect( 300,250, 500,280, 0,0,0,0.5f );
			Resources::Font_big()->print_center( 400, 254, "HURRY UP !!!! ");
		}
		if (play_hurryup)
		{			
			Resources::Observer_hurry_up()->play();
			play_hurryup = false;
		}
	}
}
