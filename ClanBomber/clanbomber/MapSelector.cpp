/***************************************************************************
                          FILENAME  -  description                              
                             -------------------                                         
    begin                : in 1999                                           
    copyright            : (C) 1999 by Andreas Hundt, Denis Oliver Kropp                         
    email                : clanbomber@fischlustig.de                                     
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   * 
 *                                                                         *
 ***************************************************************************/

#include <ClanLib/Core/System/system.h>
#include <ClanLib/Display/Input/input.h>
#include <ClanLib/Display/Input/inputbuffer.h>
#include <ClanLib/Display/Input/keyboard.h>
#include <ClanLib/Display/Input/key.h>
#include <ClanLib/Sound/soundbuffer.h>
#include <ClanLib/Display/Display/display.h>
#include <ClanLib/Display/Display/surface.h>
#include <ClanLib/Display/Font/font.h>

#include "ClanBomber.h"
#include "MapSelector.h"

#include "Config.h"
#include "Map.h"

MapSelector::MapSelector( ClanBomberApplication *_app )
{
	app = _app;

	map = new Map(app);
	
	if (Config::get_start_map() > map->get_map_count()-1)
	{
		Config::set_start_map( map->get_map_count() -1 );
	}
	current_map = Config::get_start_map();
	map_at_top = min( current_map-5, map->get_map_count()-11 );
	if (map_at_top < 0) map_at_top = 0;
	
	current_theme = Config::get_theme();
	
	map->load( current_map );

	key_buffer = new CL_InputBuffer( CL_Input::keyboards[0] );
	
	list_width = 230;
	for (int i=0; i<map->get_map_count(); i++)
	{
		CL_String s = map->map_list[i]->get_name();
		s.to_upper();
		if (Resources::Font_small()->get_text_width(s)+10 > list_width)
		{
			list_width = Resources::Font_small()->get_text_width(s) + 40;
		}
	}
}

MapSelector::~MapSelector()
{
	delete map;
	delete key_buffer;
}

void MapSelector::exec()
{
	draw();
	while (CL_Keyboard::get_keycode(CL_KEY_ENTER))
	{
		CL_System::keep_alive();
	}
	key_buffer->clear();
	
	while (1)
	{
		while (key_buffer->keys_left() == 0)
		{
			CL_System::sleep( 50 );
			CL_System::keep_alive();
		}
		while (key_buffer->keys_left())
		{
			if (key_buffer->peek_key().state != CL_Key::Pressed)
			{
				key_buffer->get_key();
				continue;
			}
			switch (key_buffer->get_key().id)
			{
				case CL_KEY_ESCAPE:
					Config::set_start_map( current_map );
					Config::save();
					map->save_selection();
					return;
				break;
				case CL_KEY_SPACE:
					{
						map->map_list[current_map]->toggle();
						Resources::Menu_clear()->play();
					}
				break;
				case CL_KEY_A:
					{
						for (int i=0; i<map->get_map_count(); i++)
						{
							map->map_list[i]->enable();
						}
						Resources::Menu_clear()->play();
					}
				break;
				case CL_KEY_S:
					{
						for (int i=0; i<map->get_map_count(); i++)
						{
							map->map_list[i]->disable();
						}
						map->map_list[current_map]->enable();
						Resources::Menu_clear()->play();
					}
				break;
				case CL_KEY_UP:
					if (current_map > 0)
					{
						current_map--;
						map_at_top = min( current_map-5, map->get_map_count()-11 );
						if (map_at_top < 0) map_at_top = 0;
						Resources::Menu_break()->play();
						map->load( current_map );
					}
				break;
				case CL_KEY_DOWN:
					if (current_map < map->get_map_count()-1)
					{
						current_map++;
						map_at_top = min( current_map-5, map->get_map_count()-11 );
						if (map_at_top < 0) map_at_top = 0;
						Resources::Menu_break()->play();
						map->load( current_map );
					}
				break;
				case CL_KEY_LEFT:
					if (current_theme > 0)
					{
						current_theme--;
						Resources::Menu_clear()->play();
						Config::set_theme( current_theme );
						map->load( current_map );
					}
				break;
				case CL_KEY_RIGHT:
					if (current_theme < 3)
					{
						current_theme++;
						Resources::Menu_clear()->play();
						Config::set_theme( current_theme );
						map->load( current_map );
					}
				break;
			}
		}
		draw();
	}
}

void MapSelector::draw()
{
	Resources::Mapselector_background()->put_screen(0,0);
	
	Resources::Font_small()->print_left( 10, 530, "SPACE   TOGGLE" );
	Resources::Font_small()->print_left( 10, 550, "A   SELECT ALL" );
	Resources::Font_small()->print_left( 10, 570, "S   SELECT CURRENT MAP" );
	
	// show author name
	CL_String author("BY ");
	author+=map->get_author();
	author.to_upper();
	Resources::Font_small()->print_center( 510, 90, author );
	
	// show map name
	Resources::Font_big()->print_center( 510, 50, map->get_name() );
	
	// highlight selected theme
	CL_Display::fill_rect( 30+current_theme*180, 424, 210+current_theme*180, 454, 0.1f, 0.4f, 0.8f, 0.5f );

	// show all themes
	Resources::Font_small()->print_center( 120, 430, "JUNGLE ARENA" );
	Resources::Font_small()->print_center( 300, 430, "BLUE GROUND" );
	Resources::Font_small()->print_center( 480, 430, "GREEN CRYPT" );
	Resources::Font_small()->print_center( 660, 430, "MELTING METAL" );

	// show small map preview
	map->show_preview( 350, 125, 0.5f );

	// display max players info
	if (map->get_max_players() < Config::get_number_of_players())
	{
		Resources::Mapselector_not_available()->put_screen( 402, 128 );
		CL_Display::fill_rect( 200, 495, 636, 522, 1.0f, 0.0f, 0.0f, 0.5f );
		Resources::Font_small()->print_left( 210, 500, CL_String("THIS MAP IS ONLY FOR UP TO ") + map->get_max_players() + " PLAYERS" );
	} else
	{
		Resources::Font_small()->print_left( 230, 500, CL_String("THIS MAP IS FOR UP TO ") + map->get_max_players() + " PLAYERS" );
	}
	
	// map list background
	CL_Display::fill_rect( 15, 116, 15+list_width, 391, 0.3f, 0.3f, 0.3f, 0.5f );
	
	// highlight selected map name
	CL_Display::fill_rect( 15, 116+(current_map-map_at_top)*25, 15+list_width, 141+(current_map-map_at_top)*25, 0.1f, 0.4f, 0.8f, 0.5f );
	
	// show up to ten map names
	for (int i=0; i<min(11, map->get_map_count()); i++)
	{
		// enabled ?
		if (map->map_list[i+map_at_top]->is_enabled())
		{
			CL_Display::fill_rect( 15, 116+i*25, 15+list_width, 141+i*25, 0.9f, 0.6f, 0.1f, 0.4f );
		}
		
		// show name
		CL_String s = map->map_list[i+map_at_top]->get_name();
		s.to_upper();
		Resources::Font_small()->print_left( 30, 120+i*25, s );
	}
	
	// show scroll indicators
	if (map_at_top > 0)
	{
		Resources::Font_big()->print_center( 135, 85, "+" );
	}
	if (map_at_top < map->get_map_count()-11)
	{
		Resources::Font_big()->print_center( 135, 391, "-" );
	}
	
	CL_Display::flip_display();
}



















