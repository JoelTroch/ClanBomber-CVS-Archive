/***************************************************************************
                          Extra.cpp  -  description
                             -------------------
    begin                : ?
    copyright            : (C) 1999 by Andreas Hundt, Denis Oliver Kropp
    email                : clanbomber@fischlustig.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
// $Id: Extra.cpp,v 1.1 2001/08/08 21:46:14 der_fisch Exp $

#include "ClanBomber.h"
#include "Extra.h"

#include "Timer.h"
#include "Bomber.h"
#include "Map.h"
#include "MapTile.h"


Extra::Extra( int _x, int _y, ClanBomberApplication *_app ) : GameObject( _x, _y, _app )
{
	surface = Resources::Extras_extras();
	app->objects.add( this );
	destroyed = false;
	destroyable = true;
	z = Z_EXTRA;
}


Extra::~Extra()
{
}

void Extra::act()
{
	GameObject::act();
	if (falling)
	{
		return;
	}
	if (fallen_down)
	{
		delete_me = true;
		return;
	}
	
	if (destroyed)
	{
		destroy_countdown -= Timer::time_elapsed();
		if (destroy_countdown <= 0)
		{
			delete_me = true;
			return;
		}
	}

	if (!flying)
	{
		CL_Iterator<GameObject> game_object_counter (get_maptile()->objects);
		while (game_object_counter.next() != NULL)
		{
			if (game_object_counter()->get_type() == BOMBER)
			{
				effect( (Bomber*)game_object_counter());
				delete_me = true;
			}
		}
	}
}

void Extra::destroy()
{
	if (!destroyed)
	{
		if (destroyable)
		{
			destroyed = true;
			destroy_countdown = 0.5f;
		} else
		{			
			fly_to(app->map->get_passable());
		}
	}
}

bool Extra::is_destroyable() const
{
	return destroyable;
}








