/***************************************************************************
                          Controller.cpp  -  description
                             -------------------
    begin                : ?
    copyright            : (C) 1999 by Andreas Hundt, Denis Oliver Kropp
    email                : clanbomber@fischlustig.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
// $Id: Controller.cpp,v 1.1 2001/08/08 21:46:14 der_fisch Exp $

#include "ClanBomber.h"
#include "Controller.h"


#include "Controller_AI.h"
#include "Controller_AI_MIC.h"
#include "Controller_Keyboard.h"
#include "Controller_Joystick.h"

#include "Bomber.h"


Controller::Controller()
{
	bomber = NULL;
	active = false;
	reverse = false;
	bomb_mode = NORMAL;
}

void Controller::attach(Bomber* _bomber)
{
	bomber = _bomber;
}

void Controller::activate()
{
	active = true;
}

void Controller::deactivate()
{
	active = false;
}

void Controller::revert()
{
	reverse = !reverse;
}

void Controller::bomb_normal()
{
	bomb_mode = NORMAL;
}

void Controller::bomb_always()
{
	bomb_mode = ALWAYS;
}


/* Keyboard Controller */

Controller* Controller::create( CONTROLLER_TYPE _type )
{
	switch( _type )
	{
		case KEYMAP_1:
			return new Controller_Keyboard(0);
		break;
		case KEYMAP_2:
			return new Controller_Keyboard(1);
		break;
		case KEYMAP_3:
			return new Controller_Keyboard(2);
		break;
		case JOYSTICK_1:
			return new Controller_Joystick(0);
		break;
		case JOYSTICK_2:
			return new Controller_Joystick(1);
		break;
		case JOYSTICK_3:
			return new Controller_Joystick(2);
		break;
		case JOYSTICK_4:
			return new Controller_Joystick(3);
		break;
		case JOYSTICK_5:
			return new Controller_Joystick(4);
		break;
		case JOYSTICK_6:
			return new Controller_Joystick(5);
		break;
		case JOYSTICK_7:
			return new Controller_Joystick(6);
		break;
		case JOYSTICK_8:
			return new Controller_Joystick(7);
		break;
		case AI:
			return new Controller_AI();
		break;
		case AI_MIC:
			return new Controller_AI_MiC();
		break;
		default:
		break;
	}
	
	return NULL; // fuck, wrong type!?
}





