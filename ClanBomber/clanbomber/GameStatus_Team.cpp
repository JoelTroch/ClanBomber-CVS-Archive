/***************************************************************************
                          GameStatus.cpp  -  description                              
                             -------------------                                         
    begin                : Fri Mar 19 1999                                           
    copyright            : (C) 1999 by Andreas Hundt, Denis Oliver Kropp                         
    email                : clanbomber@fischlustig.de                                     
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   * 
 *                                                                         *
 ***************************************************************************/
// $Id: GameStatus_Team.cpp,v 1.4 2004/01/08 18:10:39 der_fisch Exp $

#include <ClanLib/Core/System/system.h>
#include <ClanLib/Display/Input/input.h>
#include <ClanLib/Display/Input/keyboard.h>
#include <ClanLib/Display/Input/key.h>
#include <ClanLib/Sound/soundbuffer.h>
#include <ClanLib/Display/Display/display.h>
#include <ClanLib/Display/Display/surface.h>
#include <ClanLib/Display/Font/font.h>

#include "ClanBomber.h"
#include "GameStatus_Team.h"

#include "GameObject.h"
#include "Config.h"
#include "Timer.h"
#include "Bomber.h"
#include "Map.h"

#include <math.h>

char* GameStatus_Team::team_names[4] = { "BLOOD TEAM", "GOLD TEAM", "WATER TEAM", "LEAF TEAM" };
	
GameStatus_Team::team_color GameStatus_Team::team_colors[4] = {	{1.0, 0.5, 0.5},
													{1.0, 1.0, 0.0},
													{0.5, 0.5, 1.0},
													{0.5, 1.0, 0.5}};

GameStatus_Team::GameStatus_Team(ClanBomberApplication* _app) : GameStatus( _app)
{
}

void GameStatus_Team::show()
{
	Timer timer;
	timer.reset();

	CL_String temp_string;

	if (end_of_game)
	{
		Resources::Gamestatus_klatsch()->play();
	}

	while(!(CL_Keyboard::get_keycode(CL_KEY_SPACE)))
	{
		team_count[0] = team_count[1] = team_count[2] = team_count[3] = 0;	

		Resources::Gamestatus_background()->put_screen(0, 0, 0 );
		Resources::Font_small()->print_center( 400, 570, "PRESS SPACE TO CONTINUE.");		
		
		int window_x_offset = 0;
		int window_y_offset = 170;
	

		for (int team = 0;team<4;team++)
		{
			CL_Iterator<Bomber> bomber_object_counter(app->bomber_objects);
			while (bomber_object_counter.next() != NULL)
			{
				if (bomber_object_counter()->get_team()-1 == team)
				{
					CL_Display::fill_rect( window_x_offset,  window_y_offset+30+team_count[team]*30, window_x_offset+380,window_y_offset+60+team_count[team]*30, team_colors[team].r, team_colors[team].g, team_colors[team].b, 0.4f );
 					((GameObject*)bomber_object_counter())->show( window_x_offset,window_y_offset+25+team_count[team]*30, 0.5f);
					temp_string = bomber_object_counter()->get_name();
					temp_string.to_upper();
					Resources::Font_small()->print_left( window_x_offset+35,window_y_offset+36+team_count[team]*30,temp_string );
					team_count[team]++;
				}
			}
			if (team_count[team] > 0)
			{
				Resources::Playersetup_teamlamps()->put_screen( window_x_offset,window_y_offset,  team );									
				Resources::Font_small()->print_left( window_x_offset+35,window_y_offset+7, team_names[team] );
				
				CL_Display::set_clip_rect( CL_ClipRect( window_x_offset+180, window_y_offset+4, int(window_x_offset+180+(team_points[team]/(float)team_count[team])*20), window_y_offset+24 ) );
				for (int i=0 ; i<(team_points[team]/team_count[team])+1; i++ )
				{
					Resources::Gamestatus_tools()->put_screen( window_x_offset+180+i*20, window_y_offset+4,0.5f, 0.5f, 0  );
				}
				CL_Display::set_clip_rect( CL_ClipRect(0,0,800,600) );
				
				if (end_of_game && winner->get_team()-1 == team)
				{
					float scalefactor = (float)(sin(CL_System::get_time()/100.0f)+2.0f);
					Resources::Gamestatus_tools()->put_screen(int(window_x_offset+200-20*scalefactor), int(window_y_offset+70-20*scalefactor),scalefactor,scalefactor, 0 );
				}

				window_x_offset+=400;
				if (window_x_offset == 800)
				{
					window_x_offset=0;
					window_y_offset+=200;
				}
			}
  		}
	   	CL_Iterator<Bomber> bomber_object_counter2(app->bomber_objects);		
		int i = 0;
		while (bomber_object_counter2.next() != NULL)
		{
			if (bomber_object_counter2()->get_team() == 0)
			{
				Resources::Font_small()->print_left( window_x_offset+35,window_y_offset+7, "TEAMLESS HEROES" );
				CL_Display::fill_rect( window_x_offset,  window_y_offset+30+i*30, window_x_offset+380,window_y_offset+60+i*30, 0.6f, 0.6f, 0.6f, 0.4f );
				((GameObject*)bomber_object_counter2())->show( window_x_offset,window_y_offset+25+i*30, 0.5f);
				temp_string = bomber_object_counter2()->get_name();
				temp_string.to_upper();
				Resources::Font_small()->print_left( window_x_offset+35,window_y_offset+36+i*30,temp_string );
				for (int j=0 ; j<bomber_object_counter2()->get_points() ; j++ )
				{
					Resources::Gamestatus_tools()->put_screen( window_x_offset+180+j*20, window_y_offset+34+i*30,0.5f, 0.5f, 0  );
				}
				if (end_of_game && winner == bomber_object_counter2() )
				{
					float scalefactor = (float)(sin(CL_System::get_time()/100.0f)+2.0f);
					Resources::Gamestatus_tools()->put_screen(int(window_x_offset+200-20*scalefactor), int(window_y_offset+34+i*30-20*scalefactor),scalefactor,scalefactor, 0 );
				}

				i++;
			}
		}
		if (!winner)
		{
			Resources::Font_big()->print_center( 500, 40, "Draw Game" );
		}
		else
		{
			if (winner->get_team() == 0)
			{
				Resources::Font_big()->print_center( 500, 40, winner->get_name() + " won" );					
			}
			else
			{
				Resources::Font_big()->print_center( 500,40, CL_String( team_names[winner->get_team()-1] ) + " won" );
			}			
	
		}
		if (end_of_game)
		{
			Resources::Font_big()->print_center( 500, 80,"the Match");		
		}
		else
		{
			Resources::Font_small()->print_right( 785, 10, "NEXT LEVEL");
			CL_String temp_string = app->map->get_name();
			temp_string.to_upper();
			Resources::Font_small()->print_right( 785, 125, temp_string);			
			app->map->show_preview(790-119,30,0.18f);
		}
		CL_System::keep_alive();
		CL_Display::flip_display();
	}
	CL_Display::clear_display();
	CL_Display::flip_display();
	CL_Display::clear_display();
	CL_Display::flip_display();
	CL_Display::clear_display();
	CL_Display::flip_display();
}

GameStatus_Team::~GameStatus_Team()
{
}

void GameStatus_Team::analyze_game()
{
	team_count[0] = team_count[1] = team_count[2] = team_count[3] = 0;	
	team_points[0] = team_points[1] = team_points[2] = team_points[3] = 0;

	end_of_game = false;
	winner = NULL;
	
	CL_Iterator<Bomber> bomber_object_counter(app->bomber_objects);
	while (bomber_object_counter.next() != NULL)
	{

		if ((bomber_object_counter()->get_team() == 0)  && !bomber_object_counter()->is_dead())
		{
			
			winner = bomber_object_counter();
			if(bomber_object_counter()->get_points() == Config::get_points_to_win())
			{
				end_of_game = true;
			}
		}
		if (bomber_object_counter()->get_team() != 0)
		{
			if (!bomber_object_counter()->is_dead())
			{
				winner = bomber_object_counter();
			}
			team_count[bomber_object_counter()->get_team()-1] += 1;
			team_points[bomber_object_counter()->get_team()-1] += bomber_object_counter()->get_points();
		}

	}
	
	for (int i=0; i<4; i++)
	{
		if ((team_count[i] > 0) && (team_points[i] >= team_count[i]*Config::get_points_to_win()))
		{
			end_of_game = true;		
		}
	}
}
