/***************************************************************************
                          FILENAME  -  description                              
                             -------------------                                         
    begin                : in 1999                                           
    copyright            : (C) 1999 by Andreas Hundt, Denis Oliver Kropp                         
    email                : clanbomber@fischlustig.de                                     
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   * 
 *                                                                         *
 ***************************************************************************/

#ifndef WIN32
	#include <unistd.h>
#endif

#include <ClanLib/Display/Display/surface.h>
#include <ClanLib/Core/System/system.h>
#include <ClanLib/Display/Input/input.h>
#include <ClanLib/Display/Input/inputbuffer.h>
#include <ClanLib/Display/Input/keyboard.h>
#include <ClanLib/Sound/soundbuffer.h>
#include <ClanLib/Display/Display/display.h>
#include <ClanLib/Display/Font/font.h>

#include "ClanBomber.h"
#include "MapEditor.h"

#include "Config.h"
#include "Map.h"


MapEditor::MapEditor( ClanBomberApplication *_app )
{
	app = _app;

	map = new Map(app);
	
	current_map = 0;
	map_at_top = min( current_map-8, map->get_map_count()-16 );
	if (map_at_top < 0) map_at_top = 0;
	
	key_buffer = new CL_InputBuffer( CL_Input::keyboards[0] );
	
	list_width = 230;
	for (int i=0; i<map->get_map_count(); i++)
	{
		CL_String s = map->map_list[i]->get_name();
		s.to_upper();
		if (Resources::Font_small()->get_text_width(s)+10 > list_width)
		{
			list_width = Resources::Font_small()->get_text_width(s) + 40;
		}
	}
	
	cur_x = 0;
	cur_y = 0;
	text_editor_mode = false;
}

MapEditor::~MapEditor()
{
	delete map;
	delete key_buffer;
}

void MapEditor::exec()
{
	draw_select_screen();
	
	while (CL_Keyboard::get_keycode(CL_KEY_ENTER))
	{
		CL_System::keep_alive();
	}
	key_buffer->clear();
	
	while (1)
	{
		while (key_buffer->keys_left() == 0)
		{
			CL_System::sleep( 50 );
			CL_System::keep_alive();
		}
                if (key_buffer->peek_key().state != CL_Key::Pressed)
                {
                        key_buffer->get_key();
			continue;
                }
		switch (key_buffer->get_key().id)
		{
			case CL_KEY_ESCAPE:
				return;
			break;
			case CL_KEY_ENTER:
				if (map->map_list[current_map]->is_writable())
				{
					Resources::Menu_clear()->play();
					edit_map( current_map );
				}
			break;
			case CL_KEY_N:
				Resources::Menu_clear()->play();
				if ( new_map() )
				{
					edit_map( current_map );
				}
			break;
			case CL_KEY_D:
				if (map->map_list[current_map]->is_writable())
				{
					Resources::Menu_clear()->play();
					current_map = map->delete_entry( current_map );
					map_at_top = min( current_map-8, map->get_map_count()-16 );
				}
			break;
			case CL_KEY_UP:
				if (current_map > 0)
				{
					current_map--;
					map_at_top = min( current_map-8, map->get_map_count()-16 );
					if (map_at_top < 0) map_at_top = 0;
					Resources::Menu_break()->play();
				}
			break;
			case CL_KEY_DOWN:
				if (current_map < map->get_map_count()-1)
				{
					current_map++;
					map_at_top = min( current_map-8, map->get_map_count()-16 );
					if (map_at_top < 0) map_at_top = 0;
					Resources::Menu_break()->play();
				}
			break;
		}
		if (key_buffer->keys_left() == 0) draw_select_screen();
	}
}

void MapEditor::draw_select_screen(bool flip)
{
	Resources::MapEditor_background()->put_screen(0,0);
	
	// map list background
	CL_Display::fill_rect( 45, 116, 45+list_width, 516, 0.3f, 0.3f, 0.3f, 0.5f );
	
	// highlight selected map name
	CL_Display::fill_rect( 45, 116+(current_map-map_at_top)*25, 45+list_width, 141+(current_map-map_at_top)*25, 0.1f, 0.4f, 0.8f, 0.5f );
	
	// show up to twenty map names
	for (int i=0; i<min(16, map->get_map_count()); i++)
	{
		// writable ?
		if (!map->map_list[i+map_at_top]->is_writable())
		{
			CL_Display::fill_rect( 45, 116+i*25, 45+list_width, 141+i*25, 0.9f, 0.1f, 0.1f, 0.4f );
		}
		
		// show name
		CL_String s = map->map_list[i+map_at_top]->get_name();
		s.to_upper();
		Resources::Font_small()->print_left( 60, 120+i*25, s );
	}
	
	// show scroll indicators
	if (map_at_top > 0)
	{
		Resources::Font_big()->print_center( 165, 85, "+" );
	}
	if (map_at_top < map->get_map_count()-16)
	{
		Resources::Font_big()->print_center( 165, 516, "-" );
	}
	
	Resources::Font_big()->print_center( 520, 150, "Select a map to edit and press Enter" );
	Resources::Font_big()->print_center( 520, 190, "Maps marked red are readonly" );
	
	Resources::Font_big()->print_center( 520, 250, "Press N to create a new map" );
	Resources::Font_big()->print_center( 520, 290, "Press D to delete a map" );
	
	if (flip)
	{
		CL_Display::flip_display();
	}
}

bool MapEditor::new_map()
{
	while (CL_Keyboard::get_keycode(CL_KEY_N))
	{
		CL_System::keep_alive();
	}
	
	key_buffer->clear();
	CL_String new_string;
	
	while (1)
	{
		draw_select_screen(false);
	
		CL_Display::fill_rect( 200,300, 600,400, 0,0,0,0.5f);

		Resources::Font_big()->print_left( 230, 330, "Name:" );
		
		Resources::Font_big()->print_left( 380, 330, new_string );
		
		Resources::Font_small()->print_center( 400, 380, "PRESS ESC TO ABORT" );

		CL_Display::flip_display();
		CL_System::keep_alive();
		
		if (key_buffer->peek_key().state != CL_Key::Pressed)
		{
			key_buffer->get_key();
			continue;
		}

		switch (enter_string( key_buffer, new_string ))
		{
			case 1:
				if (new_string.get_length())
				{
					current_map = map->new_entry( new_string );
					map_at_top = min( current_map-8, map->get_map_count()-16 );
					return true;
				}
				return false;
			case -1:
				return false;
		}
	}
	return false; // impossible to reach this
}

void MapEditor::edit_map( int number )
{
	bool maptile_set;

	MapEntry *entry = map->map_list[number];
	map->load( number );

	draw_editor();
	CL_Display::flip_display();

	while (CL_Keyboard::get_keycode(CL_KEY_ENTER))
	{
		CL_System::keep_alive();
	}
	key_buffer->clear();
	
	while (1)
	{
		maptile_set = true;
		while (key_buffer->keys_left() == 0)
		{
			CL_System::sleep( 50 );
			CL_System::keep_alive();
		}
		if (key_buffer->peek_key().state != CL_Key::Pressed)
		{
			key_buffer->get_key();
			continue;
		}
		switch (key_buffer->get_key().id)
		{
			case CL_KEY_ESCAPE:
				maptile_set = false;
				entry->write_back();
				return;
			break;
			case CL_KEY_F1: // help screen
				maptile_set = false;
				show_help();
			break;
			case CL_KEY_F2: // editor_mode
				maptile_set = false;
				text_editor_mode = !text_editor_mode;
				Resources::Menu_clear()->play();
			break;
/*			case CL_KEY_DELETE:
				maptile_set = false;
				Resources::Menu_clear()->play();
				entry->clear();
				map->reload();
				Resources::Menu_clear()->play();
			break;*/
			case CL_KEY_BACKSPACE:
				maptile_set = false;
				Resources::Menu_clear()->play();	
				cur_x--;
				clip_cursor();
				entry->set_data(cur_x,cur_y,'-');			
				map->reload();
			break;
			case CL_KEY_PAGEUP: // increase number of players
				maptile_set = false;
				Resources::Menu_clear()->play();
				entry->set_max_players(entry->get_max_players()+1);
				map->reload();
			break;
			case CL_KEY_PAGEDOWN: // decrease number of players
				maptile_set = false;
				Resources::Menu_clear()->play();
				entry->set_max_players(entry->get_max_players()-1);
				map->reload();
			break;
			case CL_KEY_SPACE: // none
				Resources::Menu_clear()->play();
				entry->set_data(cur_x,cur_y,'-');
				map->reload();
			break;
			case CL_KEY_A: // author
				maptile_set = false;
				Resources::Menu_break()->play();
				entry->set_author( get_new_author() );
			break;
			case CL_KEY_G: // ground
				Resources::Menu_clear()->play();
				entry->set_data(cur_x,cur_y,' ');
				map->reload();
			break;
			case CL_KEY_W: // wall
				Resources::Menu_clear()->play();
				entry->set_data(cur_x,cur_y,'*');
				map->reload();
			break;
			case CL_KEY_B: // box
				Resources::Menu_clear()->play();
				entry->set_data(cur_x,cur_y,'+');
				map->reload();
			break;
			case CL_KEY_R: // random box
				Resources::Menu_clear()->play();
				entry->set_data(cur_x,cur_y,'R');
				map->reload();
			break;
			case CL_KEY_H: // arrow left
				Resources::Menu_clear()->play();
				entry->set_data(cur_x,cur_y,'<');
				map->reload();
			break;
			case CL_KEY_K: // arrow right
				Resources::Menu_clear()->play();
				entry->set_data(cur_x,cur_y,'>');
				map->reload();
			break;
			case CL_KEY_U: // arrow up
				Resources::Menu_clear()->play();
				entry->set_data(cur_x,cur_y,'^');
				map->reload();
			break;
			case CL_KEY_J: // arrow down
				Resources::Menu_clear()->play();
				entry->set_data(cur_x,cur_y,'v');
				map->reload();
			break;
			case CL_KEY_I: // ice
				Resources::Menu_clear()->play();
				entry->set_data(cur_x,cur_y,'S');
				map->reload();
			break;
			case CL_KEY_O: // bomb trap
				Resources::Menu_clear()->play();
				entry->set_data(cur_x,cur_y,'o');
				map->reload();
			break;
			case CL_KEY_1: // place first player
				Resources::Menu_clear()->play();
				entry->set_bomber_pos(cur_x,cur_y, 0);
				map->reload();
			break;
			case CL_KEY_2:
				Resources::Menu_clear()->play();
				entry->set_bomber_pos(cur_x,cur_y, 1);
				map->reload();
			break;
			case CL_KEY_3:
				Resources::Menu_clear()->play();
				entry->set_bomber_pos(cur_x,cur_y, 2);
				map->reload();
			break;
			case CL_KEY_4:
				Resources::Menu_clear()->play();
				entry->set_bomber_pos(cur_x,cur_y, 3);
				map->reload();
			break;
			case CL_KEY_5:
				Resources::Menu_clear()->play();
				entry->set_bomber_pos(cur_x,cur_y, 4);
				map->reload();
			break;
			case CL_KEY_6:
				Resources::Menu_clear()->play();
				entry->set_bomber_pos(cur_x,cur_y, 5);
				map->reload();
			break;
			case CL_KEY_7:
				Resources::Menu_clear()->play();
				entry->set_bomber_pos(cur_x,cur_y, 6);
				map->reload();
			break;
			case CL_KEY_8:
				Resources::Menu_clear()->play();
				entry->set_bomber_pos(cur_x,cur_y, 7);
				map->reload();
			break;
			case CL_KEY_LEFT:
				maptile_set = false;
				Resources::Menu_break()->play();
				cur_x--;
			break;
			case CL_KEY_RIGHT:
				maptile_set = false;
				Resources::Menu_break()->play();
				cur_x++;
			break;
			case CL_KEY_UP:
				maptile_set = false;
				Resources::Menu_break()->play();
				cur_y--;
			break;
			case CL_KEY_DOWN:
				maptile_set = false;
				Resources::Menu_break()->play();
				cur_y++;
			break;
			default:
				maptile_set = false;
		}
		if (maptile_set && text_editor_mode)
		{
			cur_x++;
		}
		clip_cursor();
		draw_editor();
		CL_Display::flip_display();
	}
}

void MapEditor::draw_editor()
{
	Resources::MapEditor_background()->put_screen(0,0);
	
	// show map
	map->show();
	map->show_random_boxes();
	map->show_start_positions();
	
	// show cursor
	CL_Display::fill_rect( 60+cur_x*40, 40+cur_y*40, 100+cur_x*40, 80+cur_y*40, 0.6f, 0.5f, 0.1f, 0.6f );
	
	// huh, what's this? ;)
	Resources::Font_small()->print_left( 10, 3, "PRESS F1 FOR HELP" );

	// show map name
	Resources::Font_big()->print_right( 780, 3, map->map_list[current_map]->get_name() );

	if (text_editor_mode)
	{
		Resources::Font_small()->print_left( 10, 580, "TEXT EDITOR MODE" );
	} else
	{
		Resources::Font_small()->print_left( 10, 580, "NORMAL EDITOR MODE" );
	}

	Resources::Font_small()->print_right( 780, 580, CL_String("NUMBER OF PLAYERS   ") + map->map_list[current_map]->get_max_players() );
}

CL_String MapEditor::get_new_author()
{
	CL_String author = map->map_list[current_map]->get_author();

	while (CL_Keyboard::get_keycode(CL_KEY_A))
	{
		CL_System::keep_alive();
	}
	
	key_buffer->clear();
	
	while (1)
	{
		draw_editor();
	
		CL_Display::fill_rect( 150,300, 650,400, 0,0,0,0.8f);

		Resources::Font_big()->print_left( 180, 330, "Author:" );
		
		Resources::Font_big()->print_left( 330, 330, author );
		
		Resources::Font_small()->print_center( 400, 380, "PRESS ESC TO ABORT" );

		CL_Display::flip_display();
		CL_System::keep_alive();
		
		if (key_buffer->peek_key().state != CL_Key::Pressed)
		{
			key_buffer->get_key();
			continue;
		}

		switch (enter_string( key_buffer, author ))
		{
			case 1:
				if (author.get_length())
				{
					Resources::Menu_clear()->play();
					return author;
				}
			case -1:
				Resources::Menu_break()->play();
				return map->map_list[current_map]->get_author();
		}
	}

	// cannot reach this!
	return map->map_list[current_map]->get_author();
}

void MapEditor::show_help()
{
	Resources::MapEditor_background()->put_screen(0,0);
	
	Resources::Game_maptiles()->put_screen( 40, 70, Config::get_theme()*4 + 0 );
	Resources::Font_small()->print_left( 110, 80, "PRESS G FOR A GROUND TILE" );
	
	Resources::Game_maptiles()->put_screen( 40, 110, Config::get_theme()*4 + 1 );
	Resources::Font_small()->print_left( 110, 120, "PRESS W FOR A WALL" );
	
	Resources::Game_maptiles()->put_screen( 40, 150, Config::get_theme()*4 + 2 );
	Resources::Font_small()->print_left( 110, 160, "PRESS B FOR A BOX" );
	
	Resources::Game_maptiles()->put_screen( 40, 190, Config::get_theme()*4 + 2 );
	Resources::Game_maptile_addons()->put_screen( 40, 190, 5 );
	Resources::Font_small()->print_left( 110, 200, "PRESS R FOR A RANDOM TILE" );
	
	Resources::Game_maptiles()->put_screen( 40, 230, Config::get_theme()*4 + 0 );
	Resources::Game_maptile_addons()->put_screen( 40, 230, 7 );
	Resources::Font_small()->print_left( 110, 240, "PRESS O FOR A BOMB TRAP" );

	Resources::Game_maptiles()->put_screen( 40, 270, Config::get_theme()*4 + 0 );
	Resources::Game_maptile_addons()->put_screen( 40, 270, 0 );
	Resources::Font_small()->print_left( 110, 280, "PRESS I FOR AN ICE TILE" );
	
	Resources::Game_maptiles()->put_screen( 40, 320, Config::get_theme()*4 + 0 );
	Resources::Game_maptile_addons()->put_screen( 40, 320, 6 );
	Resources::Font_small()->print_left( 110, 330, "PRESS NUMBER KEYS FOR BOMBER POSITIONS" );
	
	// show arrows
	Resources::Font_small()->print_left( 40, 400, "THE FOLLOWING KEYS PRODUCE ARROWS" );

	Resources::Game_maptiles()->put_screen( 150, 460, Config::get_theme()*4 + 0 );
	Resources::Game_maptile_addons()->put_screen( 150, 460, 3 );
	Resources::Font_small()->print_center( 170, 430, "U" );
	
	Resources::Game_maptiles()->put_screen( 110, 500, Config::get_theme()*4 + 0 );
	Resources::Game_maptile_addons()->put_screen( 110, 500, 2 );
	Resources::Font_small()->print_right( 98, 512, "J" );
	
	Resources::Game_maptiles()->put_screen( 150, 500, Config::get_theme()*4 + 0 );
	Resources::Game_maptile_addons()->put_screen( 150, 500, 1 );
	Resources::Font_small()->print_center( 170, 552, "K" );
	
	Resources::Game_maptiles()->put_screen( 190, 500, Config::get_theme()*4 + 0 );
	Resources::Game_maptile_addons()->put_screen( 190, 500, 4 );
	Resources::Font_small()->print_left( 242, 512, "L" );
	
	// misc stuff
	Resources::Font_small()->print_left( 380, 450, "PRESS SPACE BAR FOR A HOLE" );
	Resources::Font_small()->print_left( 380, 470, "PAGE UP AND DOWN SET NR OF PLAYERS" );
	Resources::Font_small()->print_left( 380, 490, "PRESS A FOR CHANGING THE AUTHOR" );
	Resources::Font_small()->print_left( 380, 510, "PRESSING F2 SWITCHES EDITOR MODE" );
	
	CL_Display::flip_display();

		
	// release the key!
	while (CL_Keyboard::get_keycode(CL_KEY_F1))
	{
		CL_System::keep_alive();
	}
	
	// wait for the "any key"
	key_buffer->clear();
	while (key_buffer->get_key().state != CL_Key::Pressed)
	{
		CL_System::sleep( 50 );
		CL_System::keep_alive();
	}
	key_buffer->clear();
}

void MapEditor::clip_cursor()
{
	if (text_editor_mode)
	{
		if (cur_x <0)
		{
			cur_x = MAP_WIDTH-1;
			cur_y--;
		}
		if (cur_x > MAP_WIDTH-1)
		{
			cur_x=0;
			cur_y++;
		}
	}
	else
	{
		if (cur_x <0)
		{
			cur_x = 0;
		}
		if (cur_x > MAP_WIDTH-1)
		{
			cur_x = MAP_WIDTH-1;
		}
	}
	if (cur_y <0)
	{
		cur_y = 0;
	}
	if (cur_y > MAP_HEIGHT-1)
	{
		cur_y = MAP_HEIGHT-1;
	}
}


