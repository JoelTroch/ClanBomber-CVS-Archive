/***************************************************************************
                          Bomber.h  -  description
                             -------------------
    begin                : ?
    copyright            : (C) 1999 by Andreas Hundt, Denis Oliver Kropp
    email                : clanbomber@fischlustig.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
// $Id: Bomber.h,v 1.1 2001/08/08 21:46:14 der_fisch Exp $

#ifndef Bomber_h
#define Bomber_h

#include "GameObject.h"

class Disease;
class Controller;

class Bomber : public GameObject
{
public:
	typedef enum
	{
		RED		= 0,
		BLUE		= 1,
		YELLOW	= 2,
		GREEN	= 3,
		RED2		= 4,
		BLUE2	= 5,
		YELLOW2	= 6,
		GREEN2	= 7,
	} COLOR;

	Bomber( int _x, int _y, COLOR _color, Controller* _controller, CL_String _name, int _team, int _number, ClanBomberApplication *_app);
	~Bomber();
	
	void dec_points();
	void inc_points();
	void set_points( int _points );
	int get_points() const;
	bool is_dead() const;
	int get_deaths() const;
	int get_kills() const;
	void inc_kills();
	int get_cur_bombs() const;
	int get_power() const;
	int get_bombs() const;
	
	int get_number() const;
	int get_team() const;
	void set_team( int _team);

	COLOR get_color() const;

	void gain_extra_bomb();
	void gain_extra_skateboard();
	void gain_extra_kick();
	void gain_extra_glove();
	void gain_extra_power();
	void loose_all_extras();
	void loose_disease();

	void infect (Disease* _disease);
	void infect_others();
	
	void inc_cur_bombs();
	void dec_cur_bombs();

	void put_bomb();
	bool die();
	void show();
	void act();
	void reset();

	CL_String get_name() const;
	
	ObjectType get_type() const;
	
	Controller* controller;
	
	bool is_diseased();
        bool is_able_to_throw() const
        {
                return can_throw;
        }
	
protected:
	COLOR color;
	CL_String name;
	int deaths;
	int kills;
	bool dead;
	float anim_count;
	int points;
	bool can_throw;
	Disease*  disease;
	
	
	int skateboards;
	int cur_bombs;
	int bombs;
	int power;
	int gloves;

	int extra_gloves;
	int extra_skateboards;
	int extra_power;
	int extra_kick;
	int extra_bombs;
	
	bool bomb_key;
	
	int team;
	int number;
};

#endif
