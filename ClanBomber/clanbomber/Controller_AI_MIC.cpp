//#define WITH_DEBUG_SIMPLE

#include "ClanBomber.h"
#include "Controller_AI_MIC.h"
#include "Bomber.h"
#include "Bomb.h"
#include "Map.h"
#include "MapTile.h"
#include "MapTile_Arrow.h"
#include "Timer.h"
#include "Extra.h"
#include "Explosion.h"

#define UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING -200001
#define KICK_POSITION_RATING -200000
#define BOMB_RATING -199999
#define DEADEND_RATING -100000
#define STANDARD_SURE_PLACE_RATING -3777
#define NEARBY_ON_BOMB_RATING -6777
#define BEFORE_BOMB_COUNTDOWN_RATING -7775
#define STARTING_BOMB_COUNTDOWN_RATING -7776
#define AFTER_BOMB_COUNTDOWN_RATING -22777
#define EXTRA_GLOVE_RATING 57
#define EXTRA_KICK_RATING 100
#define EXTRA_BOMB_RATING 183
#define EXTRA_POWER_RATING 253
#define EXTRA_SKATEBOARD_RATING 331
#define EXTRA_JOINT_RATING -10
#define EXTRA_KOKS_RATING -10
#define EXTRA_VIAGRA_RATING -30
#define EXTRA_RATING_WITH_VIAGRA abs(AFTER_BOMB_COUNTDOWN_RATING)
#define WITHOUT_EXTRA_GLOVE_RATING 343
#define WITHOUT_EXTRA_KICK_RATING 357
#define FOREIGN_BOMBER_HIT_RATING 37

Controller_AI_MiC::Controller_AI_MiC() : Controller()
{
  ground=new int*[MAP_WIDTH];
  for (int i=0;i<MAP_WIDTH;i++) ground[i]=new int[MAP_HEIGHT];
  exploding_boxes= new bool*[MAP_WIDTH];
	for (int a=0;a<MAP_WIDTH;a++) exploding_boxes[a]=new bool[MAP_HEIGHT];
  explosions_timer= new explosions*[MAP_WIDTH];
	for (int e=0;e<MAP_WIDTH;e++) explosions_timer[e]=new explosions[MAP_HEIGHT];
	direction=DIR_NONE;
  last_dir_rating=0;
	last_bomber_posx=0;
  last_bomber_posy=0;
  flight_from_bomb=false;
  bombing=false;
  simulation_mode_on=false;
  checking_wise_moves=false;
	getting_paths=false;
}//Controller_AI_MiC

Controller_AI_MiC::~Controller_AI_MiC()
{
  for (int i=0;i<MAP_WIDTH;i++) delete[] ground[i];
  for (int a=0;a<MAP_WIDTH;a++) delete[] exploding_boxes[a];
  for (int e=0;e<MAP_WIDTH;e++) delete[] explosions_timer[e];
}//~Controller_AI_MiC

void Controller_AI_MiC::init_explosions_timer()
{
  for(int i=0; i<MAP_WIDTH;i++)
    for(int j=0; j<MAP_HEIGHT;j++)
		{
			explosions_timer[i][j].max=0;
			explosions_timer[i][j].stack[0].timer=(float)77.0;
      explosions_timer[i][j].stack[0].is_bomb=false;
      explosions_timer[i][j].stack[0].bomb_id=0;
      explosions_timer[i][j].has_bomb=false;
		}
}//init_explosions_timer

void Controller_AI_MiC::put_explosions_timer_on_top(int mapx, int mapy, int index_of_bomb)
{
  explosions_stack tmp;
  tmp.timer=explosions_timer[mapx][mapy].stack[0].timer;
  tmp.is_bomb=explosions_timer[mapx][mapy].stack[0].is_bomb;
	tmp.bomb_id=explosions_timer[mapx][mapy].stack[0].bomb_id;
	explosions_timer[mapx][mapy].stack[0].timer=explosions_timer[mapx][mapy].stack[index_of_bomb].timer;
  explosions_timer[mapx][mapy].stack[0].is_bomb=explosions_timer[mapx][mapy].stack[index_of_bomb].is_bomb;
  explosions_timer[mapx][mapy].stack[0].bomb_id=explosions_timer[mapx][mapy].stack[index_of_bomb].bomb_id;
	explosions_timer[mapx][mapy].stack[index_of_bomb].timer=tmp.timer;
  explosions_timer[mapx][mapy].stack[index_of_bomb].is_bomb=tmp.is_bomb;
  explosions_timer[mapx][mapy].stack[index_of_bomb].bomb_id=tmp.bomb_id;
}//put_explosions_timer_on_top

int Controller_AI_MiC::get_explosions_stack_index(int mapx, int mapy, int id_of_bomb)
{
	int f=0;
  //float lowest_timer=76.0;
  while (f<explosions_timer[mapx][mapy].max)
	{
  	if (explosions_timer[mapx][mapy].stack[f].bomb_id==id_of_bomb) return f;
    f++;
	}
  return -1;
}//get_explosions_stack_index

void Controller_AI_MiC::update_bomb_timer(int mapx, int mapy, int id_of_bomb, float update_timer)
{
  int act_stack_pos=get_explosions_stack_index(mapx,mapy,id_of_bomb);
  if (act_stack_pos!=-1) explosions_timer[mapx][mapy].stack[act_stack_pos].timer=update_timer;
  bool go_on=true;
	int k=mapx-1;
	while (k>=0 && go_on)
  {
    act_stack_pos=get_explosions_stack_index(k,mapy,id_of_bomb);
    if (act_stack_pos!=-1) explosions_timer[k][mapy].stack[act_stack_pos].timer=update_timer;
    else go_on=false;
    k-=1;
  }
  go_on=true;
  k=mapx+1;
  while (k<MAP_WIDTH && go_on)
  {
	  act_stack_pos=get_explosions_stack_index(k,mapy,id_of_bomb);
    if (act_stack_pos!=-1) explosions_timer[k][mapy].stack[act_stack_pos].timer=update_timer;
    else go_on=false;
 	  k+=1;
 	}
  go_on=true;
  int l=mapy-1;
 	while (l>=0 && go_on)
 	{
	  act_stack_pos=get_explosions_stack_index(mapx,l,id_of_bomb);
    if (act_stack_pos!=-1) explosions_timer[mapx][l].stack[act_stack_pos].timer=update_timer;
    else go_on=false;
   	l-=1;
 	}
  go_on=true;
  l=mapy+1;
 	while (l<MAP_HEIGHT && go_on)
  {
	  act_stack_pos=get_explosions_stack_index(mapx,l,id_of_bomb);
    if (act_stack_pos!=-1) explosions_timer[mapx][l].stack[act_stack_pos].timer=update_timer;
    else go_on=false;
 	  l+=1;
  }
}//update_bomb_timer

void Controller_AI_MiC::update_explosions_timer()
{
  dout("in update_explosions_timer()"<<endl);
	bool there_are_changes=false;
	for(int i=0; i<MAP_WIDTH;i++)
    for(int j=0; j<MAP_HEIGHT;j++)
		{
      //nothing to do if no bomb present here or an explosion is on this ground or stacksize==1
      if ((explosions_timer[i][j].stack[0].timer!=(float)77.0) && (explosions_timer[i][j].stack[0].timer!=(float)0.0) && (explosions_timer[i][j].max>1))
			{
        //find a bomb now in this stack !
        bool bomb_present=false;
				int bomb_found_at_index=0;
        int k=0;
				if (explosions_timer[i][j].has_bomb)
				{
        	while (k<explosions_timer[i][j].max)
      	  {
						if (explosions_timer[i][j].stack[k].is_bomb)
						{
							bomb_present=true;
							bomb_found_at_index=k; //save bomb position in stack
	      		}
	          k++;
	        }
				}
        if (bomb_present)
				{
          //get the lowest timer of other bombs first !
          int f=0;
          float lowest_timer=76.0;
          while (f<explosions_timer[i][j].max)
					{
            if (f!=bomb_found_at_index)
            {
              if (explosions_timer[i][j].stack[f].timer<lowest_timer) lowest_timer=explosions_timer[i][j].stack[f].timer;
						}
            f++;
					}
					//check if bomb would explode earlier because hit by another bomb
          if (lowest_timer<explosions_timer[i][j].stack[bomb_found_at_index].timer)
          {
            update_bomb_timer(i,j,explosions_timer[i][j].stack[bomb_found_at_index].bomb_id,lowest_timer);
						there_are_changes=true;
            //copy the bomb now to the top of the explosions_stack !
						if (bomb_found_at_index>0) put_explosions_timer_on_top(i,j,bomb_found_at_index);
          }
				}
				else
				{
          //get the lowest timer now and copy that to the top of the explosions_stack
					int f=0;
          float lowest_timer=76.0;
          int found_at_index=0;
          while (f<explosions_timer[i][j].max)
					{
		        if (explosions_timer[i][j].stack[f].timer<lowest_timer)
						{
							lowest_timer=explosions_timer[i][j].stack[f].timer;
							found_at_index=f;
						}
	          f++;
					}
          if (found_at_index>0) put_explosions_timer_on_top(i,j,found_at_index);
					explosions_timer[i][j].max=1;
				}
			}
		}
  if (there_are_changes) update_explosions_timer();
  dout("update_explosions_timer() ---> fertig!!!"<<endl);
}//update_explosions_timer

void Controller_AI_MiC::compute_ground_rating()
//brings exploding_boxes[] and explosions_timer[] also up to date...
{
  Explosion* explosion=NULL;
  for (int i=0;i<MAP_WIDTH;i++)
  {
    for (int j=0;j<MAP_HEIGHT;j++)
    {
      explosion=get_explosion(i,j);
      if (explosion == NULL)
      {
        //nothing to do?
      }
      else
      {
        ground[i][j]=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING; //explosions will get the same rating as a not passable ground (?)
				explosions_timer[i][j].stack[explosions_timer[i][j].max].timer=(float)0.0;
				//explosions_timer[i][j].stack[explosions_timer[i][j].max].is_bomb=false;
				//explosions_timer[i][j].stack[explosions_timer[i][j].max].bomb_id=0;
				explosions_timer[i][j].max++;
        //explosions_timer[i][j].has_bomb=false;
        //left-expansion:
        int k=i-1;
        while (!bomber->app->map->get_maptile( k, j )->is_blocking() &&
               (k>=0) && (k>=i-explosion->get_power()) &&
               (!extraAt(k+1,j) || k+1==i))
        {
          ground[k][j]=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING;
					explosions_timer[k][j].stack[explosions_timer[k][j].max].timer=(float)0.0;
          //explosions_timer[k][j].stack[explosions_timer[k][j].max].is_bomb=false;
					//explosions_timer[k][j].stack[explosions_timer[k][j].max].bomb_id=0;
					explosions_timer[k][j].max++;
          //explosions_timer[k][j].has_bomb=false;
          k-=1;
        }
  			if (bomber->app->map->get_maptile(k,j)->get_type()==MapTile::BOX || diseaseAt(k,j)) exploding_boxes[k][j]=true;
        //right-expansion:
        k=i+1;
        while (!bomber->app->map->get_maptile( k, j )->is_blocking() &&
               (k<MAP_WIDTH) && (k<=i+explosion->get_power()) &&
               (!extraAt(k-1,j) || k-1==i))
        {
          ground[k][j]=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING;
					explosions_timer[k][j].stack[explosions_timer[k][j].max].timer=(float)0.0;
					//explosions_timer[k][j].stack[explosions_timer[k][j].max].is_bomb=false;
					//explosions_timer[k][j].stack[explosions_timer[k][j].max].bomb_id=0;
					explosions_timer[k][j].max++;
         	//explosions_timer[k][j].has_bomb=false;
	        k+=1;
        }
				if (bomber->app->map->get_maptile(k,j)->get_type()==MapTile::BOX || diseaseAt(k,j)) exploding_boxes[k][j]=true;
        //up-expansion:
        int l=j-1;
        while (!bomber->app->map->get_maptile( i, l )->is_blocking() &&
               (l>=0) && (l>=j-explosion->get_power()) &&
               (!extraAt(i,l+1) || l+1==j))
        {
          ground[i][l]=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING;
					explosions_timer[i][l].stack[explosions_timer[i][l].max].timer=(float)0.0;
					//explosions_timer[i][l].stack[explosions_timer[i][l].max].is_bomb=false;
					//explosions_timer[i][l].stack[explosions_timer[i][l].max].bomb_id=0;
					explosions_timer[i][l].max++;
          //explosions_timer[i][l].has_bomb=false;
	        l-=1;
        }
				if (bomber->app->map->get_maptile(i,l)->get_type()==MapTile::BOX || diseaseAt(i,l)) exploding_boxes[i][l]=true;
        //down-expansion:
        l=j+1;
        while (!bomber->app->map->get_maptile( i, l )->is_blocking() &&
               (l<MAP_HEIGHT) && (l<=j+explosion->get_power())&&
               (!extraAt(i,l-1) || l-1==j))
        {
          ground[i][l]=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING;
					explosions_timer[i][l].stack[explosions_timer[i][l].max].timer=(float)0.0;
					//explosions_timer[i][l].stack[explosions_timer[i][l].max].is_bomb=false;
					//explosions_timer[i][l].stack[explosions_timer[i][l].max].bomb_id=0;
					explosions_timer[i][l].max++;
         	//explosions_timer[i][l].has_bomb=false;
	        l+=1;
        }
				if (bomber->app->map->get_maptile(i,l)->get_type()==MapTile::BOX || diseaseAt(i,l)) exploding_boxes[i][l]=true;
        explosion=NULL;
      }
			if (!bomber->app->map->get_maptile( i, j )->is_vanishing())
  	  {
    	  int bomb_counter=0;
     	  if (bomber->app->map->get_maptile(i,j)->bomb==NULL)
      	{
      	  //initailization too early, do it elsewhere
      	}
	      else
 	      {
  	      bomb_counter++;
	        int bomb_rating=get_detonation_rating(i,j);
					float bomb_timer=bomber->app->map->get_maptile(i,j)->bomb->get_countdown();
  	      /*if (bomber->app->map->get_maptile(i,j)->bomb->get_countdown()>(float)(2.0)) ground[i][j]=DEADEND_RATING;
					else*/ ground[i][j]=BOMB_RATING;
					explosions_timer[i][j].stack[explosions_timer[i][j].max].timer=67.0/*bomb_timer*/;
					explosions_timer[i][j].stack[explosions_timer[i][j].max].is_bomb=true;
					explosions_timer[i][j].stack[explosions_timer[i][j].max].bomb_id=bomb_counter;
					explosions_timer[i][j].max++;
        	explosions_timer[i][j].has_bomb=true;;
        	if (bomber->app->map->get_maptile(i,j)->get_type()==MapTile::BOX || diseaseAt(i,j)) exploding_boxes[i][j]=true;
        	//left-expansion:
        	int k=i-1;
        	while (!bomber->app->map->get_maptile( k, j )->is_blocking() &&
         	       (k>=0) && (k>=i-bomber->app->map->get_maptile( i, j )->bomb->get_power()) &&
         	       (!extraAt(k+1,j) || k+1==i))
        	{
						explosions_timer[k][j].stack[explosions_timer[k][j].max].timer=bomb_timer;
						//explosions_timer[k][j].stack[explosions_timer[k][j].max].is_bomb=false;
						explosions_timer[k][j].stack[explosions_timer[k][j].max].bomb_id=bomb_counter;
						explosions_timer[k][j].max++;
        		//explosions_timer[k][j].has_bomb=false;
          	if (ground[k][j]!=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING)
          	{
	            if (ground[k][j]<STANDARD_SURE_PLACE_RATING) ground[k][j]-=(bomb_rating+(abs(k-i)));
  	          else ground[k][j]=-bomb_rating+(abs(k-i));
    	      }
      	    k-=1;
        	}
					if (bomber->app->map->get_maptile(k,j)->get_type()==MapTile::BOX || diseaseAt(k,j)) exploding_boxes[k][j]=true;
  	      //right-expansion:
    	    k=i+1;
      	  while (!bomber->app->map->get_maptile( k, j )->is_blocking() &&
        	       (k<MAP_WIDTH) && (k<=i+bomber->app->map->get_maptile( i, j )->bomb->get_power()) &&
          	     (!extraAt(k-1,j) || k-1==i))
	        {
						explosions_timer[k][j].stack[explosions_timer[k][j].max].timer=bomb_timer;
						//explosions_timer[k][j].stack[explosions_timer[k][j].max].is_bomb=false;
						explosions_timer[k][j].stack[explosions_timer[k][j].max].bomb_id=bomb_counter;
						explosions_timer[k][j].max++;
      	  	//explosions_timer[k][j].has_bomb=false;
						if (ground[k][j]!=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING)
          	{
							if (ground[k][j]<STANDARD_SURE_PLACE_RATING) ground[k][j]-=(bomb_rating+(abs(k-i)));
  	        	else ground[k][j]=-bomb_rating+(abs(k-i));
    	      }
      	    k+=1;
        	}
					if (bomber->app->map->get_maptile(k,j)->get_type()==MapTile::BOX || diseaseAt(k,j)) exploding_boxes[k][j]=true;
  	      //up-expansion:
    	    int l=j-1;
      	  while (!bomber->app->map->get_maptile( i, l )->is_blocking() &&
        	       (l>=0) && (l>=j-bomber->app->map->get_maptile( i, j )->bomb->get_power()) &&
          	     (!extraAt(i,l+1) || l+1==j))
	        {
						explosions_timer[i][l].stack[explosions_timer[i][l].max].timer=bomb_timer;
						//explosions_timer[i][l].stack[explosions_timer[i][l].max].is_bomb=false;
						explosions_timer[i][l].stack[explosions_timer[i][l].max].bomb_id=bomb_counter;
						explosions_timer[i][l].max++;
      	  	//explosions_timer[i][l].has_bomb=false;
						if (ground[i][l]!=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING)
  					{
	          	if (ground[i][l]<STANDARD_SURE_PLACE_RATING) ground[i][l]-=(bomb_rating+(abs(l-j)));
  	        	else ground[i][l]=-bomb_rating+(abs(l-j));
    	      }
      	    l-=1;
        	}
					if (bomber->app->map->get_maptile(i,l)->get_type()==MapTile::BOX || diseaseAt(i,l)) exploding_boxes[i][l]=true;
  	      //down-expansion:
    	    l=j+1;
      	  while (!bomber->app->map->get_maptile( i, l )->is_blocking() &&
        	       (l<MAP_HEIGHT) && (l<=j+bomber->app->map->get_maptile( i, j )->bomb->get_power())&&
          	     (!extraAt(i,l-1) || l-1==j))
	        {
						explosions_timer[i][l].stack[explosions_timer[i][l].max].timer=bomb_timer;
						//explosions_timer[i][l].stack[explosions_timer[i][l].max].is_bomb=false;
						explosions_timer[i][l].stack[explosions_timer[i][l].max].bomb_id=bomb_counter;
						explosions_timer[i][l].max++;
      	  	//explosions_timer[i][l].has_bomb=false;
						if (ground[i][l]!=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING)
						{
	          	if (ground[i][l]<STANDARD_SURE_PLACE_RATING) ground[i][l]-=(bomb_rating+(abs(l-j)));
  	        	else ground[i][l]=-bomb_rating+(abs(l-j));
						}
      	    l+=1;
       	  }
					if (bomber->app->map->get_maptile(i,l)->get_type()==MapTile::BOX || diseaseAt(i,l)) exploding_boxes[i][l]=true;
        }
      }
    }
  }
}//compute_ground_rating

void Controller_AI_MiC::rate_around(int **pground)
{
  int x=bomber->get_x();
  int y=bomber->get_y();
  int mapx=(x+20)/40;
  int mapy=(y+20)/40;
  //if (flight_from_bomb) pground[mapx][mapy]=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING;
  if (pground[mapx][mapy]<STANDARD_SURE_PLACE_RATING && pground[mapx][mapy]!=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING && pground[mapx][mapy]!=BOMB_RATING) pground[mapx][mapy]+=AFTER_BOMB_COUNTDOWN_RATING;
  if ((mapx-1)>0 && pground[mapx-1][mapy]!=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING && pground[mapx-1][mapy]!=BOMB_RATING) pground[mapx-1][mapy]+=get_direction_rating(DIR_LEFT,mapx,mapy,pground);
  if ((mapy-1)>0 && pground[mapx][mapy-1]!=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING && pground[mapx][mapy-1]!=BOMB_RATING) pground[mapx][mapy-1]+=get_direction_rating(DIR_UP,mapx,mapy,pground);
  if ((mapx+1)<MAP_WIDTH && pground[mapx+1][mapy]!=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING && pground[mapx+1][mapy]!=BOMB_RATING) pground[mapx+1][mapy]+=get_direction_rating(DIR_RIGHT,mapx,mapy,pground);
  if ((mapy+1)<MAP_HEIGHT && pground[mapx][mapy+1]!=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING && pground[mapx][mapy+1]!=BOMB_RATING) pground[mapx][mapy+1]+=get_direction_rating(DIR_DOWN,mapx,mapy,pground);
  //kick bombs if deadends around you!
  if (getting_paths)
	{
		if (bomber->is_able_to_kick() && flight_from_bomb)
		{
 		  if (((mapx-1)>0 && pground[mapx-1][mapy]<=BOMB_RATING) && ((mapy-1)>0 && pground[mapx][mapy-1]<=BOMB_RATING) &&
					((mapx+1)<MAP_WIDTH && pground[mapx+1][mapy]<=BOMB_RATING) && ((mapy+1)<MAP_HEIGHT && pground[mapx][mapy+1]<=BOMB_RATING))
 	  	{
  		  pground[mapx][mapy]=KICK_POSITION_RATING;
	      if ((mapx-1)>0 && pground[mapx-1][mapy]==BOMB_RATING && (mapx-2)>0 && pground[mapx-2][mapy]>BOMB_RATING) pground[mapx-1][mapy]=1+random(7);
  			if ((mapy-1)>0 && pground[mapx][mapy-1]==BOMB_RATING && (mapy-2)>0 && pground[mapx][mapy-2]>BOMB_RATING) pground[mapx][mapy-1]=1+random(7);
			  if ((mapx+1)<MAP_WIDTH && pground[mapx+1][mapy]==BOMB_RATING && (mapx+2)<MAP_WIDTH && pground[mapx+2][mapy]>BOMB_RATING) pground[mapx+1][mapy]=1+random(7);
			  if ((mapy+1)<MAP_HEIGHT && pground[mapx][mapy+1]==BOMB_RATING && (mapy+2)<MAP_HEIGHT && pground[mapx][mapy+2]<BOMB_RATING) pground[mapx][mapy+1]=1+random(7);
			}
		}
	}
}//rate_around

bool Controller_AI_MiC::feasible_dir(int bx, int by, int dir)
{
  if (dir==DIR_LEFT && (bx-1)>=0) return (ground[bx-1][by]>STANDARD_SURE_PLACE_RATING);
  if (dir==DIR_RIGHT && (bx+1)<MAP_WIDTH) return (ground[bx+1][by]>STANDARD_SURE_PLACE_RATING);
  if (dir==DIR_UP && (by-1)>=0) return (ground[bx][by-1]>STANDARD_SURE_PLACE_RATING);
  if (dir==DIR_DOWN && (by+1)<MAP_HEIGHT) return (ground[bx][by+1]>STANDARD_SURE_PLACE_RATING);
  return false;
}//feasible_dir

bool Controller_AI_MiC::is_opposite_dir(int dir)
{
  if((direction==DIR_LEFT && dir==DIR_RIGHT) ||
     (direction==DIR_RIGHT && dir==DIR_LEFT) ||
		 (direction==DIR_DOWN && dir==DIR_UP) ||
		 (direction==DIR_UP && dir==DIR_DOWN))
  return true;
  else return false;
}//is_opposite_dir

bool Controller_AI_MiC::bomber_pos_will_change()
{
	int bomber_x=bomber->get_x();
  int bomber_y=bomber->get_y();
  int mapx=(bomber_x+20)/40;
  int mapy=(bomber_y+20)/40;
  if (direction==DIR_LEFT)
  {
	  int new_bomber_x=bomber_x-1;
		int new_mapx=(new_bomber_x+20)/40;
    if (mapx==new_mapx) return false; else return true;
  }
	else if (direction==DIR_RIGHT)
  {
	  int new_bomber_x=bomber_x+1;
    int new_mapx=(new_bomber_x+20)/40;
    if (mapx==new_mapx) return false; else return true;
  }
	else if (direction==DIR_UP)
  {
	  int new_bomber_y=bomber_y-1;
    int new_mapy=(new_bomber_y+20)/40;
    if (mapy==new_mapy) return false; else return true;
  }
	else if (direction==DIR_DOWN)
  {
	  int new_bomber_y=bomber_y+1;
    int new_mapy=(new_bomber_y+20)/40;
    if (mapy==new_mapy) return false; else return true;
  }
  return false;
}//bomber_pos_will_change

bool Controller_AI_MiC::could_go_on_with_same_direction()
{
  if (direction==DIR_NONE) return false;
	int bomber_x=bomber->get_x();
  int bomber_y=bomber->get_y();
  int mapx=(bomber_x+20)/40;
  int mapy=(bomber_y+20)/40;
  if (direction==DIR_LEFT)
  {
    if ((mapx-1)>=0 && bomber->app->map->get_maptile( mapx-1, mapy )->bomb==NULL && !bomber_pos_has_changed() &&
				!bomber->app->map->get_maptile( mapx-1, mapy )->is_blocking() &&
				!explosions_timer[mapx-1][mapy].has_bomb && can_move_in_time(explosions_timer[mapx-1][mapy].stack[0].timer) &&
				!bomber->app->map->get_maptile( mapx, mapy )->is_vanishing() && bomb_mode!=ALWAYS)
		return true; else return false;
  }
	else if (direction==DIR_RIGHT)
  {
    if ((mapx+1)<MAP_WIDTH && bomber->app->map->get_maptile( mapx+1, mapy )->bomb==NULL && !bomber_pos_has_changed() &&
				!bomber->app->map->get_maptile( mapx+1, mapy )->is_blocking() &&
				!explosions_timer[mapx+1][mapy].has_bomb && can_move_in_time(explosions_timer[mapx+1][mapy].stack[0].timer) &&
			  !bomber->app->map->get_maptile( mapx, mapy )->is_vanishing() && bomb_mode!=ALWAYS)
		return true; else return false;
  }
	else if (direction==DIR_UP)
  {
    if ((mapy-1)>=0 && bomber->app->map->get_maptile( mapx, mapy-1 )->bomb==NULL && !bomber_pos_has_changed() &&
				!bomber->app->map->get_maptile( mapx, mapy-1 )->is_blocking() &&
				!explosions_timer[mapx][mapy-1].has_bomb && can_move_in_time(explosions_timer[mapx][mapy-1].stack[0].timer) &&
				!bomber->app->map->get_maptile( mapx, mapy )->is_vanishing() && bomb_mode!=ALWAYS)
		return true; else return false;
  }
	else if (direction==DIR_DOWN)
  {
    if ((mapy+1)<MAP_HEIGHT && bomber->app->map->get_maptile( mapx, mapy+1 )->bomb==NULL && !bomber_pos_has_changed() &&
				!bomber->app->map->get_maptile( mapx, mapy+1 )->is_blocking() &&
        !explosions_timer[mapx][mapy+1].has_bomb && can_move_in_time(explosions_timer[mapx][mapy+1].stack[0].timer) &&
			  !bomber->app->map->get_maptile( mapx, mapy )->is_vanishing() && bomb_mode!=ALWAYS)
		return true; else return false;
  }
  return false;
}//could_go_on_with_same_direction

bool Controller_AI_MiC::bomber_pos_has_changed()
{
  int mapx=(bomber->get_x()+20)/40;
  int mapy=(bomber->get_y()+20)/40;
  return !(last_bomber_posx==mapx && last_bomber_posy==mapy);
}//bomber_pos_has_changed

bool Controller_AI_MiC::is_foreign_bomber_at(int posx, int posy)
{
  if (posx<0 || posx>=MAP_WIDTH || posy<0 || posy>=MAP_HEIGHT) return false;
  if (!bomber->app->map->get_maptile( posx, posy )->is_vanishing())
  {
    int bomber_mapx=(bomber->get_x()+20)/40;
    int bomber_mapy=(bomber->get_y()+20)/40;
    CL_Iterator<GameObject> object_counter(bomber->app->map->get_maptile( posx, posy )->objects);
    while (object_counter.next() != NULL)
      if (object_counter()->get_type() == GameObject::BOMBER)
      {
        Bomber* foreign_bomber=(Bomber*)object_counter();
        if (bomber->get_team()==0)
        {
           if (bomber_mapx==posx && bomber_mapy==posy) return false;
					 else return true;
        }
        else
        {
					if ((foreign_bomber->get_team())==(bomber->get_team())) return false;
					else return true;
        }
			}
    return false;
  }
  return false;
}//is_foreign_bomber_at

bool Controller_AI_MiC::is_friendly_bomber_at(int posx, int posy)
{
  if (posx<0 || posx>=MAP_WIDTH || posy<0 || posy>=MAP_HEIGHT) return false;
  if (bomber->get_team()==0) return false;
  if (!bomber->app->map->get_maptile( posx, posy )->is_vanishing())
  {
//    int bomber_mapx=(bomber->get_x()+20)/40;
//    int bomber_mapy=(bomber->get_y()+20)/40;
    CL_Iterator<GameObject> object_counter(bomber->app->map->get_maptile( posx, posy )->objects);
    while (object_counter.next() != NULL)
      if (object_counter()->get_type() == GameObject::BOMBER)
      {
        Bomber* foreign_bomber=(Bomber*)object_counter();
				if ((foreign_bomber->get_team())==(bomber->get_team()) && !(foreign_bomber==bomber)) return true;
			}
    return false;
  }
  return false;
}//is_friendly_bomber_at

bool Controller_AI_MiC::would_destroy_extras_at(int mapx, int mapy)
{
  int i=1;
  while((mapy-i)>=0 && bomber->get_power()-i>=0 &&
        bomber->app->map->get_maptile( mapx, mapy-i )->get_type()!=MapTile::WALL)
  {
    if (extraAt(mapx,mapy-i) && !diseaseAt(mapx,mapy-i)) return true;
		if (exploding_boxes[mapx][mapy-i]) return true;
    i++;
  }
  i=1;
  while((mapx+i)<MAP_WIDTH && bomber->get_power()-i>=0 &&
        bomber->app->map->get_maptile( mapx+i, mapy )->get_type()!=MapTile::WALL)
  {
    if (extraAt(mapx+i,mapy) && !diseaseAt(mapx+i,mapy)) return true;
		if (exploding_boxes[mapx+i][mapy]) return true;
    i++;
  }
  i=1;
  while((mapy+i)<MAP_HEIGHT && bomber->get_power()-i>=0 &&
        bomber->app->map->get_maptile( mapx, mapy+i )->get_type()!=MapTile::WALL)
  {
    if (extraAt(mapx,mapy+i) && !diseaseAt(mapx,mapy+i)) return true;
		if (exploding_boxes[mapx][mapy+i]) return true;
    i++;
  }
  i=1;
  while((mapx-i)>=0 && bomber->get_power()-i>=0 &&
        bomber->app->map->get_maptile( mapx-i, mapy )->get_type()!=MapTile::WALL)
  {
    if (extraAt(mapx-i,mapy) && !diseaseAt(mapx-i,mapy)) return true;
		if (exploding_boxes[mapx-i][mapy]) return true;
    i++;
  }
  return false;
}//would_destroy_extras_at

int Controller_AI_MiC::get_direction()
{
  int x=bomber->get_x();
  int y=bomber->get_y();
  int mapx=(x+20)/40;
  int mapy=(y+20)/40;
  int ratings[4];
  int rating=-99999;
  //int rating=AFTER_BOMB_COUNTDOWN_RATING-STARTING_BOMB_COUNTDOWN_RATING;
  //int rating=ground[mapx][mapy]; //rating of current position
  int best_dir[4]={-2,-2,-2,-2};
  int dir_same=0;
  dout("center: "<<ground[mapx][mapy]);
//left:
  if ((mapx-1)>=0)
		if (ground[mapx-1][mapy]>=rating)
    {
      if (ground[mapx-1][mapy]>rating)
      {
        dir_same=0;
        best_dir[dir_same]=DIR_LEFT;
        for (int i=1;i<4;i++) best_dir[i]=-2;
      }
      else
      {
        if (best_dir[dir_same]!=-2) dir_same++;
        best_dir[dir_same]=DIR_LEFT;
      }
      rating=ground[mapx-1][mapy];
      ratings[DIR_LEFT]=rating;
    }
    dout("  left: "<<ground[mapx-1][mapy]);
//right:
  if ((mapx+1)<MAP_WIDTH)
    if (ground[mapx+1][mapy]>=rating)
 	  {
 	    if (ground[mapx+1][mapy]>rating)
      {
        dir_same=0;
        best_dir[dir_same]=DIR_RIGHT;
        for (int i=1;i<4;i++) best_dir[i]=-2;
      }
      else
      {
          if ( best_dir[dir_same]!=-2) dir_same++;
        best_dir[dir_same]=DIR_RIGHT;
      }
      rating=ground[mapx+1][mapy];
      ratings[DIR_RIGHT]=rating;
		}
    dout("  right: "<<ground[mapx+1][mapy]);
//up:
  if ((mapy-1)>=0)
  	if (ground[mapx][mapy-1]>=rating)
    {
	 	  if (ground[mapx][mapy-1]>rating)
      {
        dir_same=0;
        best_dir[dir_same]=DIR_UP;
        for (int i=1;i<4;i++) best_dir[i]=-2;
      }
      else
      {
        if ( best_dir[dir_same]!=-2) dir_same++;
        best_dir[dir_same]=DIR_UP;
      }
      rating=ground[mapx][mapy-1];
      ratings[DIR_UP]=rating;
   	}
    dout("  up: "<<ground[mapx][mapy-1]);
//down:
  if ((mapy+1)<MAP_HEIGHT)
    if (ground[mapx][mapy+1]>=rating)
    {
			if (ground[mapx][mapy+1]>rating)
      {
        dir_same=0;
        best_dir[dir_same]=DIR_DOWN;
        for (int i=1;i<4;i++) best_dir[i]=-2;
      }
      else
      {
         if (best_dir[dir_same]!=-2) dir_same++;
        best_dir[dir_same]=DIR_DOWN;
      }
      rating=ground[mapx][mapy+1];
      ratings[DIR_DOWN]=rating;
   	}
    dout("  down: "<<ground[mapx][mapy+1]<<endl);
    dout("--------bestes rating: "<<rating<<endl);
  if (rating<ground[mapx][mapy])
  {
    dout("ich bewege mich lieber nicht!!!"<<endl);
    last_dir_rating=0;
    return DIR_NONE;
  }
  for(int i=0;i<=dir_same;i++)
    if (best_dir[i]==direction)
    {
      dout("...w�hle dir aus letztem frame: "<<direction<<endl);
      last_dir_rating=ratings[direction];
      return direction;
    }
  dout(best_dir[0]<<" "<<best_dir[1]<<" "<<best_dir[2]<<" "<<best_dir[3]<<endl);
  int random_dir=best_dir[random(dir_same+1)];
  dout("muss per zufall ausw�hlen...: "<<random_dir<<endl);
  if (random_dir==-2)
  {
    last_dir_rating=0;
    return DIR_NONE;
  }
  else
  {
   	last_dir_rating=ratings[random_dir];
   	return random_dir;
  }
}//get_direction

int Controller_AI_MiC::get_extra_rating(int extra_x, int extra_y, int bomber_x, int bomber_y)
{
  if(!bomber->app->map->get_maptile( extra_x, extra_y )->is_vanishing())
  {
    bool bomber_has_viagra=(bomb_mode==ALWAYS);
		CL_Iterator<GameObject> object_counter(bomber->app->map->get_maptile( extra_x, extra_y )->objects);
    int rating=0;
    while (object_counter.next() != NULL)
      if ( object_counter()->get_type()==GameObject::EXTRA )
      {
        Extra* extra= (Extra*)object_counter();
        switch (extra->get_ExtraType())
        {
         case Extra::BOMB : if (bomber_has_viagra) rating+=EXTRA_RATING_WITH_VIAGRA;
														else rating+=EXTRA_BOMB_RATING; break;
         case Extra::POWER : if (bomber_has_viagra) rating+=EXTRA_RATING_WITH_VIAGRA;
														 else rating+=EXTRA_POWER_RATING; break;
         case Extra::KICK  : if (bomber_has_viagra) rating+=EXTRA_RATING_WITH_VIAGRA;
                             else if (!bomber->is_able_to_kick()) rating+=WITHOUT_EXTRA_KICK_RATING;
														 else rating+=EXTRA_KICK_RATING; break;
         case Extra::GLOVE : if (bomber_has_viagra) rating+=EXTRA_RATING_WITH_VIAGRA;
                             else if (!bomber->is_able_to_throw()) rating+=WITHOUT_EXTRA_GLOVE_RATING;
														 else rating+=EXTRA_GLOVE_RATING;  break;
         case Extra::SKATEBOARD: if (bomber_has_viagra) rating+=EXTRA_RATING_WITH_VIAGRA;
																 else rating+=EXTRA_SKATEBOARD_RATING; break;
         case Extra::JOINT : if (bomber_has_viagra) rating+=EXTRA_RATING_WITH_VIAGRA;
														 else rating+=EXTRA_JOINT_RATING; break;
         case Extra::KOKS  : if (bomber_has_viagra) rating+=EXTRA_RATING_WITH_VIAGRA;
														 else rating+=EXTRA_KOKS_RATING; break;
         case Extra::VIAGRA: if (bomber_has_viagra) rating+=AFTER_BOMB_COUNTDOWN_RATING;
														 else rating+=EXTRA_VIAGRA_RATING; break;
         default: break;
        }
      }
    int tmp=(abs(bomber_x-extra_x)+abs(bomber_y-extra_y));
    if (tmp==0) tmp=1;
    return (rating/tmp);
  }
  else return -77;
}//get_extra_rating

int Controller_AI_MiC::get_extra_rating(int extra_x,int extra_y,int edge1_x,int edge1_y,int bomber_x,int bomber_y)
{
  if(!bomber->app->map->get_maptile( extra_x, extra_y )->is_vanishing())
  {
    bool bomber_has_viagra=(bomb_mode==ALWAYS);
    CL_Iterator<GameObject> object_counter(bomber->app->map->get_maptile( extra_x, extra_y )->objects);
    int rating=0;
    while (object_counter.next() != NULL)
      if ( object_counter()->get_type()==GameObject::EXTRA )
      {
        Extra* extra= (Extra*)object_counter();
        switch (extra->get_ExtraType())
        {
         case Extra::BOMB : if (bomber_has_viagra) rating+=EXTRA_RATING_WITH_VIAGRA;
														else rating+=EXTRA_BOMB_RATING; break;
         case Extra::POWER : if (bomber_has_viagra) rating+=EXTRA_RATING_WITH_VIAGRA;
														 else rating+=EXTRA_POWER_RATING; break;
         case Extra::KICK  : if (bomber_has_viagra) rating+=EXTRA_RATING_WITH_VIAGRA;
														 else if (!bomber->is_able_to_kick()) rating+=WITHOUT_EXTRA_KICK_RATING;
														 else rating+=EXTRA_KICK_RATING; break;
         case Extra::GLOVE : if (bomber_has_viagra) rating+=EXTRA_RATING_WITH_VIAGRA;
														 else if (!bomber->is_able_to_throw()) rating+=WITHOUT_EXTRA_GLOVE_RATING;
														 else rating+=EXTRA_GLOVE_RATING;  break;
         case Extra::SKATEBOARD: if (bomber_has_viagra) rating+=EXTRA_RATING_WITH_VIAGRA;
																 else rating+=EXTRA_SKATEBOARD_RATING; break;
         case Extra::JOINT : if (bomber_has_viagra) rating+=EXTRA_RATING_WITH_VIAGRA;
														 else rating+=EXTRA_JOINT_RATING; break;
         case Extra::KOKS  : if (bomber_has_viagra) rating+=EXTRA_RATING_WITH_VIAGRA;
														 else rating+=EXTRA_KOKS_RATING; break;
         case Extra::VIAGRA: if (bomber_has_viagra) rating+=AFTER_BOMB_COUNTDOWN_RATING;
														 else rating+=EXTRA_VIAGRA_RATING; break;
         default: break;
        }
      }
    int tmp=(abs(bomber_x-edge1_x)+abs(edge1_x-extra_x)+abs(bomber_y-edge1_y)+abs(edge1_y-extra_y));
    if (tmp==0) tmp=1;
		return (rating/tmp);
  }
  else return -77;
}//get_extra_rating

int Controller_AI_MiC::get_box_number(int mapx, int mapy)
//returns -1 if an extra will be destroyed or
//					 if a box perhaps will be hit by another bomb
{
  if (mapx<0 || mapx>=MAP_WIDTH || mapy<0 || mapy>=MAP_HEIGHT) return -1;
  int num=0;
	int i=1;
  while((mapy-i)>=0 && bomber->get_power()-i>=0 &&
        bomber->app->map->get_maptile( mapx, mapy-i )->get_type()!=MapTile::WALL)
  {
    if (extraAt(mapx,mapy-i) && !diseaseAt(mapx,mapy-i)) return -1; //i don't like to destroy extras!
		if (exploding_boxes[mapx][mapy-i]) return -1; //another bomb perhaps will destroy this box
    if (is_foreign_bomber_at(mapx,mapy-i))
		{
		  if (i>0) num+=(FOREIGN_BOMBER_HIT_RATING);
			else num+=FOREIGN_BOMBER_HIT_RATING;
		}
		if (bomber->app->map->get_maptile( mapx, mapy-i )->get_type()==MapTile::BOX)
    {
      num+=5;
      break;
    }
    else
      if (extraAt(mapx,mapy-i))
			{
			 	if (diseaseAt(mapx,mapy-i)) num+=1;
        break;
      }
    i++;
  }
  i=1;
  while((mapx+i)<MAP_WIDTH && bomber->get_power()-i>=0 &&
        bomber->app->map->get_maptile( mapx+i, mapy )->get_type()!=MapTile::WALL)
  {
    if (extraAt(mapx+i,mapy) && !diseaseAt(mapx+i,mapy)) return -1; //i don't like to destroy extras!
		if (exploding_boxes[mapx+i][mapy]) return -1; //another bomb perhaps will destroy this box
    if (is_foreign_bomber_at(mapx+i,mapy))
		{
		  if (i>0) num+=(FOREIGN_BOMBER_HIT_RATING);
			else num+=FOREIGN_BOMBER_HIT_RATING;
		}
    if (bomber->app->map->get_maptile( mapx+i, mapy )->get_type()==MapTile::BOX)
    {
      num+=5;
      break;
    }
    else
      if (extraAt(mapx+i,mapy))
		  {
			  if (diseaseAt(mapx+i,mapy)) num+=1;
        break;
      }
    i++;
  }
  i=1;
  while((mapy+i)<MAP_HEIGHT && bomber->get_power()-i>=0 &&
        bomber->app->map->get_maptile( mapx, mapy+i )->get_type()!=MapTile::WALL)
  {
    if (extraAt(mapx,mapy+i) && !diseaseAt(mapx,mapy+i)) return -1; //i don't like to destroy extras!
		if (exploding_boxes[mapx][mapy+i]) return -1; //another bomb perhaps will destroy this box
    if (is_foreign_bomber_at(mapx,mapy+i))
		{
		  if (i>0) num+=(FOREIGN_BOMBER_HIT_RATING);
			else num+=FOREIGN_BOMBER_HIT_RATING;
		}
		if (bomber->app->map->get_maptile( mapx, mapy+i )->get_type()==MapTile::BOX)
    {
      num+=5;
      break;
    }
    else
      if (extraAt(mapx,mapy+i))
			{
        if (diseaseAt(mapx,mapy+i)) num+=1;
			  break;
      }
    i++;
  }
  i=1;
  while((mapx-i)>=0 && bomber->get_power()-i>=0 &&
        bomber->app->map->get_maptile( mapx-i, mapy )->get_type()!=MapTile::WALL)
  {
    if (extraAt(mapx-i,mapy) && !diseaseAt(mapx-i,mapy)) return -1; //i don't like to destroy extras!
		if (exploding_boxes[mapx-i][mapy]) return -1; //another bomb perhaps will destroy this box
    if (is_foreign_bomber_at(mapx-i,mapy))
		{
		  if (i>0) num+=(FOREIGN_BOMBER_HIT_RATING);
			else num+=FOREIGN_BOMBER_HIT_RATING;
		}
    if (bomber->app->map->get_maptile( mapx-i, mapy )->get_type()==MapTile::BOX)
    {
      num+=5;
      break;
    }
    else
      if (extraAt(mapx-i,mapy))
			{
 			  if (diseaseAt(mapx-i,mapy)) num+=1;
        break;
      }
    i++;
  }
  int bx=(bomber->get_x()+20)/40;
  int by=(bomber->get_y()+20)/40;
  int tmp=(abs(bx-mapx)+abs(by-mapy));
  if (tmp==0) tmp=1;
  return (num/tmp);
}//get_box_number

int Controller_AI_MiC::get_detonation_rating(int i, int j)
{
  float detonation=bomber->app->map->get_maptile( i, j )->bomb->get_countdown();
  int countdown=(int)(detonation*2000);
  return (7592-countdown)*3;
}//get_detonation_rating

bool Controller_AI_MiC::there_are_my_bombs_on_the_ground()
{
  for (int y=0;y<MAP_HEIGHT;y++)
  {
    for (int x=0;x<MAP_WIDTH;x++)
    {
	    if (bomber->app->map->get_maptile(x,y)->bomb!=NULL && bomber->app->map->get_maptile(x,y)->bomb->get_bomber()==bomber)
      return true;
    }
  }
  return false;
}//there_are_my_bombs_on_the_ground

void Controller_AI_MiC::reset()
{
	direction=DIR_NONE;
  last_dir_rating=0;
	last_bomber_posx=(bomber->get_x()+20)/40;
  last_bomber_posy=(bomber->get_y()+20)/40;
  flight_from_bomb=false;
	bombing=false;
  simulation_mode_on=false;
  checking_wise_moves=false;
	getting_paths=false;
}//reset

void Controller_AI_MiC::update()
{
  dout("+++ update call ----------------------------------------------------"<<endl);
	if (!active) return;

// dok part starts here
	int off_x = (bomber->get_x()+20) % 40;
	int off_y = (bomber->get_y()+20) % 40;
	if (direction==DIR_LEFT)
	{
		if (off_x > 32) return;
	} else
	if (direction==DIR_RIGHT)
	{
		if (off_x < 7) return;
	} else
	if (direction==DIR_UP)
	{
		if (off_y > 32) return;
	} else
	if (direction==DIR_DOWN)
	{
		if (off_y < 7) return;
	}
// dok part ends here

  init_non_ground();
	init_exploding_boxes();
  init_explosions_timer();
  compute_ground_rating();
  update_explosions_timer();
  int mapx=(bomber->get_x()+20)/40;
  int mapy=(bomber->get_y()+20)/40;
	if (bomber_pos_has_changed()) dout("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ position gewechselt !!!"<<endl);
  else dout("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ wechsle meine position nicht !!!"<<endl);
  if (could_go_on_with_same_direction())
   {
    dout("coool - gehe mit sicherheit weiter ohne nachzudenken !!!"<<endl);
		last_bomber_posx=mapx;
  	last_bomber_posy=mapy;
    return;
  }
  bool will_put_bomb=false;
	if (ground[mapx][mapy]>STANDARD_SURE_PLACE_RATING) flight_from_bomb=false;
  else if (ground[mapx][mapy]<NEARBY_ON_BOMB_RATING) flight_from_bomb=true;
	dout("update:    bomberposition: "<<mapx<<","<<mapy<<endl);
  dout("probiere bombe zu legen!!!"<<endl);
  if (flight_from_bomb) dout(" ======= fliehe vor einer bombe ========"<<endl);
  else dout(" ======= muss vor keiner bombe fliehen ========="<<endl);
  adding_bomb_to_ground=true;
	if (bomb_mode==ALWAYS)
	{
  	bombing=true;
    will_put_bomb=true;
  	dout(" ######## bomb mode is always ... #########"<<endl);
	}
	else
	{
	  bombing=false;
	  if (try_bombing(mapx,mapy) && !would_destroy_extras_at(mapx,mapy))
		{
	    //don't put a bomb if box-rating around is better
	    /*int curr_box_number=get_box_number(mapx,mapy);
	    if ((get_box_number(mapx+1,mapy)>curr_box_number && direction==DIR_RIGHT && try_bombing(mapx+1,mapy))||
	   		  (get_box_number(mapx-1,mapy)>curr_box_number && direction==DIR_LEFT && try_bombing(mapx-1,mapy)) ||
	        (get_box_number(mapx,mapy+1)>curr_box_number && direction==DIR_DOWN && try_bombing(mapx,mapy+1)) ||
	        (get_box_number(mapx,mapy-1)>curr_box_number && direction==DIR_UP && try_bombing(mapx,mapy-1)))
	    {
	      //nothing to do (?)
	    }
	    else
	    {*/
   		  will_put_bomb=true;
	   		dout("lege eine bombe!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl);
	    //}
	  }
	}
  adding_bomb_to_ground=false;
  if (will_put_bomb)
	{
	  bomber->put_bomb();
		flight_from_bomb=true;
	}
  init_non_ground();
  init_exploding_boxes();
	init_explosions_timer();
	compute_ground_rating();
  update_explosions_timer();
  getting_paths=true;
	init_paths();
	rate_around(ground);
	getting_paths=false;
//  int last_dir=direction;
  PosXY last_paths[4];
  for (int p=0;p<4;p++)
	{
		last_paths[p].posx=paths[p].posx;
		last_paths[p].posy=paths[p].posy;
	}
  direction=get_direction();
  /*if (direction!=DIR_NONE && !flight_from_bomb && is_opposite_dir(last_dir) &&
			last_paths[last_dir].posx>0 && paths[direction].posx>0 &&
			abs(get_ground_rating(last_dir,mapx,mapy)-get_ground_rating(direction,mapx,mapy))<=17)
  {
		direction=last_dir;
    dout(")))))))((((((( gehe in alter richtung weiter ))))))))((((((("<<endl);
	}*/
  last_bomber_posx=mapx;
  last_bomber_posy=mapy;
  dout("+++ leaving update ------------------------------------------------"<<endl);
}//update

void Controller_AI_MiC::init_non_ground()
{
  for(int i=0; i<MAP_WIDTH;i++)
    for(int j=0; j<MAP_HEIGHT;j++)
      if(!bomber->app->map->get_maptile(i,j)->is_passable() || bomber->app->map->get_maptile(i,j)->is_vanishing())
        ground[i][j]=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING;
      else ground[i][j]=1;
}//init_non_ground

void Controller_AI_MiC::init_exploding_boxes()
{
  for(int i=0; i<MAP_WIDTH;i++)
    for(int j=0; j<MAP_HEIGHT;j++)
			exploding_boxes[i][j]=false;
}//init_exploding_boxes

bool Controller_AI_MiC::extraAt(int i, int j)
{
  if (i<0 || i>=MAP_WIDTH || j<0 || j>=MAP_HEIGHT) return false;
  if(!bomber->app->map->get_maptile( i, j )->is_vanishing())
  {
    CL_Iterator<GameObject> object_counter(bomber->app->map->get_maptile( i, j )->objects);
    while (object_counter.next() != NULL)
      if (object_counter()->get_type() == GameObject::EXTRA)
        return true;
    return false;
  }
  return false;
}//extraAt

bool Controller_AI_MiC::diseaseAt(int i, int j)
{
  if (i<0 || i>=MAP_WIDTH || j<0 || j>=MAP_HEIGHT) return false;
  if(!bomber->app->map->get_maptile( i, j )->is_vanishing())
  {
    CL_Iterator<GameObject> object_counter(bomber->app->map->get_maptile( i, j )->objects);
    while (object_counter.next() != NULL)
      if (object_counter()->get_type() == GameObject::EXTRA)
      {
         Extra* extra=(Extra*)object_counter();
         if (extra->get_ExtraType()==Extra::JOINT ||
             extra->get_ExtraType()==Extra::KOKS ||
             extra->get_ExtraType()==Extra::VIAGRA)
           return true;
      }
    return false;
  }
  return false;
}//diseaseAt

bool Controller_AI_MiC::is_very_dangerous(int i, int j)
//Some special grounds that should not be passable for the AI
{
  if (i<0 || i>=MAP_WIDTH || j<0 || j>=MAP_HEIGHT) return false;
  CL_Iterator<GameObject> object_counter(bomber->app->map->get_maptile( i, j )->objects);
  while (object_counter.next() != NULL)
    if (object_counter()->get_type() == GameObject::EXTRA)
    {
      Extra* extra=(Extra*)object_counter();
      if (extra->get_ExtraType()==Extra::VIAGRA) return true; //because Viagra is very dangerous!
    }
  return false;
}//is_very_dangerous

Explosion* Controller_AI_MiC::get_explosion(int i, int j)
{
  if (i<0 || i>=MAP_WIDTH || j<0 || j>=MAP_HEIGHT) return NULL;
  CL_Iterator<GameObject> object_counter(bomber->app->map->get_maptile( i, j )->objects);
  while (object_counter.next() != NULL)
    if (object_counter()->get_type() == GameObject::EXPLOSION)
      return (Explosion*)object_counter();
  return NULL;
}//get_explosion

void Controller_AI_MiC::add_bomb_to_ground(int i, int j)
{
  ground[i][j]=BOMB_RATING;
  int k=i-1;
 	while (!bomber->app->map->get_maptile( k, j )->is_blocking() &&
   	     (k>=0) && (k>=i-bomber->get_power()) &&
     	   (!extraAt(k+1,j) || k+1==i))
	{
	  if (ground[k][j]!=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING)
    {
 	  	if (ground[k][j]<STANDARD_SURE_PLACE_RATING) ground[k][j]+=(AFTER_BOMB_COUNTDOWN_RATING+(abs(k-i)));
 	   	else ground[k][j]=STARTING_BOMB_COUNTDOWN_RATING+(abs(k-i));
    }
 	  k-=1;
 	}
	//right-expansion:
	k=i+1;
 	while (!bomber->app->map->get_maptile( k, j )->is_blocking() &&
         (k<MAP_WIDTH) && (k<=i+bomber->get_power()) &&
         (!extraAt(k-1,j) || k-1==i))
 	{
	  if (ground[k][j]!=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING)
		{
    	if (ground[k][j]<STANDARD_SURE_PLACE_RATING) ground[k][j]+=(AFTER_BOMB_COUNTDOWN_RATING+(abs(k-i)));
    	else ground[k][j]=STARTING_BOMB_COUNTDOWN_RATING+(abs(k-i));
		}
    k+=1;
  }
	//up-expansion:
 	int l=j-1;
  while (!bomber->app->map->get_maptile( i, l )->is_blocking()  &&
         (l>=0) && (l>=j-bomber->get_power()) &&
   	     (!extraAt(i,l+1) || l+1==j))
  {
	  if (ground[i][l]!=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING)
    {
			if (ground[i][l]<STANDARD_SURE_PLACE_RATING) ground[i][l]+=(AFTER_BOMB_COUNTDOWN_RATING+(abs(l-j)));
   		else ground[i][l]=STARTING_BOMB_COUNTDOWN_RATING+(abs(l-j));
		}
   	l-=1;
  }
	//down-expansion:
 	l=j+1;
  while (!bomber->app->map->get_maptile( i, l )->is_blocking() &&
   	     (l<MAP_HEIGHT) && (l<=j+bomber->get_power()) &&
   	     (!extraAt(i,l-1) || l-1==j))
	{
	  if (ground[i][l]!=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING)
		{
 	 	  if (ground[i][l]<STANDARD_SURE_PLACE_RATING) ground[i][l]+=(AFTER_BOMB_COUNTDOWN_RATING+(abs(l-j)));
   	  else ground[i][l]=STARTING_BOMB_COUNTDOWN_RATING+(abs(l-j));
		}
    l+=1;
	}
}//add_bomb_to_ground

bool Controller_AI_MiC::try_bombing(int i, int j)
{
  if (is_friendly_bomber_at(i,j)) return false;
  int **ground_tmp;
  ground_tmp=new int*[MAP_WIDTH];
  for (int a=0;a<MAP_WIDTH;a++) ground_tmp[a]=new int[MAP_HEIGHT];
  for (int x=0;x<MAP_WIDTH;x++)
    for (int y=0;y<MAP_HEIGHT;y++)
    {
      ground_tmp[x][y]=ground[x][y];
		  /*if (ground[x][y]>STANDARD_SURE_PLACE_RATING) ground_tmp[x][y]=1;
      else ground_tmp[x][y]=DEADEND_RATING;*/
		}
  int curr_box_number=get_box_number(i,j);
	if (curr_box_number>0 && bomber->get_cur_bombs()>0)
	{
    //try to expand
	  if (ground_tmp[i][j]<STANDARD_SURE_PLACE_RATING) ground_tmp[i][j]=BOMB_RATING;
    //ground_tmp[i][j]=BOMB_RATING;
	  //left-expansion:
		explosions_timer[i][j].stack[explosions_timer[i][j].max].timer=(float)2.5;
		explosions_timer[i][j].stack[explosions_timer[i][j].max].is_bomb=true;
		explosions_timer[i][j].stack[explosions_timer[i][j].max].bomb_id=99;
		explosions_timer[i][j].max++;
    explosions_timer[i][j].has_bomb=true;
	  int k=i-1;
 		while (!bomber->app->map->get_maptile( k, j )->is_blocking() &&
    	     (k>=0) && (k>=i-bomber->get_power()) &&
      	   (!extraAt(k+1,j) || k+1==i))
	  {
			if (is_friendly_bomber_at(k,j))
			{
				for (int t=0;t<MAP_WIDTH;t++) delete[] ground_tmp[t];			
			  return false;
			}
      explosions_timer[k][j].stack[explosions_timer[k][j].max].timer=(float)2.5;
			//explosions_timer[k][j].stack[explosions_timer[k][j].max].is_bomb=false;
			explosions_timer[k][j].stack[explosions_timer[k][j].max].bomb_id=99;
			explosions_timer[k][j].max++;
  	  //explosions_timer[k][j].has_bomb=false;
	    if (ground_tmp[k][j]!=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING)
      {
 	    	if (ground_tmp[k][j]<STANDARD_SURE_PLACE_RATING) ground_tmp[k][j]+=(AFTER_BOMB_COUNTDOWN_RATING+(abs(k-i)));
 	    	else ground_tmp[k][j]=STARTING_BOMB_COUNTDOWN_RATING+(abs(k-i));
      }
 	    k-=1;
 	  }
	  //right-expansion:
	  k=i+1;
 	  while (!bomber->app->map->get_maptile( k, j )->is_blocking() &&
  	       (k<MAP_WIDTH) && (k<=i+bomber->get_power()) &&
   	       (!extraAt(k-1,j) || k-1==i))
 	  {
			if (is_friendly_bomber_at(k,j))
			{
				for (int t=0;t<MAP_WIDTH;t++) delete[] ground_tmp[t];					
				return false;
			}
      explosions_timer[k][j].stack[explosions_timer[k][j].max].timer=(float)2.5;
			//explosions_timer[k][j].stack[explosions_timer[k][j].max].is_bomb=false;
			explosions_timer[k][j].stack[explosions_timer[k][j].max].bomb_id=99;
			explosions_timer[k][j].max++;
  	  //explosions_timer[k][j].has_bomb=false;
		  if (ground_tmp[k][j]!=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING)
			{
  	  	if (ground_tmp[k][j]<STANDARD_SURE_PLACE_RATING) ground_tmp[k][j]+=(AFTER_BOMB_COUNTDOWN_RATING+(abs(k-i)));
   	  	else ground_tmp[k][j]=STARTING_BOMB_COUNTDOWN_RATING+(abs(k-i));
			}
   	  k+=1;
  	}
	  //up-expansion:
 	  int l=j-1;
  	while (!bomber->app->map->get_maptile( i, l )->is_blocking()  &&
   	       (l>=0) && (l>=j-bomber->get_power()) &&
    	     (!extraAt(i,l+1) || l+1==j))
  	{
			if (is_friendly_bomber_at(i,l))
			{
				for (int t=0;t<MAP_WIDTH;t++) delete[] ground_tmp[t];		
				return false;
			}
      explosions_timer[i][l].stack[explosions_timer[i][l].max].timer=(float)2.5;
			//explosions_timer[i][l].stack[explosions_timer[i][l].max].is_bomb=false;
			explosions_timer[i][l].stack[explosions_timer[i][l].max].bomb_id=99;
			explosions_timer[i][l].max++;
  	  //explosions_timer[i][l].has_bomb=false;
		  if (ground_tmp[i][l]!=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING)
   	  {
				if (ground_tmp[i][l]<STANDARD_SURE_PLACE_RATING) ground_tmp[i][l]+=(AFTER_BOMB_COUNTDOWN_RATING+(abs(l-j)));
    		else ground_tmp[i][l]=STARTING_BOMB_COUNTDOWN_RATING+(abs(l-j));
			}
    	l-=1;
  	}
	  //down-expansion:
 	  l=j+1;
  	while (!bomber->app->map->get_maptile( i, l )->is_blocking() &&
    	     (l<MAP_HEIGHT) && (l<=j+bomber->get_power()) &&
     	     (!extraAt(i,l-1) || l-1==j))
	  {
			if (is_friendly_bomber_at(i,l))
			{
			  for (int t=0;t<MAP_WIDTH;t++) delete[] ground_tmp[t];		
				return false;
			}
      explosions_timer[i][l].stack[explosions_timer[i][l].max].timer=(float)2.5;
			//explosions_timer[i][l].stack[explosions_timer[i][l].max].is_bomb=false;
			explosions_timer[i][l].stack[explosions_timer[i][l].max].bomb_id=99;
			explosions_timer[i][l].max++;
  	  //explosions_timer[i][l].has_bomb=false;
		  if (ground_tmp[i][l]!=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING)
			{
 	   	  if (ground_tmp[i][l]<STANDARD_SURE_PLACE_RATING) ground_tmp[i][l]+=(AFTER_BOMB_COUNTDOWN_RATING+(abs(l-j)));
  	 	  else ground_tmp[i][l]=STARTING_BOMB_COUNTDOWN_RATING+(abs(l-j));
			}
   	  l+=1;
	  }
 	  dout("try_bombing: center: "<<ground_tmp[i][j]<<"  left: "<<ground_tmp[i-1][j]<<"  up: "<<ground_tmp[i][j-1]<<"  right: "<<ground_tmp[i+1][j]<<"  down: "<<ground_tmp[i][j+1]<<endl);
    update_explosions_timer();
 	  bool way_out=false;
    //flight_from_bomb=true;
    simulation_mode_on=true; //start simulation!
 	  if(get_direction_rating(DIR_LEFT,i,j,ground_tmp)!=DEADEND_RATING)
 	  {
  	  dout("links keine sackgasse!"<<endl);
   	  way_out=true;
      sure_directions_after_bombing[DIR_LEFT]=true;
   	}
    else sure_directions_after_bombing[DIR_LEFT]=false;
 	  if(get_direction_rating(DIR_UP,i,j,ground_tmp)!=DEADEND_RATING)
  	{
   	  dout("oben keine sackgasse!"<<endl);
    	way_out=true;
      sure_directions_after_bombing[DIR_UP]=true;
    }
    else sure_directions_after_bombing[DIR_UP]=false;
 	  if(get_direction_rating(DIR_RIGHT,i,j,ground_tmp)!=DEADEND_RATING)
  	{
   	  dout("rechts keine sackgasse!"<<endl);
    	way_out=true;
      sure_directions_after_bombing[DIR_RIGHT]=true;
   	}
    else sure_directions_after_bombing[DIR_RIGHT]=false;
  	if(get_direction_rating(DIR_DOWN,i,j,ground_tmp)!=DEADEND_RATING)
  	{
   	  dout("unten keine sackgasse!"<<endl);
    	way_out=true;
      sure_directions_after_bombing[DIR_DOWN]=true;
    }
    else sure_directions_after_bombing[DIR_DOWN]=false;
    simulation_mode_on=false;
    if (way_out /*&& ground_tmp[i][j]>STANDARD_SURE_PLACE_RATING*/ && (bomber->app->map->get_maptile(i,j)->get_type()!=MapTile::ARROW ||
				(bomber->app->map->get_maptile(i,j)->get_type()==MapTile::ARROW && ((MapTile_Arrow*)(bomber->app->map->get_maptile(i,j)))->get_direction()!=direction)))
  	{
   	  dout("bomber hat einen ausweg!"<<endl);
    	dout("bomberposition: "<<i<<" , "<<j<<"      boxen: "<<get_box_number(i,j)<<endl);
      for (int t=0;t<MAP_WIDTH;t++) delete[] ground_tmp[t];
    	return true;
  	}
  	else dout("aber ich mag keine kettenreaktionen...!"<<endl);
  	dout("bomberposition: "<<i<<" , "<<j<<"      boxen: "<<get_box_number(i,j)<<endl);
  	dout("neeeeeeeeeeeeeein!"<<endl);
	}
	else dout("leider keine boxen oder bomben da!!!"<<endl);
  for (int t=0;t<MAP_WIDTH;t++) delete[] ground_tmp[t];
  return false;
}//try_bombing

bool Controller_AI_MiC::there_are_wise_moves(int i, int j)
{
  int **ground_tmp;
  ground_tmp=new int*[MAP_WIDTH];
  for (int a=0;a<MAP_WIDTH;a++) ground_tmp[a]=new int[MAP_HEIGHT];
  for (int x=0;x<MAP_WIDTH;x++)
    for (int y=0;y<MAP_HEIGHT;y++)
      ground_tmp[x][y]=ground[x][y];

  int k=i-1;
	while (!bomber->app->map->get_maptile( k, j )->is_blocking() &&
   	     (k>=0) && (k>=i-bomber->get_power()) &&
     	   (!extraAt(k+1,j) || k+1==i))
  {
    if (ground_tmp[k][j]!=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING)
    {
    	if (ground_tmp[k][j]<STANDARD_SURE_PLACE_RATING) ground_tmp[k][j]+=(AFTER_BOMB_COUNTDOWN_RATING+(abs(k-i)));
    	else ground_tmp[k][j]=STARTING_BOMB_COUNTDOWN_RATING+(abs(k-i));
    }
    k-=1;
  }
  k=i+1;
  while (!bomber->app->map->get_maptile( k, j )->is_blocking() &&
 	       (k<MAP_WIDTH) && (k<=i+bomber->get_power()) &&
 	       (!extraAt(k-1,j) || k-1==i))
  {
	  if (ground_tmp[k][j]!=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING)
		{
 	  	if (ground_tmp[k][j]<STANDARD_SURE_PLACE_RATING) ground_tmp[k][j]+=(AFTER_BOMB_COUNTDOWN_RATING+(abs(k-i)));
 	  	else ground_tmp[k][j]=STARTING_BOMB_COUNTDOWN_RATING+(abs(k-i));
		}
 	  k+=1;
 	}
  int l=j-1;
 	while (!bomber->app->map->get_maptile( i, l )->is_blocking()  &&
 	       (l>=0) && (l>=j-bomber->get_power()) &&
   	     (!extraAt(i,l+1) || l+1==j))
 	{
	  if (ground_tmp[i][l]!=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING)
 	  {
			if (ground_tmp[i][l]<STANDARD_SURE_PLACE_RATING) ground_tmp[i][l]+=(AFTER_BOMB_COUNTDOWN_RATING+(abs(l-j)));
   		else ground_tmp[i][l]=STARTING_BOMB_COUNTDOWN_RATING+(abs(l-j));
		}
   	l-=1;
 	}
  l=j+1;
 	while (!bomber->app->map->get_maptile( i, l )->is_blocking() &&
   	     (l<MAP_HEIGHT) && (l<=j+bomber->get_power()) &&
   	     (!extraAt(i,l-1) || l-1==j))
  {
	  if (ground_tmp[i][l]!=UNPASSABLE_BLOCKING_VANISHING_EXPLODING_RATING)
		{
   	  if (ground_tmp[i][l]<STANDARD_SURE_PLACE_RATING) ground_tmp[i][l]+=(AFTER_BOMB_COUNTDOWN_RATING+(abs(l-j)));
 	 	  else ground_tmp[i][l]=STARTING_BOMB_COUNTDOWN_RATING+(abs(l-j));
		}
 	  l+=1;
  }
  bool way_out=false;
  simulation_mode_on=true;
  checking_wise_moves=true;
  if(get_direction_rating(DIR_LEFT,i,j,ground_tmp)!=DEADEND_RATING) way_out=true;
  if(get_direction_rating(DIR_UP,i,j,ground_tmp)!=DEADEND_RATING) way_out=true;
  if(get_direction_rating(DIR_RIGHT,i,j,ground_tmp)!=DEADEND_RATING) way_out=true;
 	if(get_direction_rating(DIR_DOWN,i,j,ground_tmp)!=DEADEND_RATING)	way_out=true;
  checking_wise_moves=false;
  simulation_mode_on=false;
  for (int t=0;t<MAP_WIDTH;t++) delete[] ground_tmp[t];
  if (way_out) return true;
  else return false;
}//there_are_wise_moves

void Controller_AI_MiC::init_paths()
{
  for (int p=0;p<4;p++)
	{
		paths[p].posx=-1;
		paths[p].posy=-1;
	}
}//init_paths

bool Controller_AI_MiC::same_positions(int x1, int y1, int x2, int y2)
{
  return (x1==x2 && y1==y2);
}//same_positions

int Controller_AI_MiC::get_ground_rating(int dir, int mapx, int mapy)
{
	int rating = 0;
  if (dir==DIR_LEFT && (mapx-1)>=0) rating=ground[mapx-1][mapy];
  if (dir==DIR_RIGHT && (mapx+1)<MAP_WIDTH) rating=ground[mapx+1][mapy];
  if (dir==DIR_UP && (mapy-1)>=0) rating=ground[mapx][mapy-1];
  if (dir==DIR_DOWN && (mapy+1)<MAP_HEIGHT) rating=ground[mapx][mapy+1];
	return rating;
}//get_ground_rating

bool Controller_AI_MiC::can_move_in_time(float countdown)
//returns false if bomber would be killed by an explosion or
//        true if bomber could move over two grounds in time=countdown
{
  if (countdown==(float)0.0) return false;
	if (countdown==(float)67.0) return false;
  if (countdown==(float)77.0) return true;
	float time_to_move=(80/(bomber->get_speed()));
  if (time_to_move<=countdown) return true;
  else return false;
}//can_move_in_time

bool Controller_AI_MiC::can_move_over_distance(int distance, float countdown)
{
  if (distance==0) return true;
  if (countdown==(float)0.0) return false;
	if (countdown==(float)67.0) return false;
  if (countdown==(float)77.0) return true;
  //int distance_in_pixel=distance*40+80;
  //float time_per_pixel=1/(bomber->get_speed());
  //float needed_time=(float)distance_in_pixel*time_per_pixel;
  if (((distance*40+70)/(bomber->get_speed()))<=countdown) return true;
  else return false;
}//can_move_over_distance

bool Controller_AI_MiC::is_left()
{
	return direction==DIR_LEFT && active;
}//is_left

bool Controller_AI_MiC::is_right()
{
	return direction==DIR_RIGHT && active;
}//is_right

bool Controller_AI_MiC::is_up()
{
	return direction==DIR_UP && active;
}//is_up

bool Controller_AI_MiC::is_down()
{
	return direction==DIR_DOWN && active;
}//is_down

bool Controller_AI_MiC::is_bomb()
{
	return bombing && active;
}//is_bomb

int Controller_AI_MiC::get_direction_rating(int dir, int mapx, int mapy, int **pground)
//--- returns "-100000"==DEADEND_RATING if deadend ahead, way rating ">=0" else
{
  int r;
  int extra_rating=-100;
  int sure_place_rating=50;
  int box_rating=0;
  bool deadend=true;
  int tmp;
  int x=bomber->get_x();
  int y=bomber->get_y();
  int bx=(x+20)/40;
  int by=(y+20)/40;
  int sure_x=-100;
  int sure_y=-100;
  if (dir==DIR_LEFT || dir==DIR_UP) r=-1; else r=1;
  if (dir==DIR_LEFT || dir==DIR_RIGHT) //horizontal
  {
    int mapx_=mapx+r;
    while (bomber->app->map->get_maptile( mapx_, mapy )->is_passable() &&
           !bomber->app->map->get_maptile( mapx_, mapy )->is_vanishing() &&
           bomber->app->map->get_maptile( mapx_, mapy )->bomb==NULL &&
           can_move_over_distance(abs(mapx-mapx_),explosions_timer[mapx_][mapy].stack[0].timer) &&
           !is_very_dangerous(mapx_,mapy))
    {
      if (pground[mapx_][mapy]>STANDARD_SURE_PLACE_RATING && !simulation_mode_on)
      {
        tmp=get_extra_rating(mapx_,mapy,bx,by);
     		if (tmp>extra_rating) extra_rating=tmp;
     		tmp=abs(mapx-mapx_);
				if (sure_place_rating>tmp)
				{
					sure_place_rating=tmp;
 	    		deadend=false;
       	  sure_x=mapx_;
					sure_y=mapy;
				}
       	dout("get_direction("<<dir<<")= mapx="<<mapx_<<" mapy="<<mapy<<" ===> OK"<<endl);
			}
			else dout("get_direction("<<dir<<")= mapx="<<mapx_<<" mapy="<<mapy<<" ===> DEADEND"<<endl);
      int mapy_up=mapy-1;
	    while (bomber->app->map->get_maptile( mapx_, mapy_up )->is_passable() &&
 	           !bomber->app->map->get_maptile( mapx_, mapy_up )->is_vanishing() &&
 	           bomber->app->map->get_maptile( mapx_, mapy_up )->bomb==NULL &&
						 !is_very_dangerous(mapx_,mapy_up) &&
						 can_move_over_distance(abs(mapx-mapx_)+abs(mapy-mapy_up),explosions_timer[mapx_][mapy_up].stack[0].timer))
 	    {
 	      if (pground[mapx_][mapy_up]>STANDARD_SURE_PLACE_RATING)
 	      {
          tmp=get_box_number(mapx_,mapy_up);
          if (tmp>box_rating) box_rating=tmp;
 	      	tmp=get_extra_rating(mapx_,mapy_up,bx,by);
  	      if (tmp>extra_rating) extra_rating=tmp;
          tmp=(abs(mapx-mapx_)+abs(mapy-mapy_up));
   	      if (sure_place_rating>tmp)
					{
						sure_place_rating=tmp;
    	    	deadend=false;
            sure_x=mapx_;
						sure_y=mapy_up;
					}
        }
        	int mapx_up_left=mapx_-1;
	    	  while (bomber->app->map->get_maptile( mapx_up_left, mapy_up )->is_passable() &&
 	               !bomber->app->map->get_maptile( mapx_up_left, mapy_up )->is_vanishing() &&
 	               bomber->app->map->get_maptile( mapx_up_left, mapy_up )->bomb==NULL &&
								 !is_very_dangerous(mapx_up_left,mapy_up) &&
								 can_move_over_distance(abs(mapx-mapx_)+abs(mapx_-mapx_up_left)+abs(mapy-mapy_up),explosions_timer[mapx_up_left][mapy_up].stack[0].timer))
 	     		{
 	     		  if (pground[mapx_up_left][mapy_up]>STANDARD_SURE_PLACE_RATING)
 	      		{
          		tmp=get_box_number(mapx_up_left,mapy_up);
          		if (tmp>box_rating) box_rating=tmp;
 	        		tmp=get_extra_rating(mapx_up_left,mapy_up,mapx_,mapy_up,bx,by);
 	        		if (tmp>extra_rating) extra_rating=tmp;
 				  		tmp=(abs(mapx-mapx_)+abs(mapx_-mapx_up_left)+abs(mapy-mapy_up)+abs(mapy_up-mapy_up));
				 			if (sure_place_rating>tmp)
							{
								sure_place_rating=tmp;
    	  		  	deadend=false;
            		sure_x=mapx_up_left;
								sure_y=mapy_up;
							}
          	}
  	    		mapx_up_left-=1;
    			}
          int mapx_up_right=mapx_+1;
	    	  while (bomber->app->map->get_maptile( mapx_up_right, mapy_up )->is_passable() &&
 	               !bomber->app->map->get_maptile( mapx_up_right, mapy_up )->is_vanishing() &&
 	               bomber->app->map->get_maptile( mapx_up_right, mapy_up )->bomb==NULL &&
								 !is_very_dangerous(mapx_up_right,mapy_up) &&
								 can_move_over_distance(abs(mapx-mapx_)+abs(mapx_-mapx_up_right)+abs(mapy-mapy_up),explosions_timer[mapx_up_right][mapy_up].stack[0].timer))
 	     		{
 	     		  if (pground[mapx_up_right][mapy_up]>STANDARD_SURE_PLACE_RATING)
 	      		{
          		tmp=get_box_number(mapx_up_right,mapy_up);
          		if (tmp>box_rating) box_rating=tmp;
 	        		tmp=get_extra_rating(mapx_up_right,mapy_up,mapx_,mapy_up,bx,by);
 	        		if (tmp>extra_rating) extra_rating=tmp;
 				  		tmp=(abs(mapx-mapx_)+abs(mapx_-mapx_up_right)+abs(mapy-mapy_up)+abs(mapy_up-mapy_up));
 			        if (sure_place_rating>tmp)
							{
								sure_place_rating=tmp;
    	  		  	deadend=false;
          		  sure_x=mapx_up_right;
								sure_y=mapy_up;
							}
          	}
  	    		mapx_up_right-=1;
    			}
   	    mapy_up-=1;
   	  }
   	  int mapy_down=mapy+1;
	    while (bomber->app->map->get_maptile( mapx_, mapy_down )->is_passable() &&
 	           !bomber->app->map->get_maptile( mapx_, mapy_down )->is_vanishing() &&
 	           bomber->app->map->get_maptile( mapx_, mapy_down )->bomb==NULL &&
						 !is_very_dangerous(mapx_,mapy_down) &&
						 can_move_over_distance(abs(mapx-mapx_)+abs(mapy-mapy_down),explosions_timer[mapx_][mapy_down].stack[0].timer))
 	    {
 	      if (pground[mapx_][mapy_down]>STANDARD_SURE_PLACE_RATING)
 	      {
          tmp=get_box_number(mapx_,mapy_down);
          if (tmp>box_rating) box_rating=tmp;
 	        tmp=get_extra_rating(mapx_,mapy_down,bx,by);
 	        if (tmp>extra_rating) extra_rating=tmp;
 				  tmp=(abs(mapx-mapx_)+abs(mapy-mapy_down));
 			    if (sure_place_rating>tmp)
					{
						sure_place_rating=tmp;
    	    	deadend=false;
            sure_x=mapx_;
						sure_y=mapy_down;
					}
        }
          int mapx_down_left=mapx_-1;
	    	  while (bomber->app->map->get_maptile( mapx_down_left, mapy_down )->is_passable() &&
 	               !bomber->app->map->get_maptile( mapx_down_left, mapy_down )->is_vanishing() &&
 	               bomber->app->map->get_maptile( mapx_down_left, mapy_down )->bomb==NULL &&
								 !is_very_dangerous(mapx_down_left,mapy_down) &&
								 can_move_over_distance(abs(mapx-mapx_)+abs(mapx_-mapx_down_left)+abs(mapy-mapy_down),explosions_timer[mapx_down_left][mapy_down].stack[0].timer))
 	     		{
 	     		  if (pground[mapx_down_left][mapy_down]>STANDARD_SURE_PLACE_RATING)
 	      		{
          		tmp=get_box_number(mapx_down_left,mapy_down);
          		if (tmp>box_rating) box_rating=tmp;
 	        		tmp=get_extra_rating(mapx_down_left,mapy_down,mapx_,mapy_down,bx,by);
 	        		if (tmp>extra_rating) extra_rating=tmp;
 				     	tmp=(abs(mapx-mapx_)+abs(mapx_-mapx_down_left)+abs(mapy-mapy_down)+abs(mapy_down-mapy_down));
				 			if (sure_place_rating>tmp)
							{
								sure_place_rating=tmp;
   			 	    	deadend=false;
     	 		      sure_x=mapx_down_left;
								sure_y=mapy_down;
							}
          	}
  	    		mapx_down_left-=1;
    			}
          int mapx_down_right=mapx_+1;
	    	  while (bomber->app->map->get_maptile( mapx_down_right, mapy_down )->is_passable() &&
 	               !bomber->app->map->get_maptile( mapx_down_right, mapy_down )->is_vanishing() &&
 	               bomber->app->map->get_maptile( mapx_down_right, mapy_down )->bomb==NULL &&
								 !is_very_dangerous(mapx_down_right,mapy_down) &&
								 can_move_over_distance(abs(mapx-mapx_)+abs(mapx_-mapx_down_right)+abs(mapy-mapy_down),explosions_timer[mapx_down_right][mapy_down].stack[0].timer))
 	     		{
 	     		  if (pground[mapx_down_right][mapy_down]>STANDARD_SURE_PLACE_RATING)
 	      		{
          		tmp=get_box_number(mapx_down_right,mapy_down);
          		if (tmp>box_rating) box_rating=tmp;
 	        		tmp=get_extra_rating(mapx_down_right,mapy_down,mapx_,mapy_down,bx,by);
 	        		if (tmp>extra_rating) extra_rating=tmp;
 				  	  tmp=(abs(mapx-mapx_)+abs(mapx_-mapx_down_right)+abs(mapy-mapy_down)+abs(mapy_down-mapy_down));
 			        if (sure_place_rating>tmp)
							{
								sure_place_rating=tmp;
   			 	    	deadend=false;
       		      sure_x=mapx_down_right;
								sure_y=mapy_down;
							}
						}
  	    		mapx_down_right-=1;
    			}
 	 	    mapy_down+=1;
    	}
      mapx_+=r;
    }
  }
  else //vertical
  {
    int mapy_=mapy+r;
    while (bomber->app->map->get_maptile( mapx, mapy_ )->is_passable() &&
           !bomber->app->map->get_maptile( mapx, mapy_ )->is_vanishing() &&
           bomber->app->map->get_maptile( mapx, mapy_ )->bomb==NULL &&
           can_move_over_distance(abs(mapy-mapy_),explosions_timer[mapx][mapy_].stack[0].timer) &&
					 !is_very_dangerous(mapx,mapy_))
    {
   	  if (pground[mapx][mapy_]>STANDARD_SURE_PLACE_RATING && !simulation_mode_on)
      {
        tmp=get_extra_rating(mapx,mapy_,bx,by);
     	  if (tmp>extra_rating) extra_rating=tmp;
       	tmp=abs(mapy-mapy_);
   			if (sure_place_rating>tmp)
				{
					sure_place_rating=tmp;
 	    		deadend=false;
       	  sure_x=mapx;
					sure_y=mapy_;
				}
       	dout("get_direction("<<dir<<")= mapx="<<mapx<<" mapy="<<mapy_<<" ===> OK"<<endl);
			}
			else dout("get_direction("<<dir<<")= mapx="<<mapx<<" mapy="<<mapy_<<" ===> DEADEND"<<endl);		
      int mapx_left=mapx-1;
      while (bomber->app->map->get_maptile( mapx_left, mapy_ )->is_passable() &&
             !bomber->app->map->get_maptile( mapx_left, mapy_ )->is_vanishing() &&
             bomber->app->map->get_maptile( mapx_left, mapy_ )->bomb==NULL &&
						 !is_very_dangerous(mapx_left,mapy_) &&
						 can_move_over_distance(abs(mapy-mapy_)+abs(mapx-mapx_left),explosions_timer[mapx_left][mapy_].stack[0].timer))
      {
        if (pground[mapx_left][mapy_]>STANDARD_SURE_PLACE_RATING)
        {
          tmp=get_box_number(mapx_left,mapy_);
          if (tmp>box_rating) box_rating=tmp;
          tmp=get_extra_rating(mapx_left,mapy_,bx,by);
          if (tmp>extra_rating) extra_rating=tmp;
					tmp=(abs(mapy-mapy_)+abs(mapx-mapx_left));
          if (sure_place_rating>tmp)
					{
						sure_place_rating=tmp;
    	    	deadend=false;
            sure_x=mapx_left;
						sure_y=mapy_;
					}
				}
        	int mapy_left_up=mapy_-1;
	    	  while (bomber->app->map->get_maptile( mapx_left, mapy_left_up )->is_passable() &&
 	               !bomber->app->map->get_maptile( mapx_left, mapy_left_up )->is_vanishing() &&
 	               bomber->app->map->get_maptile( mapx_left, mapy_left_up )->bomb==NULL &&
								 !is_very_dangerous(mapx_left,mapy_left_up) &&
								 can_move_over_distance(abs(mapy-mapy_)+abs(mapy_-mapy_left_up)+abs(mapx-mapx_left),explosions_timer[mapx_left][mapy_left_up].stack[0].timer))
 	     		{
 	     		  if (pground[mapx_left][mapy_left_up]>STANDARD_SURE_PLACE_RATING)
 	      		{
          		tmp=get_box_number(mapx_left,mapy_left_up);
          		if (tmp>box_rating) box_rating=tmp;
 	        		tmp=get_extra_rating(mapx_left,mapy_left_up,mapx_left,mapy_,bx,by);
 	        		if (tmp>extra_rating) extra_rating=tmp;
 				  		tmp=(abs(mapy-mapy_)+abs(mapy_-mapy_left_up)+abs(mapx-mapx_left)+abs(mapx_left-mapx_left));
 			        if (sure_place_rating>tmp)
							{
								sure_place_rating=tmp;
    			    	deadend=false;
       		      sure_x=mapx_left;
								sure_y=mapy_left_up;
							}
          	}
  	    		mapy_left_up-=1;
    			}
          int mapy_left_down=mapy_+1;
	    	  while (bomber->app->map->get_maptile( mapx_left, mapy_left_down )->is_passable() &&
 	               !bomber->app->map->get_maptile( mapx_left, mapy_left_down )->is_vanishing() &&
 	               bomber->app->map->get_maptile( mapx_left, mapy_left_down )->bomb==NULL &&
								 !is_very_dangerous(mapx_left,mapy_left_down) &&
								 can_move_over_distance(abs(mapy-mapy_)+abs(mapy_-mapy_left_down)+abs(mapx-mapx_left),explosions_timer[mapx_left][mapy_left_down].stack[0].timer))
 	     		{
 	     		  if (pground[mapx_left][mapy_left_down]>STANDARD_SURE_PLACE_RATING)
 	      		{
          		tmp=get_box_number(mapx_left,mapy_left_down);
          		if (tmp>box_rating) box_rating=tmp;
 	        		tmp=get_extra_rating(mapx_left,mapy_left_down,mapx_left,mapy_,bx,by);
 	        		if (tmp>extra_rating) extra_rating=tmp;
 				  		tmp=(abs(mapy-mapy_)+abs(mapy_-mapy_left_down)+abs(mapx-mapx_left)+abs(mapx_left-mapx_left));
							if (sure_place_rating>tmp)
							{
								sure_place_rating=tmp;
    			    	deadend=false;
       		      sure_x=mapx_left;
								sure_y=mapy_left_down;
							}
          	}
  	    		mapy_left_down+=1;
    			}
        mapx_left-=1;
      }
      int mapx_right=mapx+1;
      while (bomber->app->map->get_maptile( mapx_right, mapy_ )->is_passable() &&
             !bomber->app->map->get_maptile( mapx_right, mapy_ )->is_vanishing() &&
             bomber->app->map->get_maptile( mapx_right, mapy_ )->bomb==NULL &&
						 !is_very_dangerous(mapx_right,mapy_) &&
						 can_move_over_distance(abs(mapy-mapy_)+abs(mapx-mapx_right),explosions_timer[mapx_right][mapy_].stack[0].timer))
      {
        if (pground[mapx_right][mapy_]>STANDARD_SURE_PLACE_RATING)
        {
          tmp=get_box_number(mapx_right,mapy_);
          if (tmp>box_rating) box_rating=tmp;
          tmp=get_extra_rating(mapx_right,mapy_,bx,by);
          if (tmp>extra_rating) extra_rating=tmp;
					tmp=(abs(mapy-mapy_)+abs(mapx-mapx_right));
    			if (sure_place_rating>tmp)
					{
						sure_place_rating=tmp;
    	    	deadend=false;
            sure_x=mapx_right;
						sure_y=mapy_;
					}
				}
          int mapy_right_up=mapy_-1;
	    	  while (bomber->app->map->get_maptile( mapx_right, mapy_right_up )->is_passable() &&
 	               !bomber->app->map->get_maptile( mapx_right, mapy_right_up )->is_vanishing() &&
 	               bomber->app->map->get_maptile( mapx_right, mapy_right_up )->bomb==NULL &&
								 !is_very_dangerous(mapx_right,mapy_right_up) &&
								 can_move_over_distance(abs(mapy-mapy_)+abs(mapy_-mapy_right_up)+abs(mapx-mapx_right),explosions_timer[mapx_right][mapy_right_up].stack[0].timer))
 	     		{
 	     		  if (pground[mapx_right][mapy_right_up]>STANDARD_SURE_PLACE_RATING)
 	      		{
          		tmp=get_box_number(mapx_right,mapy_right_up);
          		if (tmp>box_rating) box_rating=tmp;
 	        		tmp=get_extra_rating(mapx_right,mapy_right_up,mapx_right,mapy_,bx,by);
 	        		if (tmp>extra_rating) extra_rating=tmp;
 				  		tmp=(abs(mapy-mapy_)+abs(mapy_-mapy_right_up)+abs(mapx-mapx_right)+abs(mapx_right-mapx_right));
 			      	if (sure_place_rating>tmp)
							{
								sure_place_rating=tmp;
    			    	deadend=false;
       		      sure_x=mapx_right;
								sure_y=mapy_right_up;
							}
          	}
  	    		mapy_right_up-=1;
    			}
          int mapy_right_down=mapy_+1;
	    	  while (bomber->app->map->get_maptile( mapx_right, mapy_right_down )->is_passable() &&
 	               !bomber->app->map->get_maptile( mapx_right, mapy_right_down )->is_vanishing() &&
 	               bomber->app->map->get_maptile( mapx_right, mapy_right_down )->bomb==NULL &&
								 !is_very_dangerous(mapx_right,mapy_right_down) &&
								 can_move_over_distance(abs(mapy-mapy_)+abs(mapy_-mapy_right_down)+abs(mapx-mapx_right),explosions_timer[mapx_right][mapy_right_down].stack[0].timer))
 	     		{
 	     		  if (pground[mapx_right][mapy_right_down]>STANDARD_SURE_PLACE_RATING)
 	      		{
          		tmp=get_box_number(mapx_right,mapy_right_down);
          		if (tmp>box_rating) box_rating=tmp;
 	        		tmp=get_extra_rating(mapx_right,mapy_right_down,mapx_right,mapy_,bx,by);
 	        		if (tmp>extra_rating) extra_rating=tmp;
 				  		tmp=(abs(mapy-mapy_)+abs(mapy_-mapy_right_down)+abs(mapx-mapx_right)+abs(mapx_right-mapx_right));
 			      	if (sure_place_rating>tmp)
							{
								sure_place_rating=tmp;
    	  		  	deadend=false;
            		sure_x=mapx_right;
								sure_y=mapy_right_down;
							}
          	}
  	    		mapy_right_down+=1;
    			}
        mapx_right+=1;
      }
      mapy_+=r;
    }
  }
  dout(dir<<" --- shortest path="<<sure_place_rating<<" && extra_rating="<<extra_rating<<" && box_rating="<<box_rating<<endl);
  dout("+++++++ sicherer platz: x="<<sure_x<<" y="<<sure_y<<endl);
  if (getting_paths)
	{
		paths[dir].posx=sure_x;
 		paths[dir].posy=sure_y;
	}
  if (deadend)
  {
		return DEADEND_RATING;
  }
  else
  {
		if (!flight_from_bomb) return (extra_rating+box_rating);
		else return ((100-sure_place_rating)*7+extra_rating+box_rating);
 }
}//get_direction_rating







