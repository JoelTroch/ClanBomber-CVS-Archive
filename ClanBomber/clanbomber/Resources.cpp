/***************************************************************************
                          FILENAME  -  description                              
                             -------------------                                         
    begin                : in 1999                                           
    copyright            : (C) 1999 by Andreas Hundt, Denis Oliver Kropp                         
    email                : clanbomber@fischlustig.de                                     
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   * 
 *                                                                         *
 ***************************************************************************/

#include <stdlib.h>

#include <ClanLib/Display/Font/font.h>
#include <ClanLib/Core/Resources/resource_manager.h>
#include <ClanLib/Core/System/error.h>
#include <ClanLib/Display/Display/surface.h>
#include <ClanLib/Sound/soundbuffer.h>

#include "Resources.h"

#ifdef WIN32
	#define PKGDATADIR
#endif

CL_ResourceManager*	Resources::res = NULL;

CL_Font*			Resources::fnt_big = NULL;
CL_Font*			Resources::fnt_small = NULL;

CL_Surface*		Resources::sur_titlescreen = NULL;

CL_Surface*		Resources::sur_fl_logo = NULL;
CL_SoundBuffer*	Resources::sfx_typewriter = NULL;
CL_SoundBuffer*	Resources::sfx_winlevel = NULL;

CL_Surface*		Resources::sur_ps_teams = NULL;
CL_Surface*		Resources::sur_ps_controls = NULL;
CL_Surface*		Resources::sur_ps_teamlamps = NULL;
CL_Surface*		Resources::sur_ps_background = NULL;

CL_Surface*		Resources::sur_ls_background = NULL;
CL_Surface*		Resources::sur_ls_not_available = NULL;

CL_Surface*		Resources::sur_gs_tools = NULL;
CL_SoundBuffer*	Resources::sfx_klatsch = NULL;
CL_Surface*		Resources::sur_gs_background = NULL;

CL_SoundBuffer*	Resources::sfx_forward = NULL;
CL_SoundBuffer*	Resources::sfx_rewind = NULL;
CL_SoundBuffer*	Resources::sfx_stop = NULL;

CL_Surface*		Resources::sur_horst = NULL;
CL_Surface*		Resources::sur_horst_evil = NULL;

CL_Surface*		Resources::sur_bombers[8] = { NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL };

CL_Surface*		Resources::sur_extras = NULL;
CL_SoundBuffer*	Resources::sfx_wow = NULL;
CL_SoundBuffer*	Resources::sfx_joint = NULL;
CL_SoundBuffer*	Resources::sfx_horny = NULL;
CL_SoundBuffer*	Resources::sfx_schnief = NULL;

CL_SoundBuffer*	Resources::sfx_whoosh = NULL;
CL_SoundBuffer*	Resources::sfx_break = NULL;
CL_SoundBuffer*	Resources::sfx_clear = NULL;
CL_SoundBuffer*	Resources::sfx_menu_back = NULL;

CL_Surface*		Resources::sur_observer = NULL;
CL_SoundBuffer*	Resources::sfx_hurry_up = NULL;
CL_SoundBuffer*	Resources::sfx_time_over = NULL;
CL_SoundBuffer*	Resources::sfx_crunch = NULL;

CL_SoundBuffer*	Resources::sfx_die = NULL;
CL_SoundBuffer*	Resources::sfx_explode = NULL;
CL_SoundBuffer*	Resources::sfx_putbomb = NULL;
CL_SoundBuffer*	Resources::sfx_deepfall = NULL;
CL_Surface*		Resources::sur_maptiles = NULL;
CL_Surface*		Resources::sur_maptile_addons = NULL;
CL_Surface*		Resources::sur_bombs = NULL;
CL_Surface*		Resources::sur_explosion = NULL;
CL_Surface*		Resources::sur_cb_logo_small = NULL;

CL_Surface*		Resources::sur_me_background = NULL;

CL_SoundBuffer*	Resources::sfx_corpse_explode = NULL;
CL_Surface*		Resources::sur_corpse_part = NULL;

CL_SoundBuffer*	Resources::sfx_splash[2] = { NULL, NULL};


Resources::Resources()
{
}

Resources::~Resources()
{
}

void Resources::init()
{
	try
	{
		res = new CL_ResourceManager(PKGDATADIR "clanbomber.dat", true);
	}
	catch (CL_Error err)
	{
	    	try
		{
			res = new CL_ResourceManager(
				"/usr/share/clanbomber/clanbomber.dat",
				true);
		}
		catch (CL_Error err)
		{
			res = new CL_ResourceManager(
				"/usr/local/share/clanbomber/clanbomber.dat",
				true);
		}
	}
}

void Resources::load_all()
{
	res->load_all();
}
	
CL_Font* Resources::Font_big()
{
	if (!fnt_big)
	{
		fnt_big = CL_Font::load("Font/big", res);
	}
	return fnt_big;
}

CL_Font* Resources::Font_small()
{
	if (!fnt_small)
	{
		fnt_small = CL_Font::load("Font/small", res);
	}
	return fnt_small;
}

CL_Surface* Resources::Titlescreen()
{
	if (!sur_titlescreen)
	{
		if (rand()%2)
		{
			sur_titlescreen = CL_Surface::load("Titlescreen/andi", res);
		}
		else
		{
			sur_titlescreen = CL_Surface::load("Titlescreen/dok", res);
		}
	}
	return sur_titlescreen;
}

CL_Surface* Resources::Intro_fl_logo()
{
	if (!sur_fl_logo)
	{
		sur_fl_logo = CL_Surface::load("Intro/fl_logo", res);
	}
	return sur_fl_logo;
}

CL_SoundBuffer* Resources::Intro_typewriter()
{
	if (!sfx_typewriter)
	{
		sfx_typewriter = CL_SoundBuffer::load("Intro/typewriter",res);
	}
	return sfx_typewriter;
}

CL_SoundBuffer* Resources::Intro_winlevel()
{
	if (!sfx_winlevel)
	{
		sfx_winlevel = CL_SoundBuffer::load("Intro/winlevel", res);
	}
	return sfx_winlevel;
}

CL_Surface* Resources::Playersetup_teams()
{
	if (!sur_ps_teams)
	{
		sur_ps_teams = CL_Surface::load("Playersetup/teams", res);
	}
	return sur_ps_teams;
}

CL_Surface* Resources::Playersetup_controls()
{
	if (!sur_ps_controls)
	{
		sur_ps_controls = CL_Surface::load("Playersetup/controls", res);
	}
	return sur_ps_controls;
}

CL_Surface* Resources::Playersetup_teamlamps()
{
	if (!sur_ps_teamlamps)
	{
		sur_ps_teamlamps = CL_Surface::load("Playersetup/teamlamps", res);
	}
	return sur_ps_teamlamps;
}

CL_Surface* Resources::Playersetup_background()
{
	if (!sur_ps_background)
	{
		sur_ps_background = CL_Surface::load("Playersetup/background", res);
	}
	return sur_ps_background;
}

CL_Surface* Resources::Mapselector_background()
{
	if (!sur_ls_background)
	{
		sur_ls_background = CL_Surface::load("Mapselector/background", res);
	}
	return sur_ls_background;
}

CL_Surface* Resources::Mapselector_not_available()
{
	if (!sur_ls_not_available)
	{
		sur_ls_not_available = CL_Surface::load("Mapselector/not_available", res);
	}
	return sur_ls_not_available;
}

CL_Surface* Resources::Gamestatus_tools()
{
	if (!sur_gs_tools)
	{
		sur_gs_tools = CL_Surface::load("Gamestatus/tools", res);
	}
	return sur_gs_tools;
}

CL_SoundBuffer* Resources::Gamestatus_klatsch()
{
	if (!sfx_klatsch)
	{
		sfx_klatsch = CL_SoundBuffer::load("Gamestatus/klatsch", res);
	}
	return sfx_klatsch;
}

CL_Surface* Resources::Gamestatus_background()
{
	if (!sur_gs_background)
	{
		sur_gs_background = CL_Surface::load("Gamestatus/background", res);
	}
	return sur_gs_background;
}

CL_SoundBuffer* Resources::Credits_forward()
{
	if (!sfx_forward)
	{
		sfx_forward = CL_SoundBuffer::load("Credits/forward", res);
	}
	return sfx_forward;
}

CL_SoundBuffer* Resources::Credits_rewind()
{
	if (!sfx_rewind)
	{
		sfx_rewind = CL_SoundBuffer::load("Credits/rewind", res);
	}
	return sfx_rewind;
}

CL_SoundBuffer* Resources::Credits_stop()
{
	if (!sfx_stop)
	{
		sfx_stop = CL_SoundBuffer::load("Credits/stop", res);
	}
	return sfx_stop;
}


CL_Surface* Resources::Credits_horst_evil()
{
	if (!sur_horst_evil)
	{
		sur_horst_evil = CL_Surface::load("Credits/horst_evil", res);
	}
	return sur_horst_evil;
}

CL_Surface* Resources::Bombers(int nr)
{
	if (nr>7 || nr<0) return NULL;
	if (!sur_bombers[nr])
	{
		sur_bombers[0] = CL_Surface::load("Bombers/snake", res);
		sur_bombers[1] = CL_Surface::load("Bombers/tux", res);
		sur_bombers[2] = CL_Surface::load("Bombers/spider", res);
		sur_bombers[3] = CL_Surface::load("Bombers/bsd", res);
		sur_bombers[4] = CL_Surface::load("Bombers/dull_red", res);
		sur_bombers[5] = CL_Surface::load("Bombers/dull_blue", res);
		sur_bombers[6] = CL_Surface::load("Bombers/dull_yellow", res);
		sur_bombers[7] = CL_Surface::load("Bombers/dull_green", res);
	}
	return sur_bombers[nr];
}

CL_Surface* Resources::Extras_extras()
{
	if (!sur_extras)
	{
		sur_extras = CL_Surface::load("Extras/extras", res);
	}
	return sur_extras;
}

CL_SoundBuffer* Resources::Extras_wow()
{
	if (!sfx_wow)
	{
		sfx_wow = CL_SoundBuffer::load("Extras/wow", res);
	}
	return sfx_wow;
}

CL_SoundBuffer* Resources::Extras_joint()
{
	if (!sfx_joint)
	{
		sfx_joint = CL_SoundBuffer::load("Extras/joint", res);
	}
	return sfx_joint;
}

CL_SoundBuffer* Resources::Extras_horny()
{
	if (!sfx_horny)
	{
		sfx_horny = CL_SoundBuffer::load("Extras/horny", res);
	}
	return sfx_horny;
}

CL_SoundBuffer* Resources::Extras_schnief()
{
	if (!sfx_schnief)
	{
		sfx_schnief = CL_SoundBuffer::load("Extras/schnief", res);
	}
	return sfx_schnief;
}

CL_SoundBuffer* Resources::Menu_whoosh()
{
	if (!sfx_whoosh)
	{
		sfx_whoosh = CL_SoundBuffer::load("Menu/whoosh", res);
	}
	return sfx_whoosh;
}

CL_SoundBuffer* Resources::Menu_break()
{
	if (!sfx_break)
	{
		sfx_break = CL_SoundBuffer::load("Menu/break", res);
	}
	return sfx_break;
}

CL_SoundBuffer* Resources::Menu_clear()
{
	if (!sfx_clear)
	{
		sfx_clear = CL_SoundBuffer::load("Menu/clear", res);
	}
	return sfx_clear;
}
	
CL_SoundBuffer* Resources::Menu_back()
{
	if (!sfx_menu_back)
	{
		sfx_menu_back = CL_SoundBuffer::load("Menu/back", res);
	}
	return sfx_menu_back;
}
	
CL_Surface* Resources::Observer_observer()
{
	if (!sur_observer)
	{
    		sur_observer = CL_Surface::load("Observer/observer", res);
	}
	return sur_observer;
}

CL_SoundBuffer* Resources::Observer_hurry_up()
{
	if (!sfx_hurry_up)
	{
		sfx_hurry_up = CL_SoundBuffer::load("Observer/hurry_up", res);
	}
	return sfx_hurry_up;
}

CL_SoundBuffer* Resources::Observer_time_over()
{
	if (!sfx_time_over)
	{
		sfx_time_over = CL_SoundBuffer::load("Observer/time_over", res);
	}
	return sfx_time_over;
}

CL_SoundBuffer* Resources::Observer_crunch()
{
	if (!sfx_crunch)
	{
		sfx_crunch = CL_SoundBuffer::load("Observer/crunch", res);
	}
	return sfx_crunch;
}

CL_SoundBuffer* Resources::Game_die()
{
	if (!sfx_die)
	{
		sfx_die = CL_SoundBuffer::load("Game/die", res);
	}
	return sfx_die;
}

CL_SoundBuffer* Resources::Game_explode()
{
	if (!sfx_explode)
	{
		sfx_explode = CL_SoundBuffer::load("Game/explode", res);
	}
	return sfx_explode;
}

CL_SoundBuffer* Resources::Game_putbomb()
{
	if (!sfx_putbomb)
	{
		sfx_putbomb = CL_SoundBuffer::load("Game/putbomb", res);
	}
	return sfx_putbomb;
}

CL_SoundBuffer* Resources::Game_deepfall()
{
	if (!sfx_deepfall)
	{
		sfx_deepfall = CL_SoundBuffer::load("Game/deepfall", res);
	}
	return sfx_deepfall;
}

CL_Surface* Resources::Game_maptiles()
{
	if (!sur_maptiles)
	{
		sur_maptiles = CL_Surface::load("Game/maptiles", res);
	}
	return sur_maptiles;
}

CL_Surface* Resources::Game_maptile_addons()
{
	if (!sur_maptile_addons)
	{
		sur_maptile_addons = CL_Surface::load("Game/maptile_addons", res);
	}
	return sur_maptile_addons;
}

CL_Surface* Resources::Game_bombs()
{
	if (!sur_bombs)
	{
		sur_bombs = CL_Surface::load("Game/bombs", res);
	}
	return sur_bombs;
}

CL_Surface* Resources::Game_explosion()
{
	if (!sur_explosion)
	{
		sur_explosion = CL_Surface::load("Game/explosion", res);
	}
	return sur_explosion;
}

CL_Surface* Resources::Game_cb_logo_small()
{
	if (!sur_cb_logo_small)
	{
		sur_cb_logo_small = CL_Surface::load("Game/cb_logo_small", res);
	}
	return sur_cb_logo_small;
}
	
CL_Surface* Resources::MapEditor_background()
{
	if (!sur_me_background)
	{
		sur_me_background = CL_Surface::load("MapEditor/background", res);
	}
	return sur_me_background;
}

CL_SoundBuffer* Resources::Corpse_explode()
{
	if (!sfx_corpse_explode)
	{
		sfx_corpse_explode = CL_SoundBuffer::load("Corpse/explode", res);
	}
	return sfx_corpse_explode;
}

CL_Surface* Resources::Corpse_part()
{
	if (!sur_corpse_part)
	{
		sur_corpse_part = CL_Surface::load("Corpse/part", res);
	}
	return sur_corpse_part;
}

CL_SoundBuffer* Resources::Splash(int nr)
{
	if (nr>1 || nr<0) return NULL;
	if (!sfx_splash[nr])
	{
		sfx_splash[0] = CL_SoundBuffer::load("Splash/spl_1a", res);
		sfx_splash[1] = CL_SoundBuffer::load("Splash/spl_2a", res);
	}
	return sfx_splash[nr];
}








