/***************************************************************************
                          ClanBomber.cpp  -  description
                             -------------------
    begin                : ?
    copyright            : (C) 1999 by Andreas Hundt, Denis Oliver Kropp
    email                : clanbomber@fischlustig.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
// $Id: ClanBomber.cpp,v 1.12 2004/02/16 18:32:27 der_fisch Exp $

#include <ClanLib/Core/System/system.h>
#include <ClanLib/Display/setupdisplay.h>
#include <ClanLib/Sound/setupsound.h>
#include <ClanLib/Core/System/setupcore.h>
#include <ClanLib/Core/System/error.h>
#include <ClanLib/Core/Resources/datafile_compiler.h>
#include <ClanLib/Sound/soundbuffer.h>
#include <ClanLib/Display/Input/input.h>
#include <ClanLib/Display/Input/inputbuffer.h>
#include <ClanLib/Display/Input/keyboard.h>
#include <ClanLib/Display/Input/key.h>
#include <ClanLib/Display/Display/display.h>
#include <ClanLib/Display/Display/surface.h>
#include <ClanLib/Core/IOData/inputsource.h>
#include <ClanLib/Display/Font/font.h>
#include <ClanLib/display.h>

#include "ClanBomber.h"

#include "Controller.h"
#include "Debug.h"
#include "Observer.h"
#include "Config.h"
#include "Menu.h"
#include "Timer.h"
#include "PlayerSetup.h"
#include "Map.h"
#include "Credits.h"
#include "MapEditor.h"

#include "Bomber.h"
#include "MapSelector.h"

#ifndef WIN32
	#include <config.h>
	#include <dirent.h>
	#include <sys/stat.h>
#endif

#ifdef MUSIC
	#include <ClanLib/mikmod.h>
#endif

#include <time.h>

ClanBomberApplication app;

CL_String ClanBomberApplication::map_path;
CL_String ClanBomberApplication::local_map_path;

char *ClanBomberApplication::get_title()
{
	return "ClanBomber 1.05";
}

void ClanBomberApplication::quit_app(int value)
{
	deinit_modules();
	exit(value);
}

int ClanBomberApplication::main(int argc, char** argv)
{

	try
	{
		init_modules();

		bool fullscreen = false;
#ifdef WIN32
		fullscreen = true;
#endif

		if (argc > 1) {
			if (strcmp (argv[1], "-datafile") == 0) {
				CL_DatafileCompiler::write("clanbomber.scr", "clanbomber.dat");
				quit_app(0);
			}
			else if (strcmp (argv[1], "-fullscreen") == 0) {
				fullscreen = true;
			}
			else if (strcmp (argv[1], "-window") == 0) {
				fullscreen = false;
			}
			else if (strcmp (argv[1], "-h") == 0 ||
					strcmp (argv[1], "-help") == 0 ||
					strcmp (argv[1], "--help") == 0 ||
					strcmp (argv[1], "-?") == 0 ||
					strcmp (argv[1], "/?") == 0 ||
					strcmp (argv[1], "/h") == 0) {
				cout << "usage: clanbomber [-fullscreen | -window]" << endl;
				quit_app(0);
			}
		}

		srand( (long)time(NULL) );

		CL_Display::set_videomode(800, 600, 16, fullscreen);

		CL_Display::clear_display();
		CL_Display::flip_display();
		CL_Display::clear_display();
		CL_Display::flip_display();
		CL_MouseCursor::hide();

		show_fps = false;
		key_F1 = false;

#ifndef WIN32
		map_path = "maps";
		if (!opendir(map_path))
		{
			map_path = PKGDATADIR "maps";
			if (!opendir(map_path))
			{
				map_path = "/usr/share/clanbomber/maps";
				if (!opendir(map_path))
				{
					map_path = "/usr/share/clanbomber/maps";
				}
			}
		}
		if (getenv("HOME"))
		{
			CL_String path;
			path = getenv("HOME");
			path += "/.clanbomber";
			if (!opendir(path))
			{
				mkdir( path, 0755 );
			}
			
			Config::set_filename( path + "/clanbomber.cfg" );
			
			path += "/maps";
			if (!opendir(path))
			{
				mkdir( path, 0755 );
			}
			local_map_path = path;
		}
#else
		map_path = "maps";
		local_map_path = "maps";
#endif
  
		Resources::init();
		Config::load();
     	
		run_intro();
     	
		CL_Display::clear_display();
		Resources::Font_big()->print_center( 400, 300, "Loading..." );
		CL_Display::flip_display();
		Resources::load_all();

     	wav = NULL;
     	map = NULL;
     	observer = NULL;
  	
     	menu = new Menu( CL_String("Main Menu"), this);
     	menu->add_item( CL_String("Local Game"), MENU_LOCALGAME );
     		menu->add_item( CL_String("Start"), LOCALGAME_START, MENU_LOCALGAME );
     		menu->add_item( CL_String("Player Setup"), MENU_PLAYER_SETUP, MENU_LOCALGAME);
     		menu->add_item( CL_String("Map Selection"), CONFIG_MAP_SEL, MENU_LOCALGAME );
     		menu->add_value( CL_String("Random Map Order"), CONFIG_RANDOM_MAP_ORDER, MENU_LOCALGAME, 0, 1, Config::get_random_map_order()  );
     		menu->add_value( CL_String("Points to win"), CONFIG_POINTS, MENU_LOCALGAME, 1, 10, Config::get_points_to_win() );
     		menu->add_value( CL_String("Round Time"), CONFIG_ROUND_TIME, MENU_LOCALGAME, 30, 300, Config::get_round_time() );
     	menu->add_item( CL_String("Options"), MENU_OPTIONS );
     		menu->add_item( CL_String("Start/Max Extras"), MENU_EXTRA_VALUES, MENU_OPTIONS );
     			menu->add_value( CL_String("Start Bombs"), CONFIG_START_BOMBS, MENU_EXTRA_VALUES, 1, 15, Config::get_start_bombs() );
     			menu->add_value( CL_String("Start Power"), CONFIG_START_POWER, MENU_EXTRA_VALUES, 1, 15, Config::get_start_power() );
     			menu->add_value( CL_String("Start Skateboards"), CONFIG_START_SKATES, MENU_EXTRA_VALUES, 0, 10, Config::get_start_skateboards() );
     			menu->add_value( CL_String("Start Kick"), CONFIG_START_KICK, MENU_EXTRA_VALUES, 0, 1, Config::get_start_kick() );
     			menu->add_value( CL_String("Start Glove"), CONFIG_START_GLOVE, MENU_EXTRA_VALUES, 0, 1, Config::get_start_glove() );
     			menu->add_value( CL_String("Max. Bombs"), CONFIG_MAX_BOMBS, MENU_EXTRA_VALUES, 1, 15, Config::get_max_bombs() );
     			menu->add_value( CL_String("Max. Power"), CONFIG_MAX_POWER, MENU_EXTRA_VALUES, 1, 15, Config::get_max_power() );
     			menu->add_value( CL_String("Max. Skateboards"), CONFIG_MAX_SKATES, MENU_EXTRA_VALUES, 0, 10, Config::get_max_skateboards() );
			menu->add_item( CL_String("Enable/Disable Extras"), MENU_EXTRA_ONOFF, MENU_OPTIONS );
				menu->add_value( CL_String("Bombs"), CONFIG_BOMBS, MENU_EXTRA_ONOFF, 0, 1, Config::get_bombs() );
				menu->add_value( CL_String("Power"), CONFIG_POWER, MENU_EXTRA_ONOFF, 0, 1, Config::get_power() );
				menu->add_value( CL_String("Skateboard"), CONFIG_SKATES, MENU_EXTRA_ONOFF, 0, 1, Config::get_skateboards() );
				menu->add_value( CL_String("Kick"), CONFIG_KICK, MENU_EXTRA_ONOFF, 0, 1, Config::get_kick() );
				menu->add_value( CL_String("Glove"), CONFIG_GLOVE, MENU_EXTRA_ONOFF, 0, 1, Config::get_glove() );
			menu->add_item( CL_String("Enable/Disable Diseases"), MENU_DISEASE_ONOFF, MENU_OPTIONS );
				menu->add_value( CL_String("Joint"), CONFIG_JOINT, MENU_DISEASE_ONOFF, 0, 1, Config::get_joint() );
				menu->add_value( CL_String("Viagra"), CONFIG_VIAGRA, MENU_DISEASE_ONOFF, 0, 1, Config::get_viagra() );
				menu->add_value( CL_String("Koks"), CONFIG_KOKS, MENU_DISEASE_ONOFF, 0, 1, Config::get_koks() );
			menu->add_item( CL_String("Bomb Timing and Speed"), MENU_TIMING, MENU_OPTIONS );
				menu->add_value( CL_String("Bomb Countdown (1/10 s)"), CONFIG_BOMB_COUNTDOWN, MENU_TIMING, 0, 50, Config::get_bomb_countdown() );
				menu->add_value( CL_String("Bomb Chain Reaction Delay (1/100 s)"), CONFIG_BOMB_DELAY, MENU_TIMING, 0, 50, Config::get_bomb_delay() );
				menu->add_value( CL_String("Moving Bombs Speed (pixels per second)"), CONFIG_BOMB_SPEED, MENU_TIMING, 10, 500, Config::get_bomb_speed() );
			menu->add_value( CL_String("Kidz Mode"), CONFIG_KIDS_MODE, MENU_OPTIONS, 0, 1, Config::get_kids_mode() );
			menu->add_value( CL_String("Corpse Parts"), CONFIG_CORPSE_PARTS, MENU_OPTIONS, 0, 100, Config::get_corpse_parts() );
			menu->add_value( CL_String("Shaky Explosions"), CONFIG_SHAKE, MENU_OPTIONS, 0, 1, Config::get_shaky_explosions() );
			menu->add_value( CL_String("Random Bomber Positions"), CONFIG_RANDOM_POSITIONS, MENU_OPTIONS, 0, 1, Config::get_random_positions() );
#ifdef MUSIC
			menu->add_value( CL_String("Music"), CONFIG_MUSIC, MENU_OPTIONS, 0, 1, Config::get_music() );
#endif

     	menu->add_item( CL_String("Map Editor"), MENU_MAP_EDITOR );
     	menu->add_item( CL_String("Show Credits"), MENU_CREDITS );
     	menu->add_item( CL_String("Help Screen"), MENU_HELP );
     	menu->add_item( CL_String("Quit Game"), MENU_EXIT );

     	menu->scroll_in();
     	
     	while (1)
     	{
     		int result = menu->execute();
     		
     		try
     		{
	     		MenuItem* item = menu->get_item_by_id( result );
    	 			switch (result)
	     		{
	    	 			case MENU_EXIT:
     					menu->scroll_out();
     					delete menu;
					quit_app(0);
	     			break;
    		 			case MENU_PLAYER_SETUP:
	     				menu->scroll_out();
    		 				{
     						PlayerSetup ps(this);
     						ps.exec();
	     				}
	     				menu->scroll_in();
	     			break;
    	 				case CONFIG_START_BOMBS:
     					Config::set_start_bombs( ((MenuItem_Value*)item)->get_value() );
     					Config::save();
     				break;
	     			case CONFIG_START_POWER:
    	 					Config::set_start_power( ((MenuItem_Value*)item)->get_value() );
     					Config::save();
     				break;
     				case CONFIG_START_SKATES:
	     				Config::set_start_skateboards( ((MenuItem_Value*)item)->get_value() );
    	 					Config::save();
     				break;
     				case CONFIG_START_KICK:
     					Config::set_start_kick( ((MenuItem_Value*)item)->get_value() );
     					Config::save();
	     			break;
					case CONFIG_START_GLOVE:
						Config::set_start_glove( ((MenuItem_Value*)item)->get_value() );
						Config::save();
		     			break;
	     			case CONFIG_BOMBS:
	     				Config::set_bombs( ((MenuItem_Value*)item)->get_value() );
	     				Config::save();
	     			break;
	     			case CONFIG_POWER:
	     				Config::set_power( ((MenuItem_Value*)item)->get_value() );
	     				Config::save();
	     			break;
	     			case CONFIG_SKATES:
	     				Config::set_skateboards( ((MenuItem_Value*)item)->get_value() );
	     				Config::save();
	     			break;
	     			case CONFIG_KICK:
	     				Config::set_kick( ((MenuItem_Value*)item)->get_value() );
	     				Config::save();
	     			break;
	     			case CONFIG_GLOVE:
	     				Config::set_glove( ((MenuItem_Value*)item)->get_value() );
	     				Config::save();
	     			break;
	     			case CONFIG_JOINT:
	     				Config::set_joint( ((MenuItem_Value*)item)->get_value() );
	     				Config::save();
	     			break;
	     			case CONFIG_VIAGRA:
	     				Config::set_viagra( ((MenuItem_Value*)item)->get_value() );
	     				Config::save();
	     			break;
	     			case CONFIG_KOKS:
	     				Config::set_koks( ((MenuItem_Value*)item)->get_value() );
	     				Config::save();
	     			break;
	     			case CONFIG_MAX_BOMBS:
	     				Config::set_max_bombs( ((MenuItem_Value*)item)->get_value() );
	     				Config::save();
	     			break;
	     			case CONFIG_MAX_POWER:
	     				Config::set_max_power( ((MenuItem_Value*)item)->get_value() );
	     				Config::save();
	     			break;
	     			case CONFIG_MAX_SKATES:
	     				Config::set_max_skateboards( ((MenuItem_Value*)item)->get_value() );
	     				Config::save();
	     			break;
	     			case MENU_CREDITS:
	     				menu->scroll_out();
	     				{
	     					Credits credits(this);
	     					credits.exec();
	     				}
	     				menu->scroll_in();
	     			break;
	     			case MENU_MAP_EDITOR:
	     				menu->scroll_out();
	     				{
	     					MapEditor me(this);
	     					me.exec();
	     				}
	     				menu->scroll_in();
	     			break;
	     			case MENU_HELP:
	     				menu->scroll_out();
	     				show_tutorial();				
	     				menu->scroll_in();
	     			break;
	     			case CONFIG_MAP_SEL:
	     				menu->scroll_out();
	     				{
	     					MapSelector ms(this);
	     					ms.exec();
	     				}
	     				menu->scroll_in();
	     			break;
	     			case CONFIG_POINTS:
	     				Config::set_points_to_win( ((MenuItem_Value*)item)->get_value() );
	     				Config::save();
	     			break;
	     			case CONFIG_ROUND_TIME:
	     				if (((MenuItem_Value*)item)->get_value()%30 == 1)
	     				{
	     					((MenuItem_Value*)item)->set_value( ((MenuItem_Value*)item)->get_value() + 29 );
	     				} else
	     				if (((MenuItem_Value*)item)->get_value()%30 == 29)
	     				{
	     					((MenuItem_Value*)item)->set_value( ((MenuItem_Value*)item)->get_value() - 29 );
	     				}
	     				Config::set_round_time( ((MenuItem_Value*)item)->get_value() );
	     				Config::save();
	     			break;			
	     			case CONFIG_MUSIC:
	     				Config::set_music( ((MenuItem_Value*)item)->get_value() );
	     				Config::save();
	     			break;
	     			case CONFIG_KIDS_MODE:
	     				Config::set_kids_mode( ((MenuItem_Value*)item)->get_value() );
	     				Config::save();
	     			break;
	     			case CONFIG_CORPSE_PARTS:
	     				Config::set_corpse_parts( ((MenuItem_Value*)item)->get_value() );
	     				Config::save();
	     			break;
				case CONFIG_SHAKE:
			  		Config::set_shaky_explosions( ((MenuItem_Value*)item)->get_value() );
						Config::save();
				break;
				case CONFIG_BOMB_COUNTDOWN:
			  		Config::set_bomb_countdown( ((MenuItem_Value*)item)->get_value() );
						Config::save();
				break;
				case CONFIG_BOMB_DELAY:
			  		Config::set_bomb_delay( ((MenuItem_Value*)item)->get_value() );
						Config::save();
				break;
				case CONFIG_BOMB_SPEED:
					if (((MenuItem_Value*)item)->get_value()%10 == 1)
					{
						((MenuItem_Value*)item)->set_value( ((MenuItem_Value*)item)->get_value() + 9 );
					} else
					if (((MenuItem_Value*)item)->get_value()%10 == 9)
					{
						((MenuItem_Value*)item)->set_value( ((MenuItem_Value*)item)->get_value() - 9 );
					}
	     		
			  		Config::set_bomb_speed( ((MenuItem_Value*)item)->get_value() );
						Config::save();
				break;
				case CONFIG_RANDOM_POSITIONS:
			  		Config::set_random_positions( ((MenuItem_Value*)item)->get_value() );
						Config::save();
				break;
				case CONFIG_RANDOM_MAP_ORDER:
			  		Config::set_random_map_order( ((MenuItem_Value*)item)->get_value() );
						Config::save();
				break;

	     			case LOCALGAME_START:
	     				menu->scroll_out();
	     				if (Config::get_number_of_opponents() > 1)
	     				{
	     					try
	     					{
		    						init_game();
	    							run_game();
	    						}
	    						catch (CL_Error err) // mommy says: always clean up.
	    						{
	    							deinit_game();
								throw err;
	    						}
    							deinit_game();
		     			} else
		     			{
							throw CL_Error("Not enough opponents!");
		     			}
	     				menu->scroll_in();
	     			break;
	     			default:
					std::cout << result << std::endl;
	     			break;
	     		}
	     	}
	     	catch (CL_Error err)
	     	{
	     		// damn, something went wrong. Could be everything from missing
	     		// resources to network game errors.
	     		// try to write the error message in a menu, and then return to
	     		// the main menu.
	     		
	     		Menu menu_err( "Shit", this);
	     		menu_err.add_item(err.message, 0);
	     		menu_err.scroll_in();
				menu_err.execute();
				menu_err.scroll_out();
			}
		}

		quit_app(1); // fuck shouldn't reach this
     }
     catch (CL_Error err)
     {
		std::cout << std::endl << "Exception caught from ClanLib:" << std::endl;
		std::cout << err.message << std::endl;
		quit_app(255);
     }
	return 0;
}

void ClanBomberApplication::run_game()
{
	Timer::reset();
	
	CL_Display::clear_display();
	CL_Display::flip_display();
	CL_Display::clear_display();
	CL_Display::flip_display();
	CL_Display::clear_display();
	CL_Display::flip_display();
	
	bool pause_game = false;
	bool key_P = false;

	while (CL_Keyboard::get_keycode(CL_KEY_ESCAPE) == 0)
	{
		Timer::time_elapsed(true);

		if (CL_Keyboard::get_keycode(CL_KEY_P) && !key_P) {
			pause_game = !pause_game;
			if (pause_game) {
				Resources::Font_big()->print_center(400, 300, "- PAUSED -");
			}
			CL_Display::flip_display();
		}
		key_P = CL_Keyboard::get_keycode(CL_KEY_P);
		if (pause_game) {
			CL_System::keep_alive();
			continue;
		}

		if (observer != NULL)
		{
			observer->act();
			
			if (observer->end_of_game_requested())
			{
				break;
			}
		}

		delete_some();
		act_all();
		show_all();
		
		CL_Display::flip_display();
		
		CL_System::keep_alive();

		if (CL_Keyboard::get_keycode(CL_KEY_F1) && !key_F1)
		{
			show_fps = !show_fps;
		}
		key_F1 = CL_Keyboard::get_keycode(CL_KEY_F1);
		frame_count++;
		frame_time += Timer::time_elapsed();
		if (frame_time > 2)
		{
			fps = (int)(frame_count/frame_time);
			frame_time = 0;
			frame_count = 0;
		}		
	}
	while (CL_Keyboard::get_keycode(CL_KEY_ESCAPE))
	{
		CL_System::keep_alive();
	}
}

void ClanBomberApplication::act_all()
{
	// Map acts first
	map->act();

	// Let them do their stuff
	CL_Iterator<GameObject> object_counter(objects);
	while (object_counter.next() != NULL)
	{
			object_counter()->act();
	}
	CL_Iterator<Bomber> bomber_object_counter(bomber_objects);
	while (bomber_object_counter.next() != NULL)
	{
			bomber_object_counter()->act();
	}
}

void ClanBomberApplication::delete_some()
{
	// Check if we should delete some
	CL_Iterator<GameObject> object_counter(objects);
	while (object_counter.next() != NULL)
	{
		if (object_counter()->delete_me)
		{
			delete object_counter();
			object_counter.remove();
		}
	}
	CL_Iterator<Bomber> bomber_object_counter(bomber_objects);
	while (bomber_object_counter.next() != NULL)
	{
		if (bomber_object_counter()->delete_me)
		{
			delete bomber_object_counter();
			bomber_object_counter.remove();
		}
	}
}

void ClanBomberApplication::show_all()
{
	int max_draw_list_items = objects.get_num_items()+bomber_objects.get_num_items();
#ifndef WIN32
	GameObject* draw_list[max_draw_list_items];
#else	
	GameObject** draw_list = new GameObject*[max_draw_list_items];
#endif

	int n = 0;
	int i;

	CL_Display::fill_rect( 0, 0, 800, 40, 0,0,0,1 );	// clear top
	if (show_fps)
	{
		Resources::Font_big()->print_center(535,4,CL_String(fps) + " fps");
	}

	CL_Display::set_clip_rect( CL_ClipRect(60, 0, 60+MAP_WIDTH*40, 40+MAP_HEIGHT*40) );

	CL_Iterator<GameObject> object_counter(objects);
	while (object_counter.next() != NULL)
	{
		draw_list[n] = object_counter();
		n++;
	}
	CL_Iterator<Bomber> bomber_object_counter(bomber_objects);
	while (bomber_object_counter.next() != NULL)
	{
		draw_list[n] = bomber_object_counter();
		n++;
	}
	
	bool sort = true;
	GameObject* obj;
	while(sort)
	{
		sort = false;
		for( i=0; i<n-1; i++ )
		{
			if (draw_list[i]->get_z() > draw_list[i+1]->get_z())
			{
				obj = draw_list[i];
				draw_list[i] = draw_list[i+1];
				draw_list[i+1] = obj;
				sort = true;
			}
		}
	}

	map->refresh_holes();
	bool drawn_map = false;
	
	for( i=0; i<n; i++ )
	{
		if (draw_list[i]->get_z()>=Z_GROUND && drawn_map == false)
		{
			map->show();
			drawn_map= true;
		}
		draw_list[i]->show();
	}
	if (!drawn_map)
	{
		map->show();
	}

	if (observer->get_round_time() > 0)
	{
		CL_Display::set_clip_rect( CL_ClipRect(0, 0, 800, 600) );
		observer->show();
	} else
	{
		observer->show();
		CL_Display::set_clip_rect( CL_ClipRect(0, 0, 800, 600) );
	}
#ifdef WIN32
	delete draw_list;
#endif
}

void ClanBomberApplication::init_game()
{
	frame_count = 0;
	frame_time = 0;
	fps = 0;
#ifdef MUSIC	
	if (!wav && Config::get_music())
	{
		try
		{
			wav = CL_Streamed_MikModSample::create(PKGDATADIR "bud.mod", NULL, true);
			ses = new CL_SoundBuffer_Session( wav->prepare() );
			ses->set_volume(1.2f);
		}
		catch (CL_Error err)
		{
			try
			{
				wav = CL_Streamed_MikModSample::create("/usr/share/clanbomber/bud.mod", NULL, true);
				ses = new CL_SoundBuffer_Session( wav->prepare() );
				ses->set_volume(1.2f);
			}
			catch (CL_Error err)
			{
				wav = CL_Streamed_MikModSample::create("/usr/local/share/clanbomber/bud.mod", NULL, true);
				ses = new CL_SoundBuffer_Session( wav->prepare() );
				ses->set_volume(1.2f);
			}
		}
	}
	
	if (Config::get_music() )
	{
		ses->play();
	}
#endif
	
	// init map
	map = new Map(this);
	
	if (!map->any_valid_map())
	{
		throw CL_Error("No valid maps selected!");
	}
	
	if (Config::get_random_map_order())
	{
		map->load_random_valid();
	}else
	{
		if (Config::get_start_map() > map->get_map_count()-1)
		{
			Config::set_start_map( map->get_map_count() -1 );
		}
		map->load_next_valid( Config::get_start_map() );
  }
	// init GameObjects
	if (Config::get_random_positions())
	{
		map->randomize_bomber_positions();
	}
	CL_Vector pos;
	int j = 0;
	for (int i=0; i< 8 ; i++)
	{
		if (Config::bomber[i].is_enabled())
		{
			pos = map->get_bomber_pos(j++);
			new Bomber( int(pos.x*40), int(pos.y*40), (Bomber::COLOR)(Config::bomber[i].get_skin()), Controller::create((Controller::CONTROLLER_TYPE)Config::bomber[i].get_controller()), Config::bomber[i].get_name(), Config::bomber[i].get_team(),i, this );
			bomber_objects.get_last()->set_pos( 350, 270 );
			bomber_objects.get_last()->fly_to( int(pos.x*40), int(pos.y*40), 300 );
			bomber_objects.get_last()->controller->deactivate();
		}		
	}
     // this is for removing teams which only one player is in
	int team_count[] = { 0,0,0,0};
	CL_Iterator<Bomber> bomber_object_counter(bomber_objects);
	for (int team = 0;team<4;team++)
	{
		while (bomber_object_counter.next() != NULL)
		{
			if (bomber_object_counter()->get_team()-1 == team)
			{
				team_count[team]++;
			}
		}		
	}
	while (bomber_object_counter.next() != NULL)
	{
		if (bomber_object_counter()->get_team() != 0)
		{
			if (team_count[bomber_object_counter()->get_team()-1] == 1)
			{
				bomber_object_counter()->set_team (0);
			}
		}
	}

// look whether we have to enable maptile_highlighting
	Config::set_highlight_maptiles(false);
	for (j=0; j<8; j++)
	{
		if (Config::bomber[j].is_enabled() && Config::bomber[j].get_highlight_maptile())
		{
			Config::set_highlight_maptiles(true);
			break;
		}
	}

#ifdef WITH_DEBUG	
	new Debug( 0, 0, this );
#endif

	observer = new Observer( 0, 0, this );
}

void ClanBomberApplication::deinit_game()
{
	// delete all GameObjects
	CL_Iterator<GameObject> object_counter(objects);
	while (object_counter.next() != NULL)
	{
		delete object_counter();
	}
	objects.clear();
	
	CL_Iterator<Bomber> bomber_object_counter(bomber_objects);
	while (bomber_object_counter.next() != NULL)
	{
		delete bomber_object_counter();
	}
	bomber_objects.clear();
	
	if (map)
	{
		delete map;
		map = NULL;
	}
	if (observer)
	{
		delete observer;
		observer = NULL;
	}
	
#ifdef MUSIC
	if ( ses && Config::get_music() )
	{
		ses->stop();
		ses->set_position(0);
	}
#endif
}

void ClanBomberApplication::run_intro()
{
	Timer::reset();
	float alpha = 1;
	
	Resources::Intro_winlevel()->play();
	while(1)
	{
		CL_Display::clear_display();
		CL_System::keep_alive();
		if (CL_Keyboard::get_keycode(CL_KEY_ESCAPE))
		{
			Resources::Menu_back()->play();
			return;
		}
		if (alpha > 0)
		{
			Resources::Intro_fl_logo()->put_screen( 100, 250, 0 );
			CL_Display::fill_rect( 100, 250, 700, 356, 0, 0, 0, alpha );
			CL_Display::flip_display();
	
			alpha -= Timer::time_elapsed(true) / (alpha*3);
		}
		else
		{
			Resources::Intro_fl_logo()->put_screen( 100, 250, 0 );
			CL_Display::flip_display();
			break;
		}
	}
	CL_System::sleep(500);
		
	CL_String *domi_str = new CL_String("A WORLD  DOMINATION PROJECT");
	for (int domispell=0;domispell<=domi_str->get_length();domispell++)
	{
		CL_Display::clear_display();
		CL_System::keep_alive();
		if (CL_Keyboard::get_keycode(CL_KEY_ESCAPE))
		{
			Resources::Menu_back()->play();
			return;
		}
		if (domi_str->mid(domispell,1) != " ")
		{
			Resources::Intro_typewriter()->play();
		}
		Resources::Intro_fl_logo()->put_screen( 100, 250, 0 );
		Resources::Font_small()->print_left( 230, 360, domi_str->mid(0,domispell) );
		CL_Display::flip_display();
		CL_System::sleep(rand()%100 + 80);
	}
	CL_System::sleep(1500);
	
	
	// Scroll out
	Resources::Menu_back()->play();
	Timer::reset();
	float scroll = 100;
	while(scroll<800)
	{
		CL_Display::clear_display();
		CL_System::keep_alive();
		if (CL_Keyboard::get_keycode(CL_KEY_ESCAPE))
		{
			return;
		}
		Resources::Intro_fl_logo()->put_screen( (int)scroll, 250,0);
		Resources::Font_small()->print_left( 230, 360, *domi_str );
		
		CL_Display::flip_display();
		
		scroll += Timer::time_elapsed(true)*1100.0f;
	}
}

void ClanBomberApplication::show_tutorial()
{
	CL_InputBuffer key_buffer( CL_Input::keyboards[0] );

	int y = 25;
	Resources::Titlescreen()->put_screen(0,0);
	CL_Display::clear_display (0.0f,0.0f,0.0f,0.5f);
		
	Resources::Font_big()->print_center(400,y, "ClanBomber Extras");
			
	y+=80;	
	Resources::Extras_extras()->put_screen( 15,y-5,0);
	Resources::Font_big()->print_left(70,y, "Bomb:");
	Resources::Font_big()->print_left(250,y, "You can place an additional bomb");

	y+=50;
	Resources::Extras_extras()->put_screen( 15,y-5,1);
	Resources::Font_big()->print_left(70,y, "Power:");
	Resources::Font_big()->print_left(250,y, "Explosions grow one field in each direction");

	y+=50;
	Resources::Extras_extras()->put_screen( 15,y-5,2);
	Resources::Font_big()->print_left(70,y, "Skateboard:");
	Resources::Font_big()->print_left(250,y, "Lets you move faster");

	y+=50;
	Resources::Extras_extras()->put_screen( 15,y-5,3);
	Resources::Font_big()->print_left(70,y, "Kick:");
	Resources::Font_big()->print_left(250,y, "Kick bombs if you walk against one");
	
	y+=50;
	Resources::Extras_extras()->put_screen( 15,y-5,4);
	Resources::Font_big()->print_left(70,y, "Throw:");
	Resources::Font_big()->print_left(250,y, "Throw Bombs if you press the button twice");
	Resources::Font_big()->print_left(250,y+50, "without moving");
	
	y=520;
	Resources::Font_big()->print_center(400,y, "Press any key");
	CL_Display::flip_display();		
	
	while (CL_Keyboard::get_keycode(CL_KEY_ENTER))
	{
		CL_System::keep_alive();
	}

	key_buffer.clear();
	
	while (key_buffer.get_key().state != CL_Key::Released)
	{
		CL_System::sleep( 50 );
		CL_System::keep_alive();
	}

	y = 25;
	Resources::Titlescreen()->put_screen(0,0);
	CL_Display::fill_rect (0,0,799,599, 0.0f,0.0f,0.0f,0.5f);
		
	Resources::Font_big()->print_center(400,y, "ClanBomber Drugs");
			
	y+=80;	
	Resources::Extras_extras()->put_screen( 15,y-5,5);
	Resources::Font_big()->print_left(70,y, "Joint:");
	Resources::Font_big()->print_left(250,y, "Controller will be reversed");

	y+=50;
	Resources::Extras_extras()->put_screen( 15,y-5,6);
	Resources::Font_big()->print_left(70,y, "Viagra:");
	Resources::Font_big()->print_left(250,y, "Autofire, this can be very dangerous!");

	y+=50;
	Resources::Extras_extras()->put_screen( 15,y-5,7);
	Resources::Font_big()->print_left(70,y, "Cocaine:");
	Resources::Font_big()->print_left(250,y, "Lets you move real fast!! (too fast)");

	y=520;
	Resources::Font_big()->print_center(400,y, "Press any key");
	CL_Display::flip_display();		

	key_buffer.clear();
	
	while (key_buffer.get_key().state != CL_Key::Released)
	{
		CL_System::sleep( 50 );
		CL_System::keep_alive();
	}
	key_buffer.clear();
}


CL_String read_line( CL_InputSource* in )
{
	CL_String ret;
	static char c[2] = { 0, 0 };
	
	do
	{
		c[0] = in->read_char8();
		switch( c[0] )
		{
			case 13:
				c[0] = in->read_char8();
				break;
			case 10:
				break;
			default:
				ret += c;
				break;
		}
	} while( c[0] != 10 );
	
	return ret;
}

CL_String ClanBomberApplication::get_map_path()
{
	return map_path;
}

CL_String ClanBomberApplication::get_local_map_path()
{
	return local_map_path;
}

int enter_string( CL_InputBuffer *key_buffer, CL_String &new_string )
{
	while (key_buffer->keys_left())
	{
		if (key_buffer->peek_key().state != CL_Key::Pressed)
		{
			key_buffer->get_key();
			continue;
		}
		CL_Key key = key_buffer->get_key();
		
		switch (key.id)
		{
			case CL_KEY_ENTER:
				return 1;
			case CL_KEY_ESCAPE:
				return -1;
			case CL_KEY_BACKSPACE:
				new_string = new_string.mid( 0, new_string.get_length()-1 );
				Resources::Intro_typewriter()->play();
			break;
			case CL_KEY_SPACE:
				new_string += " ";
				Resources::Intro_typewriter()->play();
			break;
			case CL_KEY_A:
				new_string += SHIFT ? "A" : "a";
				Resources::Intro_typewriter()->play();
			break;
			case CL_KEY_B:
				new_string += SHIFT ? "B" : "b";
				Resources::Intro_typewriter()->play();
			break;
			case CL_KEY_C:
				new_string += SHIFT ? "C" : "c";
				Resources::Intro_typewriter()->play();
			break;
			case CL_KEY_D:
				new_string += SHIFT ? "D" : "d";
				Resources::Intro_typewriter()->play();
			break;
			case CL_KEY_E:
				new_string += SHIFT ? "E" : "e";
				Resources::Intro_typewriter()->play();
			break;
			case CL_KEY_F:
				new_string += SHIFT ? "F" : "f";
				Resources::Intro_typewriter()->play();
			break;
			case CL_KEY_G:
				new_string += SHIFT ? "G" : "g";
				Resources::Intro_typewriter()->play();
			break;
			case CL_KEY_H:
				new_string += SHIFT ? "H" : "h";
				Resources::Intro_typewriter()->play();
			break;
			case CL_KEY_I:
				new_string += SHIFT ? "I" : "i";
				Resources::Intro_typewriter()->play();
			break;
			case CL_KEY_J:
				new_string += SHIFT ? "J" : "j";
				Resources::Intro_typewriter()->play();
			break;
			case CL_KEY_K:
				new_string += SHIFT ? "K" : "k";
				Resources::Intro_typewriter()->play();
			break;
			case CL_KEY_L:
				new_string += SHIFT ? "L" : "l";
				Resources::Intro_typewriter()->play();
			break;
			case CL_KEY_M:
				new_string += SHIFT ? "M" : "m";
				Resources::Intro_typewriter()->play();
			break;
			case CL_KEY_N:
				new_string += SHIFT ? "N" : "n";
				Resources::Intro_typewriter()->play();
			break;
			case CL_KEY_O:
				new_string += SHIFT ? "O" : "o";
				Resources::Intro_typewriter()->play();
			break;
			case CL_KEY_P:
				new_string += SHIFT ? "P" : "p";
				Resources::Intro_typewriter()->play();
			break;
			case CL_KEY_Q:
				new_string += SHIFT ? "Q" : "q";
				Resources::Intro_typewriter()->play();
			break;
			case CL_KEY_R:
				new_string += SHIFT ? "R" : "r";
				Resources::Intro_typewriter()->play();
			break;
			case CL_KEY_S:
				new_string += SHIFT ? "S" : "s";
				Resources::Intro_typewriter()->play();
			break;
			case CL_KEY_T:
				new_string += SHIFT ? "T" : "t";
				Resources::Intro_typewriter()->play();
			break;
			case CL_KEY_U:
				new_string += SHIFT ? "U" : "u";
				Resources::Intro_typewriter()->play();
			break;
			case CL_KEY_V:
				new_string += SHIFT ? "V" : "v";
				Resources::Intro_typewriter()->play();
			break;
			case CL_KEY_W:
				new_string += SHIFT ? "W" : "w";
				Resources::Intro_typewriter()->play();
			break;
			case CL_KEY_X:
				new_string += SHIFT ? "X" : "x";
				Resources::Intro_typewriter()->play();
			break;
			case CL_KEY_Y:
				new_string += SHIFT ? "Y" : "y";
				Resources::Intro_typewriter()->play();
			break;
			case CL_KEY_Z:
				new_string += SHIFT ? "Z" : "z";
				Resources::Intro_typewriter()->play();
			break;
		}
	}
	return 0;
}
