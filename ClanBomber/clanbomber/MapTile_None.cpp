/***************************************************************************
                          FILENAME  -  description                              
                             -------------------                                         
    begin                : in 1999                                           
    copyright            : (C) 1999 by Andreas Hundt, Denis Oliver Kropp                         
    email                : clanbomber@fischlustig.de                                     
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   * 
 *                                                                         *
 ***************************************************************************/

#include "ClanBomber.h"
#include "MapTile_None.h"
#include "GameObject.h"

#include <ClanLib/Display/Display/display.h>

MapTile_None::MapTile_None(int _x, int _y, ClanBomberApplication* _app) : MapTile(_x , _y, _app)
{
	passable = false;
	blocking = false;
}


MapTile_None::~MapTile_None()
{
}

void MapTile_None::draw(int addx, int addy)
{
	CL_Display::fill_rect( x+60+addx, y+40+addy,  x+100+addx, y+80+addy, 0, 0, 0, 1);
}

void MapTile_None::draw_tiny( int, int, float )
{
}

void MapTile_None::act()
{
	MapTile::act();

	CL_Iterator<GameObject> object_counter(objects);
	while (object_counter.next() != NULL)
	{
		object_counter()->fall();
	}
}

void MapTile_None::vanish()
{
}



