/***************************************************************************
                          Controller.cpp  -  description
                             -------------------
    begin                : ?
    copyright            : (C) 1999 by Andreas Hundt, Denis Oliver Kropp
    email                : clanbomber@fischlustig.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
// $Id: Controller_Joystick.cpp,v 1.1 2001/08/08 21:46:14 der_fisch Exp $

#include <ClanLib/Core/System/cl_assert.h>
#include <ClanLib/Display/Input/input.h>
#include <ClanLib/Display/Input/inputdevice.h>
#include <ClanLib/Display/Input/inputaxis.h>
#include <ClanLib/Display/Input/inputbutton.h>

#include "ClanBomber.h"
#include "Controller_Joystick.h"

Controller_Joystick::Controller_Joystick(int joystick_nr) : Controller()
{
//	cl_assert( joystick_nr < CL_Input::joysticks.get_num_items() );
	joystick = CL_Input::joysticks[joystick_nr];
	
	axes.add( joystick->get_axis(0) );
	axes.add( joystick->get_axis(1) );
	
	buttons.add( joystick->get_button(0) );
}

void Controller_Joystick::update()
{
	if (active)
	{

		if (buttons[0]->is_pressed() && !bomb_button_down)
		{
			put_bomb = true;
		} else
		{
			put_bomb = false;
		}
		bomb_button_down = buttons[0]->is_pressed();
	
		bool new_left  = (axes[0]->get_pos() < -0.2f);
		bool new_right = (axes[0]->get_pos() >  0.2f);
		bool new_up    = (axes[1]->get_pos() < -0.2f);
		bool new_down  = (axes[1]->get_pos() >  0.2f);	
		
		if (new_left || new_right || new_up || new_down)
		{
			dir_pressed = true;
		
			if (new_left && new_up)
			{
				if (down || up)
				{
					new_left = false;
				}
					if (right || left)
				{
					new_up = false;
				}
			}else
			if (new_left && new_down)
			{
				if (down || up)
				{
				new_left = false;
				}
				if (right || left)
				{
					new_down=false;
				}
			
			}else
			if (new_right && new_up)
			{
				if (down || up)
				{
					new_right = false;
				}
				if (right || left)
				{
					new_up = false;
				}
			
			}else
			if (new_right && new_down)
			{
				if (down || up)
				{
					new_right = false;
				}
				if (right || left)
				{
					new_down = false;
				}
										
			}
			if (reverse)
			{
				left = new_right;
				right = new_left;
				up = new_down;
				down = new_up;		
			}
			else
			{
				left = new_left;
				right = new_right;
				up = new_up;
				down = new_down;
			}
		}
		else
		{
			dir_pressed = false;
		}
	}		
	else
	{
		left = right = up = down = false;
	}
}

void Controller_Joystick::reset()
{
	put_bomb = false;
	bomb_button_down = true;
	reverse = false;
}

bool Controller_Joystick::is_left()
{
	return left && dir_pressed;
}

bool Controller_Joystick::is_right()
{
	return right && dir_pressed;
}

bool Controller_Joystick::is_up()
{
	return up && dir_pressed;
}

bool Controller_Joystick::is_down()
{
	return down && dir_pressed;
}

bool Controller_Joystick::is_bomb()
{
	switch (bomb_mode)
	{
		case NEVER:
			return false;
		case ALWAYS:
			return true;
		default:
		break;
	}
	return put_bomb && active;
}













