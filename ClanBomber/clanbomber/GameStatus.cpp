/***************************************************************************
                          GameStatus.cpp  -  description                              
                             -------------------                                         
    begin                : Fri Mar 19 1999                                           
    copyright            : (C) 1999 by Andreas Hundt, Denis Oliver Kropp                         
    email                : clanbomber@fischlustig.de                                     
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   * 
 *                                                                         *
 ***************************************************************************/
// $Id: GameStatus.cpp,v 1.4 2004/01/08 18:10:39 der_fisch Exp $


#include <ClanLib/Core/System/system.h>
#include <ClanLib/Display/Input/input.h>
#include <ClanLib/Display/Input/keyboard.h>
#include <ClanLib/Display/Input/key.h>
#include <ClanLib/Sound/soundbuffer.h>
#include <ClanLib/Display/Display/display.h>
#include <ClanLib/Display/Display/surface.h>
#include <ClanLib/Display/Font/font.h>

#include "ClanBomber.h"
#include "GameStatus.h"

#include "GameObject.h"
#include "Config.h"
#include "Timer.h"
#include "Bomber.h"
#include "Map.h"

#include <math.h>

GameStatus::GameStatus(ClanBomberApplication* _app)
{
	app = _app;
}

void GameStatus::show()
{
	float pokal_scroll_in = 800;

	Timer timer;
	timer.reset();
	
	if (end_of_game)
	{
		Resources::Gamestatus_klatsch()->play();
	}
	
	while(!(CL_Keyboard::get_keycode(CL_KEY_SPACE)))
	{
		Resources::Gamestatus_background()->put_screen(0, 0, 0 );
		Resources::Font_small()->print_center( 400, 570, "PRESS SPACE TO CONTINUE.");
		
		if (!winner)
		{
			Resources::Font_big()->print_center( 500, 40, "Draw Game" );
		}
		else
		{
			Resources::Font_big()->print_center( 500, 40, winner->get_name() + " won" );
		}
				
		int nr = 0;
		CL_Iterator<Bomber> bomber_object_counter(app->bomber_objects);
		while ((bomber_object_counter.next() != NULL) && (nr < 8))
		{
			nr++;
			bomber_object_counter()->act();
			Resources::Font_big()->print_left( 70, 157+nr*43,bomber_object_counter()->get_name() );

			if (bomber_object_counter() == winner)
			{
				if (!end_of_game)
				{
					((GameObject*)bomber_object_counter())->show( 5+(nr%2)*20, 150+nr*43);			
					for (int i=0; i<bomber_object_counter()->get_points()-1; i++)
					{
						Resources::Gamestatus_tools()->put_screen(300+i*43, 150+nr*43, 0 );
					}
					Resources::Gamestatus_tools()->put_screen( max(257+bomber_object_counter()->get_points()*43, (int)pokal_scroll_in), 150+nr*43, 0 );
				}
				else
				{
					float scalefactor;
					scalefactor = (float)(sin(CL_System::get_time()/100.0f)+2.0f)/2;
					((GameObject*)bomber_object_counter())->show( int(5+(800-pokal_scroll_in)/3), 150+nr*43,scalefactor);
					for (int i=0; i<bomber_object_counter()->get_points()-1; i++)
					{
						Resources::Gamestatus_tools()->put_screen(int(320+i*43-20*scalefactor), int(170+nr*43-20*scalefactor),scalefactor,scalefactor, 0 );
					}
					Resources::Gamestatus_tools()->put_screen( int(max(277+bomber_object_counter()->get_points()*43-20*scalefactor, (int)pokal_scroll_in)), int(170+nr*43-20*scalefactor), scalefactor,scalefactor, 0 );
				}
			}
			else
			{
				((GameObject*)bomber_object_counter())->show( 5+(nr%2)*20, 150+nr*43);			
				for (int i=0; i<bomber_object_counter()->get_points(); i++)
				{
					Resources::Gamestatus_tools()->put_screen(300+i*43, 150+nr*43, 0 );
				}
			}
		}
		if (end_of_game)
		{
			Resources::Font_big()->print_center( 500, 80, "the Match");
		}
		else
		{
			Resources::Font_small()->print_right( 785, 10, "NEXT LEVEL");
			CL_String temp_string = app->map->get_name();
			temp_string.to_upper();
			Resources::Font_small()->print_right( 785, 125, temp_string);			
			app->map->show_preview(790-119,30,0.18f);
		}

		CL_Display::flip_display();
		if (pokal_scroll_in > 100)
		{
			if (!end_of_game)
			{
				pokal_scroll_in -= timer.time_elapsed(true)*1000.0f;
			}
			else
			{
				pokal_scroll_in -= timer.time_elapsed(true)*333.0f;
			}
		}
		CL_System::keep_alive();
	
	}

	CL_Display::clear_display();
	CL_Display::flip_display();
	CL_Display::clear_display();
	CL_Display::flip_display();
	CL_Display::clear_display();
	CL_Display::flip_display();
}

GameStatus::~GameStatus()
{
}

void GameStatus::analyze_game()
{
	end_of_game = false;
	winner = NULL;
	
	CL_Iterator<Bomber> bomber_object_counter(app->bomber_objects);
	while (bomber_object_counter.next() != NULL)
	{
		if (!bomber_object_counter()->is_dead())
		{
			winner = bomber_object_counter();
		}
		if (bomber_object_counter()->get_points() == Config::get_points_to_win() )
		{
			end_of_game = true;
		}
	}
}

bool GameStatus::get_end_of_game()
{
	return end_of_game;
}

















































