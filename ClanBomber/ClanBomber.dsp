# Microsoft Developer Studio Project File - Name="ClanBomber" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=ClanBomber - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "ClanBomber.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "ClanBomber.mak" CFG="ClanBomber - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ClanBomber - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe
# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "win32"
# PROP Intermediate_Dir "win32"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MT /W2 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /FD /c
# SUBTRACT CPP /YX /Yc /Yu
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 user32.lib clanApp.lib clanCore.lib clanDisplay.lib clanSound.lib /nologo /subsystem:windows /pdb:none /machine:I386 /out:"clanbomber/ClanBomber.exe"
# Begin Target

# Name "ClanBomber - Win32 Release"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\clanbomber\Bomb.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Bomber.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Bomber_Corpse.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\ClanBomber.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Config.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Controller.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Controller_AI.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Controller_AI_MIC.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Controller_Joystick.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Controller_Keyboard.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Corpse_Part.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Credits.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Debug.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Disease.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Disease_Fast.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Disease_Frozen.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Disease_PutBomb.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Disease_Stoned.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Explosion.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Extra.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Extra_Bomb.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Extra_Glove.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Extra_Joint.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Extra_Kick.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Extra_Koks.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Extra_Power.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Extra_Skateboard.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Extra_Viagra.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\GameObject.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\GameStatus.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\GameStatus_Team.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Map.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\MapEditor.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\MapEntry.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\MapSelector.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\MapTile.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\MapTile_Arrow.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\MapTile_Box.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\MapTile_Ground.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\MapTile_Ice.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\MapTile_None.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\MapTile_Trap.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\MapTile_Wall.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Menu.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Observer.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\PlayerSetup.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Resources.cpp
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Timer.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\clanbomber\array.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Bomb.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Bomber.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Bomber_Corpse.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\ClanBomber.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Config.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Controller.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Controller_AI.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Controller_AI_MIC.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Controller_Joystick.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Controller_Keyboard.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Corpse_Part.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Credits.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Debug.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Disease.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Disease_Fast.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Disease_Frozen.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Disease_PutBomb.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Disease_Stoned.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Explosion.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Extra.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Extra_Bomb.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Extra_Glove.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Extra_Joint.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Extra_Kick.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Extra_Koks.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Extra_Power.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Extra_Skateboard.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Extra_Viagra.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\GameObject.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\GameStatus.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\GameStatus_Team.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\link.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Map.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\MapEditor.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\MapEntry.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\MapSelector.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\MapTile.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\MapTile_Arrow.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\MapTile_Box.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\MapTile_Ground.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\MapTile_Ice.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\MapTile_None.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\MapTile_Trap.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\MapTile_Wall.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Menu.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Observer.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\PlayerSetup.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Resources.h
# End Source File
# Begin Source File

SOURCE=.\clanbomber\Timer.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
