%define version 1.03
%define release 1
%define prefix /usr

Summary: ClanBomber, the cool game that uses ClanLib.
Name: ClanBomber
Version: %{version}
Release: %{release}
Copyright: GPL
Group: Amusements/Games
BuildRoot: /tmp/clanbomber-root
Source: ClanBomber-%{version}.tar.gz
URL: http://www.clanbomber.de
Requires: ClanLib >= 0.5.0

%description
ClanBomber is very nice Bomberman/Dynablaster like game. It has multiplayer
support (8 players), but not yet network support. Please try it! :-)


%prep
%setup

%build
./configure --prefix=%prefix
make

%install
make prefix=$RPM_BUILD_ROOT%{prefix} install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)
%doc README QUOTES TODO COPYING AUTHORS ChangeLog
%{prefix}/share/clanbomber/*
%{prefix}/bin/clanbomber
